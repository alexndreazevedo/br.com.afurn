php install.php
php bin\composer.phar install

cd vendor\spekkionu\wideimage
git pull origin master
cd ..\..\..

rm db.sqlite
php bin\schema.php > schema.sql
sqlite3 db.sqlite < schema.sql
php bin\fixtures.php
rm schema.sql

cd public_html\assets
mklink /D base ..\..\lib\base\src\assets\base\
mklink /D vendor ..\..\lib\base\src\assets\vendor\

compass compile base
compass compile site