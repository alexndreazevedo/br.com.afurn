<?php
$settings = include dirname(__FILE__) . DIRECTORY_SEPARATOR . 'settings.php';

if (!isset($application))
{
    $application = null;
}

include $settings['cdc_abs'] . 'Cdc' . DIRECTORY_SEPARATOR . 'Loader.php';
include $settings['cdc_abs'] . 'Cdc' . DIRECTORY_SEPARATOR . 'ArrayHelper.php';
include $settings['cdc_abs'] . 'Cdc' . DIRECTORY_SEPARATOR . 'Config.php';
include dirname(__FILE__) . DIRECTORY_SEPARATOR . 'Config.php';

Config::$loader = new Cdc_Loader;
Config::$loader->register();

Config::source($settings, $application);
