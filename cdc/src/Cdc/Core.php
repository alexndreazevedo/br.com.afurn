<?php

/**
 * Cdc Toolkit
 *
 * Copyright 2012 Eduardo Marinho
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * 	 http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * @author Eduardo Marinho
 * @package Cdc
 */
// @codeCoverageIgnoreStart

define('DEFAULT_OPERATION', 'default');

class Cdc_Core
{

    public static function guess_module_name($class)
    {
        $pieces = explode('_', $class);
        foreach ($pieces as $p)
        {
            if ($p !== 'Module')
            {
                return $p;
            }
        }
        throw new Cdc_Exception_GuessModuleName;
    }

    public static function autoload($class)
    {
        $fileName = dirname(dirname(__FILE__)) . DIRECTORY_SEPARATOR . strtr($class, '_', DIRECTORY_SEPARATOR) . '.php';
        if (self::isReadable($fileName))
        {
            include $fileName;
        }
    }

    /* Zend Framework */

    public static function isReadable($filename)
    {
        if (is_readable($filename))
        {
            // Return early if the filename is readable without needing the
            // include_path
            return true;
        }

        if (strtoupper(substr(PHP_OS, 0, 3)) == 'WIN'
            && preg_match('/^[a-z]:/i', $filename)
        )
        {
            // If on windows, and path provided is clearly an absolute path,
            // return false immediately
            return false;
        }

        foreach (Cdc_Core::explodeIncludePath() as $path)
        {
            if ($path == '.')
            {
                if (is_readable($filename))
                {
                    return true;
                }
                continue;
            }
            $file = $path . '/' . $filename;

            if (is_readable($file))
            {
                return true;
            }
        }
        return false;
    }

    /* Zend Framework */

    public static function explodeIncludePath($path = null)
    {
        if (null === $path)
        {
            $path = get_include_path();
        }

        if (PATH_SEPARATOR == ':')
        {
            // On *nix systems, include_paths which include paths with a stream
            // schema cannot be safely explode'd, so we have to be a bit more
            // intelligent in the approach.
            $paths = preg_split('#:(?!//)#', $path);
        }
        else
        {
            $paths = explode(PATH_SEPARATOR, $path);
        }
        return $paths;
    }

    public static function boot()
    {
        if (isset($_SESSION['flash']))
        {
            foreach ($_SESSION['flash'] as $v)
            {
                event($v['message'], $v['level'], $v['dialog']);
            }
            unset($_SESSION['flash']);
        }
    }

}

function coalesce(/* ... */)
{
    $args = func_get_args();
    foreach ($args as $arg)
    {
        if (null !== $arg && '' !== $arg)
        {
            return $arg;
        }
    }
}

function required($def)
{
    if (isset($def[Cdc_Definition::TYPE_WIDGET]['attributes']['required']))
    {
        return ' *';
    }

    if (array_key_exists(Cdc_Definition::TYPE_RULE, $def))
    {
        $index = array_search(array('Cdc_Rule_Required'), $def[Cdc_Definition::TYPE_RULE]);
        if ($index !== false)
        {
            return ' *';
        }
    }
}

function e($var)
{
    echo htmlentities($var, ENT_QUOTES, 'UTF-8');
}

function r($var)
{
    return htmlentities($var, ENT_QUOTES, 'UTF-8');
}

function vd($var)
{
    var_dump($var);
}

function fvd($var, $return = false)
{
    ob_start();
    var_dump($var);
    $dump = ob_get_clean();
    $dump = "<?php\n\$dump = $dump; \n?>";

    if ($return === true)
    {
        return highlight_string($dump, true);
    }
    highlight_string($dump);
}

function fve($var, $return = false)
{
    ob_start();
    var_export($var);
    $dump = ob_get_clean();
    $dump = "<?php\n\$export = $dump; \n?>";

    if ($return === true)
    {
        return highlight_string($dump, true);
    }
    highlight_string($dump);
}

function label($index, $labels = array())
{
    if (!$labels)
    {
        $labels = Config::$labels;
    }

    if (isset($labels[$index]))
    {
        if (is_string($labels[$index]))
        {
            return ucfirst($labels[$index]);
        }
    }
    return $index;
}

function event($msg, $level = LOG_INFO, $dialog = false)
{
    if (!isset($GLOBALS['events']))
    {
        $GLOBALS['events'] = array();
    }

    $message = array();
    $message['level'] = $level;
    $message['message'] = $msg;
    $message['dialog'] = $dialog;
    $GLOBALS['events'][] = $message;
}

function flash($msg, $level = LOG_INFO, $dialog = false)
{
    if (!isset($_SESSION['flash']))
    {
        $_SESSION['flash'] = array();
    }

    $message = array();
    $message['level'] = $level;
    $message['message'] = $msg;
    $message['dialog'] = $dialog;
    $_SESSION['flash'][] = $message;
}

function store_current_events()
{
    if (!isset($GLOBALS['events']))
    {
        $GLOBALS['events'] = array();
    }

    if (!isset($_SESSION['flash']))
    {
        $_SESSION['flash'] = array();
    }
    $_SESSION['flash'] = array_merge($_SESSION['flash'], $GLOBALS['events']);
}

function get_dialog_events()
{
    $return = array();
    if (!isset($GLOBALS['events']))
    {
        return $return;
    }
    foreach ($GLOBALS['events'] as $ev)
    {
        if ($ev['dialog'] !== false)
        {
            $return[] = $ev;
        }
    }
    return $return;
}

function logLevel2Class($level)
{
    $class = '';
    $button = '<button class="close" data-dismiss="alert">&times;</button>';
    $icon = '<i class="icon-exclamation-sign"></i>';
    switch ($level)
    {
        case LOG_EMERG:
            $class = 'alert alert-error emerg';
            break;
        case LOG_ALERT:
            $class = 'alert alert-error';
            break;
        case LOG_CRIT:
            $class = 'alert alert-error crit';
            break;
        case LOG_ERR:
            $class = 'alert alert-error err';
            break;
        case LOG_WARNING:
            $class = 'alert warning';
            $icon = '<i class="icon-info-sign"></i>';
            break;
        default:
        case LOG_INFO:
            $class = 'alert alert-info info';
            $icon = '<i class="icon-ok-sign"></i>';
            break;
        case LOG_DEBUG:
            $class = 'alert alert-info debug';
            $icon = '<i class="icon-eye-open"></i>';
            break;
    }
    return array('class' => $class, 'icon' => $icon, 'button' => $button);
}

function display_system_events($return = false, $root = array())
{
    if (!isset($GLOBALS['events']))
    {
        $GLOBALS['events'] = array();
    }
    $root or $root = $GLOBALS['events'];
    if (empty($root))
    {
        return;
    }
    $events = '';
    foreach ($root as $ev)
    {
        $level = logLevel2Class($ev['level']);
        $events .= '<li class="' . $level['class'] . '">' . $level['icon'] . ' ' . $ev['message'] . ' ' . $level['button'] . '</li>';
    }
    if ($events)
    {
        $events = '<ul class="system-messages">' . $events . '</ul>';
    }
    if ($return)
    {
        return $events;
    }

    echo $events;
}

function f($type, $variable_name, $filter = FILTER_DEFAULT, $options = null)
{
    if (is_array($type))
    {
        if (isset($type[$variable_name]))
        {
            return filter_var($type[$variable_name], $filter, $options);
        }
        return null;
    }
    return filter_input($type, $variable_name, $filter, $options);
}

function db($index = 'default', $reconnect = false, $pdo_params = array(), $debug = false)
{
    static $db;
    if ($reconnect)
    {
        $db[$index] = null;
    }
    if (!isset($db[$index]))
    {
        $pdo_params[PDO::ATTR_DEFAULT_FETCH_MODE] = PDO::FETCH_ASSOC;
        $pdo_params[PDO::ATTR_ERRMODE] = PDO::ERRMODE_EXCEPTION;
        if ($debug)
        {
            $db[$index] = new Cdc_Pdo_Debug(Config::$db_dsn, Config::$db_user, Config::$db_password, $pdo_params);
        }
        else
        {
            $db[$index] = new PDO(Config::$db_dsn, Config::$db_user, Config::$db_password, $pdo_params);
        }
    }
    return $db[$index];
}

function turn_off_magic_quotes()
{
// http://php.net/manual/pt_BR/security.magicquotes.disabling.php
    if (get_magic_quotes_gpc())
    {
        $process = array(&$_GET, &$_POST, &$_COOKIE, &$_REQUEST);
        while (list($key, $val) = each($process))
        {
            foreach ($val as $k => $v)
            {
                unset($process[$key][$k]);
                if (is_array($v))
                {
                    $process[$key][stripslashes($k)] = $v;
                    $process[] = &$process[$key][stripslashes($k)];
                }
                else
                {
                    $process[$key][stripslashes($k)] = stripslashes($v);
                }
            }
        }
        unset($process);
    }
}

// @codeCoverageIgnoreEnd