<?php

class Cdc_Table
{

    /**
     * Cria uma tabela
     *
     * @param <type> $caption Caption da tabela
     * @param <type> $lista Lista de resultados
     * @param <type> $pager Paginador, normalmente o resultado de Cdc_Pager::render
     * @param array $options Opções para a coluna opções, um array de callbacks que recebem a linha atual
     * @param array $formatters Formatadores de coluna, no formato coluna => callback que recebe a linha atual *E A LISTA*
     * @param array $providers Provedores de dados, no formato coluna => callback que recebe a linha atual E A LISTA e retorna a mesma linha com os dados adicionais
     * @param array $skip Colunas ignoradas na exibição
     * @param string $index Índice que será usado como valor num checkbox que será colocado em cada linha. O nome é o próprio índice.
     * @return string Tabela
     */
    public static function render($caption, $lista, $pager = null, array $options = array(), array $formatters = array(), array $provider = array(), array $skip = array(), $index = null)
    {
        $item = array();
        $tbody = '<tbody>';
        $alt   = 0;
        if ($lista instanceof PDOStatement)
        {
            $lista = $lista->fetchAll();
        }

        if ($provider)
        {
            $provider = current($provider);
            $p_func   = reset($provider);

            if (isset($provider[1]))
            {
                $p_params = $provider[1];
            }
            else
            {
                $p_params = array();
            }

            $lista = call_user_func_array($p_func, array($lista, $p_params));
        }

        $sample = array();
        $decrease_colspan = array();
        foreach ($lista as $item)
        {
            $sample = $item;

            $tbody .= '<tr class="' . ($alt++ % 2 ? 'par' : 'impar') . '">';

            if ($index)
            {
                $tbody .= '<td class="check"><label class="checkbox"><input type="checkbox" class="checkbox" name="' . $index . '[]" value="' . $item[$index] . '"></label></td>';
            }

            $options_html = '';
            if ($options)
            {
                $options_html = '<td class="options">';

                $o_func = reset($options);
                if (isset($options[1]))
                {
                    $o_params = $options[1];
                }
                else
                {
                    $o_params = array();
                }

                $options_callback_result = call_user_func_array($o_func, array($item, $lista, $o_params));

                if (false === $options_callback_result)
                {
                    continue;
                }

                $options_html .= $options_callback_result;

                $options_html .= '</td>';
            }

            foreach ($item as $key => $value)
            {
                $cell = '';
                if (false !== array_search($key, $skip))
                {
                    continue;
                }

                if (isset($formatters[$key]))
                {
                    $f_func = reset($formatters[$key]);

                    if (isset($formatters[$key][1]))
                    {
                        $f_params = $formatters[$key][1];
                    }
                    else
                    {
                        $f_params = array();
                    }

                    $cell = call_user_func_array($f_func, array($item, $lista, $key, $f_params));
                }
                else
                {
                    if (!array_key_exists($key, $item))
                    {
                        continue;
                    }

                    if (is_array($item[$key]))
                    {
                        $decrease_colspan[$key] = true;
                        unset($sample[$key]);
                    }
                    else
                    {
                        $cell = '<td class="' . $key . '"><div>' . $item[$key] . '</div></td>';
                    }
                }
                $tbody .= $cell;
            }

            $tbody .= $options_html . '</tr>';
        }
        $tbody .= '</tbody>';

        $colspan = count($item) - count($decrease_colspan);

        $thead = '<thead><tr>';
        if ($index)
        {
            $colspan++;
            $thead .= '<th><input class="checkbox select_all" type="checkbox" rel="tooltip" title="Alternar seleção das caixas"></th>';
        }
        foreach ($sample as $key => $ignore_me)
        {
            if (false !== array_search($key, $skip))
            {
                continue;
            }
            $thead .= '<th class="' . $key . '">' . label($key) . '</th>';
        }
        if (!empty($options))
        {
            $thead .= '<th>Opções</th>';
            $colspan++;
        }
        $thead .= '</tr></thead>';

        $tfoot = '';
        if ($pager)
        {
            $tfoot = '<tfoot><tr><td colspan="' . $colspan . '">' . $pager . '</td></tr></tfoot>';
        }
        else
        {
            $tfoot = '';
        }
        return '<table class="table table-striped table-bordered table-hover"><caption>' . $caption . '</caption>' . $thead . $tfoot . $tbody . '</table>';
    }

    /**
     * Cria uma tabela
     *
     * @param <type> $caption Caption da tabela
     * @param <type> $lista Lista de resultados
     * @param array $formatters Formatadores de coluna, no formato coluna => callback que recebe a linha atual *E A LISTA*
     * @param array $providers Provedores de dados, no formato coluna => callback que recebe a linha atual E A LISTA e retorna a mesma linha com os dados adicionais
     * @param array $skip Colunas ignoradas na exibição
     * @return string Tabela
     */
    public static function renderHorizontal($caption, $lista, array $formatters = array(), array $provider = array(), array $skip = array())
    {
        $tbody = '<tbody>';
        $alt   = 0;
        if ($lista instanceof PDOStatement)
        {
            $lista = $lista->fetchAll();
        }

        if ($provider)
        {
            $provider = current($provider);
            $p_func   = reset($provider);

            if (isset($provider[1]))
            {
                $p_params = $provider[1];
            }
            else
            {
                $p_params = array();
            }

            $lista = call_user_func_array($p_func, array($lista, $p_params));
        }

        foreach ($lista as $key => $value)
        {

            $tbody .= '<tr class="' . ($alt++ % 2 ? 'par' : 'impar') . '">';

            $cell = '';
            if (false !== array_search($key, $skip))
            {
                continue;
            }

            if (isset($formatters[$key]))
            {
                $f_func = reset($formatters[$key]);

                if (isset($formatters[$key][1]))
                {
                    $f_params = $formatters[$key][1];
                }
                else
                {
                    $f_params = array();
                }

                $cell = call_user_func_array($f_func, array($lista, array($lista), $key, $f_params));
            }
            else
            {
                $cell = '<td class="' . $key . '">' . $value . '</td>';
            }
            $tbody .= '<th>' . label($key) . '</th>' . $cell;
            $tbody  .= '</tr>';
        }

        $tbody .= '</tbody>';


        return '<table class="table-horizontal table table-striped table-bordered table-condensed"><caption class="label">' . $caption . '</caption>' . $tbody . '</table>';
    }

}
