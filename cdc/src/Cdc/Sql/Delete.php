<?php

class Cdc_Sql_Delete extends Cdc_Sql_Statement
{

    public function __toString()
    {
        $sql = 'delete '
                . $this->buildFrom()
                . $this->buildWhere();
        return $sql;
    }

    public function stmt(&$values = array())
    {
        $sql = 'delete '
                . $this->buildFrom()
                . $this->buildWhere(array(), $values);
        $stmt = $this->getPdo()->prepare($sql);

        $this->_bindValues($stmt, $values);

        $stmt->execute();
        return $stmt;
    }

    public function buildFrom($params = array(), &$values = array())
    {
        if (!$params)
        {
            $params = $this->from;
        }
        $result = '';
        foreach ($this->from as $k => $v)
        {
            $result .= $v . ', ';
        }
        if ($result)
        {
            $result = 'from ' . substr($result, 0, -2);
        }
        return $result;
    }

}

