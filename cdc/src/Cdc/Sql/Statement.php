<?php

class Cdc_Sql_Statement extends Cdc_Pdo_Container
{

    public $where = array();
    public $from = array();

    /**
     * This is used internally by the toolkit, to bind the query to an operation.
     * @var string
     */
    protected $operation;

    public function setOperation($operation)
    {
        $this->operation = $operation;
        return $this;
    }

    public function getOperation()
    {
        return $this->operation;
    }

    public function where($where = array())
    {
        $this->where = $where;
        return $this;
    }

    public function from($from = array())
    {
        $this->from = $from;
        return $this;
    }

    protected function _bindValues($stmt, $values)
    {
        $i = 1;
        foreach ($values as $v)
        {
            if (is_null($v))
            {
                $stmt->bindValue($i++, $v, PDO::PARAM_NULL);
            }
            elseif (is_bool($v))
            {
                $stmt->bindValue($i++, $v, PDO::PARAM_BOOL);
            }
            else
            {
                $stmt->bindValue($i++, $v);
            }
        }
    }

    private function _where($params = array(), $root = null, &$values = array())
    {
        $values = (array) $values;
        if (!$root)
        {
            $root = 'and';
        }

        if (count($params) == 1)
        {
            $_possible_root = trim(key($params));
            $value          = reset($params);
            if ($this->_isRoot($_possible_root))
            {
                $root   = $_possible_root;
                $params = $value;
                $return = array();
                if (is_array($params))
                {
                    foreach ($params as $kc => $kv)
                    {
                        if (is_numeric($kc))
                        {
                            $kc       = 'and';
                        }
                        $return[] = $this->_where(array($kc => $kv), $root, $values);
                    }
                }
                else
                {
                    $return[] = $value;
                }

                return '(' . implode(' ' . $root . ' ', $return) . ')';
            }
            if (is_array($value))
            {
                $values = array_merge($values, $value);
                return '(' . $_possible_root . ' (' . implode(', ', array_fill(0, count($value), '?')) . '))';
            }
            else
            {
                if (is_numeric($_possible_root))
                {
                    return '(' . $value . ')';
                }
                $values[] = $value;
                return '(' . $_possible_root . ' ?)';
            }
        }

        $params = array($root => $params);

        return $this->_where($params, $root, $values);
    }

    private function _isRoot($value)
    {
        return in_array(strtolower($value), array('and', 'or'));
    }

    public function buildWhere($params = array(), &$values = array(), $prefix = 'where')
    {
        $prefix = trim($prefix);

        if (!$params && isset($this->$prefix))
        {
            $params = $this->$prefix;
        }

        // @codeCoverageIgnoreStart
        if (!$params)
        {
            return '';
        }
        // @codeCoverageIgnoreEnd

        $params = array_filter($params);

        $where = $this->_where($params, null, $values);

        if ($where == '()')
        {
            return '';
        }

        if ($where)
        {
            $where = ' ' . $prefix . ' ' . $where;
        }

        return $where;
    }

}

