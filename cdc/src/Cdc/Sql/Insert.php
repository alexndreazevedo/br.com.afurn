<?php

class Cdc_Sql_Insert extends Cdc_Sql_Statement
{

    public $cols = array();

    public function cols($cols = array())
    {
        $this->cols = $cols;
        return $this;
    }

    public function __toString()
    {
        $sql = 'insert into '
                . $this->buildFrom()
                . $this->buildColumns();
        return $sql;
    }

    public function stmt($values = array())
    {
        $sql = 'insert into '
                . $this->buildFrom()
                . $this->buildColumns(array(), $values);
        $stmt = $this->getPdo()->prepare($sql);
        $this->_bindValues($stmt, $values);

        $stmt->execute();
        return $stmt;
    }

    public function buildFrom($params = array(), &$values = array())
    {
        if (!$params)
        {
            $params = $this->from;
        }
        $result = '';
        foreach ($this->from as $k => $v)
        {
            $result .= $v . ', ';
        }
        if ($result)
        {
            $result = substr($result, 0, -2);
        }
        return $result;
    }

    public function buildColumns($params = array(), &$values = array())
    {
        if (!$params)
        {
            $params = $this->cols;
        }
        // @CodeCoverageIgnoreStart
        if (!$params)
        {
            return '';
        }
        // @CodeCoverageIgnoreEnd
        $result = '';

        $result .= ' (' . implode(', ', array_keys($params)) . ')';
        foreach ($params as $v)
        {
            $values[] = $v;
        }
        $result .= ' values (' . implode(', ', array_fill(1, count($params), '?')) . ')';

        return $result;
    }

}

