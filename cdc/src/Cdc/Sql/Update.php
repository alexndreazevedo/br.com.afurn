<?php

class Cdc_Sql_Update extends Cdc_Sql_Statement
{

    public $cols = array();

    public function cols($cols = array())
    {
        $this->cols = $cols;
        return $this;
    }

    public function __toString()
    {
        $sql = 'update '
                . $this->buildFrom()
                . $this->buildColumns()
                . $this->buildWhere();
        return $sql;
    }

    public function stmt(&$values = array())
    {
        $sql = 'update '
                . $this->buildFrom()
                . $this->buildColumns(array(), $values)
                . $this->buildWhere(array(), $values);
        $stmt = $this->getPdo()->prepare($sql);

        $this->_bindValues($stmt, $values);

        $stmt->execute();
        return $stmt;
    }

    public function buildFrom($params = array(), &$values = false)
    {
        if (!$params)
        {
            $params = $this->from;
        }
        $result = '';
        foreach ($this->from as $k => $v)
        {
            $result .= $v . ', ';
        }
        if ($result)
        {
            $result = substr($result, 0, -2);
        }
        return $result;
    }

    public function buildColumns($params = array(), &$values = false)
    {
        // @CodeCoverageIgnoreStart
        if (!$params)
        {
            $params = $this->cols;
        }
        // @CodeCoverageIgnoreEnd
        $result = '';
        foreach ($params as $k => $v)
        {
            $result .= $k . ' = ?, ';
            $values[] = $v;
        }
        if ($result)
        {
            $result = ' set ' . substr($result, 0, -2);
        }
        return $result;
    }

}

