<?php

class Cdc_Navigation
{

    public static function breadcrumb($menu, $highlight_index = null, $ul_open = null, $breadcrumb_start = null, $breadcrumb_end = null, $separator = '/')
    {
        if ($breadcrumb_start)
        {
            $k                      = key($breadcrumb_start);
            $v                      = reset($breadcrumb_start);
            $v['submenu']           = $menu;
            $v['classes']['active'] = 'active';
            $menu                   = array(
                $k => $v
            );
        }
        return self::_menu($menu, $highlight_index, 1, $ul_open, true, $breadcrumb_end, $separator);
    }

    public static function menu($menu, $highlight_index = null, $ul_open = null, $raw = false)
    {
        return self::_menu($menu, $highlight_index, 1, $ul_open, false, array(), '/', $raw);
    }

    protected static function _has_active($set, $highlight_index, &$active)
    {
        foreach ($set as $key => $item)
        {
            if ($key == $highlight_index)
            {
                $active = true;
                return;
            }
            if (array_key_exists('submenu', $item) && count($item['submenu']))
            {
                self::_has_active($item['submenu'], $highlight_index, $active);
            }
        }
    }

    protected static function _menu($menu, $highlight_index = null, $level = 1, $ul_open = null, $breadcrumb = false, $breadcrumb_end = array(), $separator = '/', $raw = false)
    {
        $return = '';
        $type   = 'item-' . $level;

        $first_item = 'first';
        foreach ($menu as $key => $value)
        {
            if (array_key_exists('allow', $value) && !$value['allow'])
            {
                continue;
            }

            $has_submenu = false;
            $classes     = compact('type', 'first_item');
            if (array_key_exists('classes', $value))
            {
                $classes    = array_merge($classes, $value['classes']);
            }
            $first_item = '';
            if (array_key_exists('submenu', $value) && count($value['submenu']))
            {
                $has_submenu = true;
                $active = false;
                self::_has_active(array($key => $value), $highlight_index, $active);
                if ($active)
                {
                    $classes['active'] = 'active';
                }
            }

            if (array_key_exists('target', $value))
            {
                $target = 'target="' . $value['target'] . '"';
            }
            else
            {
                $target = '';
            }

            if (array_key_exists('alt', $value))
            {
                $alt = $value['alt'];
            }
            else
            {
                $alt = $value['title'];
            }

            if ($key == $highlight_index)
            {
                $classes['active'] = 'active';
            }

            if (!array_key_exists('active', $classes) && $breadcrumb)
            {
                continue;
            }

            if (array_key_exists('icon', $value))
            {
                $title = $value['icon'] . $value['title'];
            }
            else
            {
                $title = $value['title'];
            }

            if ($has_submenu)
            {
                $classes['dropdown'] = 'dropdown';

                $classes_str = ' class="' . implode(' ', $classes);

                $classes_str_list = $classes_str . ' li"';
                $classes_str_link = $classes_str . ' a"';

                if ($breadcrumb)
                {
                    $data_toggle    = '';
                    $caret          = '';
                    $title          = $value['title'];
                }
                else
                {
                    if ($raw)
                    {
                        $data_toggle = '';
                    }
                    else
                    {
                        $data_toggle = ' data-toggle="dropdown"';
                    }
                    $caret       = '<i class="fa fa-caret-down"></i>';
                }

                if (array_key_exists('url', $value))
                {

                    $item_content = '<a' . $data_toggle . ' href="' . $value['url'] . '"' . $classes_str_link . ' title="' . $alt . '" ' . $target . '>' . $title . $caret . '</a>';
                }
                else
                {
                    $item_content = $title;
                }

                if ($breadcrumb)
                {
                    $return .= '<li' . $classes_str_list . '>' . $item_content . '<span class="divider">' . $separator . '</span></li>' . self::_menu($value['submenu'], $highlight_index, $level + 1, null, $breadcrumb, $breadcrumb_end, $separator, $raw);
                }
                else
                {
                    $return .= '<li' . $classes_str_list . '>' . $item_content . self::_menu($value['submenu'], $highlight_index, $level + 1, null, $breadcrumb, $breadcrumb_end, $separator, $raw) . '</li>';
                }
            }
            else
            {
                $classes_str = ' class="' . implode(' ', $classes);

                $classes_str_list = $classes_str . ' li"';
                $classes_str_link = $classes_str . ' a"';

                if ($breadcrumb && !$breadcrumb_end)
                {
                    $return .= '<li' . $classes_str_list . '><span>' . $title . '</span></li>';
                }
                else
                {

                    if (array_key_exists('url', $value))
                    {
                        $item_content = '<a' . $classes_str_link . ' href="' . $value['url'] . '" title="' . $alt . '" ' . $target . '>' . $title . '</a>';
                    }
                    else
                    {
                        $item_content = $title;
                    }

                    $return .= '<li' . $classes_str_list . '>' . $item_content . '</li>';
                }
            }
        }
        $ul_close = '</ul>';
        if ($return)
        {
            if (!$ul_open)
            {
                if ($level === 1)
                {
                    if ($breadcrumb)
                    {
                        $c = 'breadcrumb';
                    }
                    else
                    {
                        $c       = $type . ' menu';
                    }
                    $ul_open = '<ul class="' . $c . '">';
                }
                else
                {
                    if ($breadcrumb)
                    {
                        $c       = $type;
                        $ul_open = '';
                    }
                    else
                    {
                        if ($raw)
                        {
                            $tbClass = '';
                        }
                        else
                        {
                            $tbClass = ' dropdown-menu';
                        }
                        $c       = $type . ' submenu' . $tbClass;
                        $ul_open = '<ul class="' . $c . '">';
                    }
                }
            }

            if (($level > 1) && $breadcrumb)
            {
                $ul_close = '';
            }
            if ($level === 1 && $breadcrumb_end)
            {
                $v = reset($breadcrumb_end);
                $return .= '<li class="active li"><span>' . $v['title'] . '</span> <span class="divider">' . $separator . '</span> </li>';
            }

            $return = $ul_open . $return . $ul_close;
        }
        return $return;
    }

}
