<?php

class Cdc_ConstraintMessagePrinter
{

    public static function event($messages, $labels, $label_function = 'label')
    {
        foreach ($messages as $key => $m)
        {
            foreach ($m as $message)
            {
                event('<strong>' . $label_function($key, $labels) . ': </strong>' . $message, LOG_ERR);
            }
        }
    }

}