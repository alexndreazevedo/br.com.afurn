<?php

abstract class Cdc_Controller
{
    public $isFront = false;

    public $pageTitle = '';

    public $index;

    public $args;

    /**
     * Parâmetros que foram extraídos da URL. Apenas para comodidade.
     *
     * Isto é igual ao que está contido em Cdc_Router, que está presente em
     * Cdc_Dispatcher, que está contido em Cdc_Config.
     *
     * @var array
     */
    public $urlParams;

    public $layoutTemplate = 'layout/default.phtml';

    public $loginRouteName = 'admin_login';

    abstract public function content();

    public function link($route, $data = array(), $query_params = array())
    {
        return Config::$dispatcher->router->generate($route, $data, $query_params);
    }

    public function getLayout($module = null, $template = null)
    {
        $template or $template = $this->layoutTemplate;

        return $this->getTemplate($template, $module);
    }

    public function getTemplate($file, $module = null)
    {

        $fn = Config::$modules[''] . 'templates' . DIRECTORY_SEPARATOR . $file;

        if (file_exists($fn))
        {
            return $fn;
        }

        if (!$module)
        {
            $pieces = explode('_', get_class($this));
            $module = $pieces[0];
        }

        if (array_key_exists($module, Config::$modules))
        {
            $fn = Config::$modules[$module] . $module . DIRECTORY_SEPARATOR . 'templates' . DIRECTORY_SEPARATOR . $file;

            if (file_exists($fn))
            {
                return $fn;
            }
        }

        return $file;
    }

    public function __construct($args)
    {
        $this->args = $args;
        $this->urlParams = Config::$dispatcher->matchedRoute->getParameters();
        $this->init();
    }

    public function getPdo()
    {
        return Cdc_Pdo_Pool::getConnection(Config::$default_db, Config::$debug);
    }

    protected $model_cache = array();

    /**
     *
     * @param type $model_name
     * @return Cdc_Datastore
     */
    public function getModel($model_name, $operation = DEFAULT_OPERATION)
    {
        if (!array_key_exists($model_name, $this->model_cache))
        {
            $model      = new $model_name($this->getPdo());
            $definition = new Cdc_Definition($model->struct, $operation);
            $model->setDefinition($definition);
            $this->model_cache[$model_name] = $model;
        }
        else
        {
            $this->model_cache[$model_name]->getDefinition()->setOperation($operation);
        }

        return $this->model_cache[$model_name];
    }

    public function init()
    {

    }

    public function pageMenu()
    {
        return array();
    }

    public function titlebar()
    {

        if(!empty($this->pageTitle))
        {
            return $this->pageTitle . ' - ' . Config::$baseTitle;
        }

        return Config::$baseTitle;
    }

}
