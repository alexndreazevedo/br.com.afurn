<?php

class Cdc_Datastore extends Cdc_Pdo_Container
{

    /**
     * Definition
     * @var Cdc_Definition
     */
    protected $definition = null;
    public $struct     = array();

    public function __construct($pdo = null, $args = null)
    {
        parent::__construct($pdo, $args);
        $this->init();
    }

    public function createQuery($args = array(), $operation = null)
    {
        if ($operation)
        {
            $originalOperation = $this->getDefinition()->getOperation();
            $this->getDefinition()->setOperation($operation);
        }
        $relation          = $this->getDefinition()->query(Cdc_Definition::TYPE_RELATION)->fetch();

        $key = key($relation);

        if (!array_key_exists('statement_type', $relation[$key]))
        {
            $statementType = 'Select';
        }
        else
        {
            $statementType = ucfirst(strtolower($relation[$key]['statement_type']));
        }

        $method = 'create' . $statementType;

        $result = $this->$method($args);

        if ($operation)
        {
            $this->getDefinition()->setOperation($originalOperation);
        }
        return $result;
    }

    public function execute($sql)
    {
        return $sql->stmt();
    }

    protected function fetchCount($sql, $attachment_key)
    {
        $sql_count = new Cdc_Sql_Select($this->getPdo());
        $cols      = array();

        if ($attachment_key)
        {
            $sql_count->group = array($attachment_key);
            $cols[]     = $attachment_key;
            $fetch_mode = PDO::FETCH_KEY_PAIR;
            $method     = 'fetchAll';
            $sql_count->join = $sql->join; // @TODO: Entender isto
        }
        else
        {
            $fetch_mode = PDO::FETCH_COLUMN;
            $method     = 'fetch';
        }

        if ($sql->distinct)
        {
            $cols[] = 'count(distinct(' . key($sql->cols) . '))';
        }
        else
        {
            $cols[] = 'count(*)';
        }


        $sql_count->cols = $cols;
        $sql_count->from = $sql->from;
        $sql_count->where = $sql->where;
        // @TODO: Entender isto
        $sql_count->join = $sql->join;

        return $sql_count->stmt()->$method($fetch_mode);
    }

    public function hydrateResultOfExec($sql, $data = array() /* , $attachments = null */)
    {
        if (func_num_args() > 2)
        {
            $attachments = func_get_arg(1);
        }
        else
        {
            // $attachments = false;
            $attachments = null;
        }

        $result = $this->execute($sql);
        $pdo    = $this->getPdo();

//        $ins = new Cdc_Sql_Insert($pdo);
//        $ins->from = array($attachment['relation_table']);

        if (null === $attachments)
        {
            $relations   = $this->getDefinition()->query(Cdc_Definition::TYPE_RELATION)->byKey(Cdc_Definition::TYPE_ATTACHMENT)->fetch();
            $relation    = reset($relations);
            $attachments = $relation[Cdc_Definition::TYPE_ATTACHMENT];
        }

        if ($sql instanceof Cdc_Sql_Insert)
        {
            $id = $this->getPdo()->lastInsertId($this->getDefaultSequenceName());
        }
        elseif ($sql instanceof Cdc_Sql_Update)
        {
            $id = reset($sql->where);
        }
        elseif ($sql instanceof Cdc_Sql_Delete)
        {
            $id = reset($sql->where);
        }
//                        'exec_params' => array(
//                            'relation_table' => array(
//                                'grupo_recurso',
//                            ),
//                            'relation_column_attachment' => array(
//                                'recurso_id',
//                            ),
//                            'attachment_table' => array(
//                                'recurso',
//                            ),
//                            'extra_columns' => array(
//                            ),
//                        ),
//            $del = new Cdc_Sql_Delete($pdo);
//
//            $del->from = $ins->from;
//            $del_ids = reset($data);
//            $del_ids = $del_ids[$key];
//
//            $del->where = array(
//                $value['relation_column_owner'] . ' = ' => $id,
//                    // $value['relation_column_attachment'] . ' in ' => $del_ids,
//            );
//
//            $del->stmt();


        $ins = new Cdc_Sql_Insert($pdo);
        $del = new Cdc_Sql_Delete($pdo);
        $cleaned_relations = array();
        $fields = $this->getDefinition()->query(Cdc_Definition::TYPE_ATTACHMENT)->fetch();

        // Colocar valores padrão nos campos. Se não tiver uma definição
        // de qual attachment pertence, configurar para o nome da chave.
        foreach ($fields as $kf => $f)
        {
            if ($f['type'] == Cdc_Definition::TYPE_RELATION)
            {
                unset($fields[$kf]);
                continue;
            }

            if (!isset($f[Cdc_Definition::TYPE_ATTACHMENT]))
            {
                throw new Cdc_Definition_Exception_AttachmentColumnWithoutRelation;
            }

            foreach ((array) $attachments as $key => $value)
            {

                $data_format = '';

                if ($f[Cdc_Definition::TYPE_ATTACHMENT] != $key)
                {
                    continue;
                }

                $data_format = f($f, 'data_format');

                // unset($fields[$k]);

                if (array_key_exists($kf, $data))
                {

                    $normalizedData = (array) Cdc_Form::obterValor(array('name' => $kf), $data, $f, false);

                    if (!isset($cleaned_relations[$value['exec_params']['relation_table']]))
                    {
                        $cleaned_relations[$value['exec_params']['relation_table']] = true;

                        if ($sql instanceof Cdc_Sql_Update)
                        {

                            $del->from = array($value['exec_params']['relation_table']);

                            $del->where = array($value['attachment_key'] . ' =' => $id);

                            /*
                            if ($normalizedData)
                            {
                                $conds = array();
                                $cond[$value['exec_params']['relation_column_attachment'] . ' not in '] = array_keys($normalizedData);

                                if (array_key_exists('extra_columns', $value['exec_params']))
                                {
                                    foreach ($value['exec_params']['extra_columns'] as $col)
                                    {
                                        $val = Cdc_ArrayHelper::pluck($normalizedData, $col);
                                        foreach ($val as $ecv)
                                        {
                                            $conds['and'] = array_merge($cond, array($col . ' = ' => $ecv));
                                        }

                                        $del->where['and'] = $conds;
                                    }
                                }



                            }
                            */

                            //if ($normalizedData || !array_key_exists('extra_columns', $value['exec_params']) || empty($value['exec_params']['extra_columns']))
                            // {
                            $this->execute($del);
                            // }

                        }
                    }

                    $ins->from = array($value['exec_params']['relation_table']);
                    foreach ($normalizedData as $val)
                    {
                        if (null === $val) continue; // @TODO: RETIRAR ESTA GAMBIARRA
                        // always reset insert clause
                        $ins->cols = array($value['attachment_key'] => $id);
                        // @TODO: Otimizar
                        if (is_array($val))
                        {
                            $ins->cols[$value['exec_params']['relation_column_attachment']] = $val[$value['exec_params']['relation_column_attachment']];

                            if (array_key_exists('extra_columns', $value['exec_params']))
                            {
                                foreach ($value['exec_params']['extra_columns'] as $col)
                                {
                                    $ins->cols[$col] = $val[$col];
                                }
                            }
                        }
                        else
                        {
                            $ins->cols[$value['exec_params']['relation_column_attachment']] = $val;
                        }

                        try
                        {
                            $this->execute($ins);
                        }
                        catch (Exception $e)
                        {

                        }
                    }
                }
            }

        }

        return $id;
    }

    public function hydrateResultOf($sql /* , $attachments = false */)
    {
        if (is_array($sql))
        {
            return $sql;
        }

        if (func_num_args() > 1)
        {
            $attachments = func_get_arg(1);
        }
        else
        {
            $attachments = false;
        }

        $result = $this->execute($sql)->fetchAll();

        $count = $this->fetchCount($sql, $attachments);

        if (empty($result))
        {
            return array();
        }

        if (null === $attachments)
        {
            return array($count => $result);
        }

        if (false === $attachments)
        {
            $relations = $this->getDefinition()->query(Cdc_Definition::TYPE_RELATION)->byKey(Cdc_Definition::TYPE_ATTACHMENT)->fetch();

            if (empty($relations))
            {
                return array($count => $result);
            }

            $this->resetTokens();

            $relations = reset($relations);

            $attachments = $relations[Cdc_Definition::TYPE_ATTACHMENT];

            foreach ($attachments as $value)
            {
                array_walk_recursive($value['query_params'], array($this, 'findTokens'));
            }
        }



        $hydrationParams = array();

        if ($this->tokens)
        {
            foreach ($this->tokens as $tok)
            {
                if (!array_key_exists($tok, $hydrationParams))
                {
                    $hydrationParams[$tok] = Cdc_ArrayHelper::pluck($result, $tok);
                }
            }
        }

        $rawAttachmentResults = array();
        $rawAttachmentCounts = array();

        foreach ($attachments as $key => $value)
        {

            $queryParams = $this->replaceAttachmentParams($value['query_params'], $hydrationParams);

            $select = new Cdc_Sql_Select($this->getPdo());

            foreach ($queryParams as $k => $v)
            {
                $select->$k = $v;
            }

            if (array_key_exists(Cdc_Definition::TYPE_ATTACHMENT, $value) && $value[Cdc_Definition::TYPE_ATTACHMENT])
            {
                $rawAttachmentResults[$key] = $this->hydrateResultOf($select, $value[Cdc_Definition::TYPE_ATTACHMENT]);
            }
            else
            {
                $rawAttachmentCounts[$key]  = $this->fetchCount($select, $attachments[$key]['attachment_key']);
                $rawAttachmentResults[$key] = $this->execute($select)->fetchAll();
            }
        }

        $attachmentResults = array();
        $attachmentCounts = array();
        $attachmentNames = array();

        $fields = $this->getDefinition()->query(Cdc_Definition::TYPE_ATTACHMENT)->fetch();

        foreach ($fields as $fk => $f)
        {
            if ($f['type'] == Cdc_Definition::TYPE_RELATION)
            {
                if (array_key_exists(Cdc_Definition::TYPE_ATTACHMENT, $f))
                {
                    foreach ($f[Cdc_Definition::TYPE_ATTACHMENT] as $cardinality_key => $cardinality_value)
                    {
                        if (array_key_exists('cardinality', $cardinality_value))
                        {
                            $attachmentResults[$cardinality_key] = $rawAttachmentResults[$cardinality_key];
                            $attachmentCounts[$cardinality_key] = $rawAttachmentCounts[$cardinality_key];
                            $attachmentNames[$cardinality_key] = $cardinality_key;
                        }
                    }
                }

                continue;
            }

            if (!isset($f[Cdc_Definition::TYPE_ATTACHMENT]))
            {
                throw new Cdc_Definition_Exception_AttachmentColumnWithoutRelation;
            }

            $rawAttachmentName = $f[Cdc_Definition::TYPE_ATTACHMENT];

            $attachmentResults[$fk] = $rawAttachmentResults[$rawAttachmentName];
            $attachmentCounts[$fk] = $rawAttachmentCounts[$rawAttachmentName];
            $attachmentNames[$fk] = $rawAttachmentName;

        }

        foreach ($result as $r_key => $r_value)
        {
            foreach ($attachmentResults as $k => $v)
            {
                $result[$r_key][$k] = array();
                $indexKey      = $attachments[$attachmentNames[$k]]['index_key'];
                $attachmentKey = $attachments[$attachmentNames[$k]]['attachment_key'];
                $distributionKey = f($attachments[$attachmentNames[$k]], 'distribution_key');
                foreach ($v as $k_row => $row)
                {
                    if ($r_value[$attachments[$attachmentNames[$k]]['parent_key']] === $row[$attachments[$attachmentNames[$k]]['attachment_key']])
                    {
                        if ($distributionKey)
                        {
                            if ($row[$distributionKey] !== $k)
                            {
                                continue;
                            }
                        }

                        $result[$r_key][$k][$attachmentCounts[$k][$row[$attachmentKey]]][$row[$indexKey]] = $row;

                    }
                }
            }
        }

        return array($count => $result);
    }

    private $tokens;

    private function resetTokens()
    {
        $this->tokens = array();
    }

    private function findTokens($value, $key)
    {
        if (preg_match_all('/#(\w+)#/', $value, $matches))
        {

            array_shift($matches);

            foreach ($matches as $m)
            {
                $this->tokens[] = $m[0];
            }
        }
    }

    private function replaceAttachmentParams($input, $hydrationParams)
    {
        $return = array();

        foreach ($input as $key => $value)
        {
            if (is_array($value))
            {
                $value = $this->replaceAttachmentParams($value, $hydrationParams);
            }
            elseif (preg_match_all('/#(\w+)#/', $value, $matches))
            {
                foreach ($matches[1] as $mk => $mv)
                {
                    $key          = trim(preg_replace('/' . $matches[0][$mk] . '/', '', $value));
                    $value        = $hydrationParams[$mv];
                }
            }
            $return[$key] = $value;
        }
        return $return;
    }

    protected function createSelect($args)
    {
        $sql = new Cdc_Sql_Select($this->getPdo());
        $sql->setOperation($this->getDefinition()->getOperation());

        $sql->cols($this->getDefinition()->query(Cdc_Definition::TYPE_COLUMN)->fetch(Cdc_Definition::MODE_KEY_ONLY));

        $sql->from($this->getDefinition()->query(Cdc_Definition::TYPE_RELATION)->fetch(Cdc_Definition::MODE_KEY_ONLY));

        if (array_key_exists('distinct', $args))
        {
            $sql->distinct = $args['distinct'];
            unset($args['distinct']);
        }
        foreach ($args as $key => $value)
        {
            $sql->$key($value);
        }

        return $sql;
    }

    protected function createInsert($args)
    {
        $sql = new Cdc_Sql_Insert($this->getPdo());
        $sql->setOperation($this->getDefinition()->getOperation());

        $keys = $this->getDefinition()->query(Cdc_Definition::TYPE_COLUMN)->fetch();

        $cols = array_intersect_key($args, $keys);

        $sql->cols($cols);
        $sql->from($this->getDefinition()->query(Cdc_Definition::TYPE_RELATION)->fetch(Cdc_Definition::MODE_KEY_ONLY));

        return $sql;
    }

    protected function createUpdate($args)
    {
        $sql = new Cdc_Sql_Update($this->getPdo());
        $sql->setOperation($this->getDefinition()->getOperation());

        if (array_key_exists('cols', $args))
        {
            $keys = $this->getDefinition()->query(Cdc_Definition::TYPE_COLUMN)->fetch();
            $cols = array_intersect_key($args['cols'], $keys);
            $sql->cols($cols);
        }

        $sql->from($this->getDefinition()->query(Cdc_Definition::TYPE_RELATION)->fetch(Cdc_Definition::MODE_KEY_ONLY));

        if (array_key_exists('where', $args))
        {
            $sql->where($args['where']);
        }



        return $sql;
    }

    protected function createDelete($args)
    {
        $sql = new Cdc_Sql_Delete($this->getPdo());
        $sql->setOperation($this->getDefinition()->getOperation());

        $sql->from($this->getDefinition()->query(Cdc_Definition::TYPE_RELATION)->fetch(Cdc_Definition::MODE_KEY_ONLY));
        $sql->where($args);

        return $sql;
    }

    public function getDefinition()
    {
        if (!$this->definition)
        {
            throw new Cdc_Exception_DefinitionNotFound;
        }
        $this->definition->reset();
        return $this->definition;
    }

    public function setDefinition(Cdc_Definition $definition)
    {
        $definition->reset();
        $this->definition = $definition;
    }

    public function fetchKeyValue($select_args)
    {

        if ($select_args instanceof Cdc_Sql_Select)
        {
            $sql = $select_args;
        }
        else
        {
            $sql = new Cdc_Sql_Select($this->getPdo());

            foreach ($select_args as $key => $value)
            {
                $sql->$key = $value;
            }
        }

        return $sql->stmt()->fetchAll(PDO::FETCH_KEY_PAIR);
    }

    // If this returns false, Cdc_Table will skip the row output
    public function getHtmlOptions($row, $rowset, $args)
    {
        if (!$this->allow($row))
        {
            return '';
        }

        $return    = '';
        $link_args = array(
            'r'               => $args['controller']->relation
        );
        $link_http_params = array(
            'op'                         => 'update',
            $args['controller']->primary => $row[$args['controller']->primary],
        );
        $return .= '<a class="btn" rel="tooltip" title="Editar" href="' . $args['controller']->link('admin', $link_args, $link_http_params) . '"><i class="fa fa-pencil"></i></a>';

        $link_http_params['op'] = 'delete';
        $return .= '<a class="btn" rel="tooltip" title="Excluir" href="' . $args['controller']->link('admin', $link_args, $link_http_params) . '"><i class="fa fa-trash-o"></i></a>';

        return '<div class="btn-group">' . $return . '</div>';
    }

    public function allow($row = array(), $rowset = array(), $index = null, $args = array())
    {
        return true;
    }

    public function getDefaultSequenceName()
    {

        $pdo    = $this->getPdo();
        $driver = $pdo->getAttribute(PDO::ATTR_DRIVER_NAME);

        if ($driver == 'pgsql')
        {
            $rel     = $this->getDefinition()->query(Cdc_Definition::TYPE_RELATION)->fetch(Cdc_Definition::MODE_SINGLE);
            $primary = $this->getDefinition()->query(Cdc_Definition::TYPE_COLUMN)->byKey('primary')->fetch(Cdc_Definition::MODE_SINGLE);
            return $rel . '_' . $primary . '_seq';
        }
        return null;
    }

}
