<?php

class Cdc_ArrayHelper
{

    public static function current($array)
    {
        $c = current($array);

        if (false == $c)
        {
            return array();
        }

        return $c;
    }

    public static function flatten($array, $prefix = '')
    {
        $result = array();

        foreach ($array as $key => $value)
        {
            $new_key = $prefix . (empty($prefix) ? '' : '[') . $key . (empty($prefix) ? '' : ']');

            if (is_array($value))
            {
                $result = array_merge($result, self::flatten($value, $new_key));
            }
            else
            {
                $result[$new_key] = $value;
            }
        }

        return $result;
    }

    public static function unflatten($array)
    {
        $keys = array_keys($array);
        $result = array();
        $vars = get_defined_vars();
        foreach ($array as $key => $value)
        {
            $varName = str_replace(array('[', ']'), array('[\'', '\']'), $key);
            eval("\$$varName = \$value;");
        }
        unset($value, $key, $varName);
        $result = array_diff_key(get_defined_vars(), $vars);
        unset($result['vars']);
        return $result;
    }

    public static function pluck($result, $key)
    {
        $return = array();
        foreach ($result as $k => $item)
        {
            $return[$k] = $item[$key];
        }

        return $return;
    }



    /**
     * Dá um PDO::FETCH_KEY_PAIR
     * @param mixed $result
     * @param string $key nome da coluna
     * @return array
     */
    public static function keyPair($result, $key = null)
    {
        if (!$result)
        {
            return array();
        }

        $first = reset($result);
        if (!is_array($first))
        {
            return array($first => $result);
        }


        if (!$key)
        {
            $key = key($first);
        }

        $return = array();
        foreach ($result as $t)
        {
            $return[$t[$key]] = $t;
        }
        return $return;
    }

    /**
     * array_merge_recursive does indeed merge arrays, but it converts values with duplicate
     * keys to arrays rather than overwriting the value in the first array with the duplicate
     * value in the second array, as array_merge does. I.e., with array_merge_recursive,
     * this happens (documented behavior):
     *
     * array_merge_recursive(array('key' => 'org value'), array('key' => 'new value'));
     *     => array('key' => array('org value', 'new value'));
     *
     * array_merge_recursive_distinct does not change the datatypes of the values in the arrays.
     * Matching keys' values in the second array overwrite those in the first array, as is the
     * case with array_merge, i.e.:
     *
     * array_merge_recursive_distinct(array('key' => 'org value'), array('key' => 'new value'));
     *     => array('key' => 'new value');
     *
     * Parameters are passed by reference, though only for performance reasons. They're not
     * altered by this function.
     *
     * @param array $array1
     * @param mixed $array2
     * @author daniel@danielsmedegaardbuus.dk
     * @return array
     */
    public static function array_merge_recursive_distinct(array &$array1, &$array2 = null)
    {
        $merged = $array1;

        if (is_array($array2))
            foreach ($array2 as $key => $val)
                if (is_array($array2[$key]))
                    $merged[$key] = (isset($merged[$key]) && is_array($merged[$key])) ? self::array_merge_recursive_distinct($merged[$key], $array2[$key]) : $array2[$key];
                else
                    $merged[$key] = $val;

        return $merged;
    }

}