<?php

class Cdc_Upload
{

    public static function createBuffer($fileName)
    {
        return WideImage::loadFromFile(Config::$upload_abs . $fileName);
    }

    public static function createInstances($file, array $instances, $create_preview = true)
    {
        $ext = pathinfo($file['nome'], PATHINFO_EXTENSION);
        $is_image = in_array($ext, array('jpg', 'jpeg', 'png', 'gif'));

        if (!$is_image || empty($instances))
        {
            return array();
        }

        $buffer = self::createBuffer($file['nome']);

        if ($create_preview)
        {
			$instances['__preview__']['crop'] = ['width' => 24, 'height' => 24];
			$instances['__thumbnail__']['crop'] = ['width' => 150, 'height' => 150];
        }

        $report = array();

        foreach ($instances as $key => $i)
        {
            $dir = Config::$upload_abs . $key;
            if (!file_exists($dir))
            {
                mkdir($dir, '0755');
            }
            $fileName = $dir . DIRECTORY_SEPARATOR . $file['nome'];
            $report[$key] = self::createInstance($buffer, $i, $key, $fileName);
        }

        return $report;
    }

    public static function resize(&$buffer, $instanceConfig, $index, $w, $h)
    {
        $args = array(
            'width' => null,
            'height' => null,
            'fit' => 'inside',
            'scale' => 'any',
        );

        $args = array_merge($args, $instanceConfig['resize']);

        $buffer = call_user_func_array(array($buffer, 'resize'), $args);

        return $args;
    }

    public static function crop(&$buffer, $instanceConfig, $index, $w, $h)
    {
        $args = $instanceConfig['crop'];

        if (!f($args, 'width') && !f($args, 'height'))
        {
            throw new Cdc_Exception_Upload_Crop;
        }
        
        $razao_original = $w / $h;
        $razao_recorte = $args['width'] / $args['height'];
        
        $resize = [];
        
        if($razao_original <= $razao_recorte)
        {
            $resize['width'] = $args['width'];
            if($w < $args['width'])
            {
                $resize['fit'] = 'outside';
            }
        }
        else
        {
            $resize['height'] = $args['height'];
            if($h < $args['height'])
            {
                $resize['scale'] = 'up';
            }
        }
        
        if(empty($resize))
        {
            $resize = $args;
        }

        $defaults = array(
            'width' => null,
            'height' => null,
            'fit' => 'inside',
            'scale' => 'any',
        );
        
        $resize = array_merge($defaults, $resize);

        $buffer = call_user_func_array(array($buffer, 'resize'), $resize);

        $defaults = array(
            'left' => 'center',
            'top' => 'center',
            'width' => null,
            'height' => null,
        );
        $crop = array_merge($defaults, $args);

        $buffer = call_user_func_array(array($buffer, 'crop'), $crop);
        
        return $args;
    }

    public static function createInstance(WideImage_Image $buffer, $instanceConfig, $index, $destination)
    {
        $result = array();

        $h = $buffer->getHeight();
        $w = $buffer->getWidth();

        if (isset($instanceConfig['crop']))
        {
            $result['crop'] = self::crop($buffer, $instanceConfig, $index, $w, $h);
        }

        $buffer->saveToFile($destination);

        return $result;

    }

    public static function saveFile($files, $index, $dir)
    {
        if (!file_exists($dir))
        {
            mkdir($dir, '0755');
        }
        $nome_original = $files[$index]['name'];
        $extension     = pathinfo($nome_original, PATHINFO_EXTENSION);
        $novo_nome     = Cdc_Vendor_UUID::mint(5, uniqid(null, true) . $nome_original . uniqid(null, true), Cdc_Vendor_UUID::nsOID) . '.' . mb_strtolower($extension);
        $tempFile      = $files[$index]['tmp_name'];
        $targetFile    = $dir . $novo_nome;

        if (!copy($tempFile, $targetFile))
        {
            throw new Cdc_Exception_Upload;
        }

        return array(
            'nome_original' => $nome_original,
            'nome' => $novo_nome,
            'titulo' => pathinfo($nome_original, PATHINFO_FILENAME),
            'criado' => date('Y-m-d H:i:s'),
        );
    }

    public static function validateFileType(&$file, $pdo, $model = null)
    {
        if ($model)
        {
            $q = $model->createQuery();
            $types = current($model->hydrateResultOf($q));
        }
        else
        {
            $types = $pdo->query('select id, nome, extensoes from arquivo_tipo')->fetchAll();
        }

        if (empty($types))
        {
            throw new Cdc_Exception_Upload_UnkownFileType;
        }

        $extension = pathinfo($file['nome'], PATHINFO_EXTENSION);

        $filetype = null;

        foreach ($types as $t)
        {
            if (false !== stristr($t['extensoes'], $extension))
            {
                $filetype = $t['id'];
                break;
            }
        }


        if (!$filetype)
        {
            throw new Cdc_Exception_Upload_UnknownFileType;
        }

        $file['arquivo_tipo_id'] = $filetype;
    }

}