<?php

/**
 * Cdc Toolkit
 *
 * Copyright 2012 Eduardo Marinho
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * 	 http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * @author Eduardo Marinho
 * @package Cdc
 *
 *
 * Based on third-party code found on
 * https://gist.github.com/221634
 * Original file header:
 *
 *
 * SplClassLoader implementation that implements the technical interoperability
 * standards for PHP 5.3 namespaces and class names.
 *
 * http://groups.google.com/group/php-standards/web/final-proposal
 *
 * // Example which loads classes for the Doctrine Common package in the
 * // Doctrine\Common namespace.
 * $classLoader = new SplClassLoader('Doctrine\Common', '/path/to/doctrine');
 * $classLoader->register();
 *
 * @author Jonathan H. Wage <jonwage@gmail.com>
 * @author Roman S. Borschel <roman@code-factory.org>
 * @author Matthew Weier O'Phinney <matthew@zend.com>
 * @author Kris Wallsmith <kris.wallsmith@gmail.com>
 * @author Fabien Potencier <fabien.potencier@symfony-project.org>
 */
class Cdc_Loader
{

    private $namespaces;

    public function addNamespace($namespace, $path, $separator = '_', $extension = '.php')
    {
        $this->namespaces[$namespace] = compact('path', 'separator', 'extension');
        return $this;
    }

    /**
     * Installs this class loader on the SPL autoload stack.
     */
    public function register()
    {
        spl_autoload_register(array($this, 'loadClass'));
    }

    /**
     * Uninstalls this class loader from the SPL autoloader stack.
     */
    public function unregister()
    {
        spl_autoload_unregister(array($this, 'loadClass'));
    }

    /**
     * Loads the given class or interface.
     *
     * @param string $className The name of the class to load.
     * @return void
     */
    public function loadClass($className)
    {
        foreach ($this->namespaces as $key => $ns)
        {
            $tok = substr($className, 0, strpos($className, $ns['separator']));

            if (($tok == $key) || !$tok || !$key)
            {
                $fileName  = str_replace($ns['separator'], DIRECTORY_SEPARATOR, $className) . $ns['extension'];

                $fileName = ($ns['path'] !== null ? $ns['path'] : '') . $fileName;

                if (Cdc_Core::isReadable($fileName))
                {
                    include $fileName;
                    return;
                }
            }
        }
    }

}