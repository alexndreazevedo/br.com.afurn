<?php

class Cdc_CellDataFormatter
{

    public static function boolean($row, $rowset, $index)
    {
        return '<td class="' . $index . '">' . ($row[$index] ? '<span class="label label-success">Sim</span>' : '<span class="label label-important">Não</span>') . '</td>';
    }

    public static function arrayValue($row, $rowset, $index, $args)
    {
        return '<td class="' . $index . '">' . $args[0][$row[$index]] . '</td>';
    }

    public static function formatArray($row, $rowset, $index, $args)
    {
        if (array_key_exists(1, $args))
        {
            $separator = $args[1];
        }
        else
        {
            $separator = '; ';
        }
        if ($row[$index])
        {
            $titles = Cdc_ArrayHelper::pluck(current($row[$index]), $args[0]);
            $data   = implode($separator, $titles);
        }
        else
        {
            $data = '';
        }

        return '<td class="' . $index . '">' . $data . '</td>';
    }

    /**
     * Args: format
     *
     */
    public static function formatDate($row, $rowset, $index, $args)
    {

        $data = date(current($args), strtotime(str_replace('/', '-', $row[$index])));

        return '<td class="' . $index . '">' . $data . '</td>';
    }

    /**
     * Args: prefix, thousand, decimal
     *
     */
    public static function formatNumber($row, $rowset, $index, $args)
    {
        if (isset($args[0]))
        {
            $prefix = $args[0];
        }
        else
        {
            $prefix = '';
        }
        if (isset($args[1]))
        {
            $decimals = $args[1];
        }
        else
        {
            $decimals = 2;
        }
        if (isset($args[2]))
        {
            $dec_point = $args[2];
        }
        else
        {
            $dec_point = ',';
        }

        if (isset($args[3]))
        {
            $thousands_sep = $args[3];
        }
        else
        {
            $thousands_sep = '.';
        }

        if (isset($args[4]))
        {
            $suffix = $args[4];
        }
        else
        {
            $suffix = '';
        }

        $data = $prefix . number_format($row[$index], $decimals, $dec_point, $thousands_sep) . $suffix;

        return '<td class="' . $index . '">' . $data . '</td>';
    }

}
