<?php

class Cdc_Dispatcher
{

    /**
     *
     * @var Router
     */
    public $router;

    /**
     *
     * @var Route
     */
    public $matchedRoute;

    public function __construct(Cdc_Router $router, Cdc_Route $matched_route)
    {
        $this->router = $router;
        $this->matchedRoute = $matched_route;
    }

    public function getRouter()
    {
        return $this->router;
    }

    public function setRouter($router)
    {
        $this->router = $router;
    }

    public function getMatchedRoute()
    {
        return $this->matchedRoute;
    }

    public function setMatchedRoute($matchedRoute)
    {
        $this->matchedRoute = $matchedRoute;
    }


    public function dispatch($args)
    {
        $target = $this->matchedRoute->getTarget();

        if (array_key_exists('class', $target))
        {
            $class = $target['class'];

            return new $class($args);
        }

        throw new Cdc_Exception_Dispatch;

    }

}
