<?php
/**
 * Cdc Toolkit
 *
 * Copyright 2012 Eduardo Marinho
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *	 http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * @author Eduardo Marinho
 * @package Cdc
 * @subpackage Cdc_Rule
 */

class Cdc_Rule_Date extends Cdc_Rule_Abstract
{

    protected $_format = 'Y-m-d H:i:s';

    public function __construct($format)
    {
        if ($format)
        {
            $this->_format = $format;
        }
    }

    public function check($index, &$row, $definition = null, $rowset = array())
    {

        if (!$row[$index])
        {
            $row[$index] = null;
            return array();
        }

        // $row[$index] = date($this->_format, strtotime(str_replace('/', '-', $row[$index])));

        if (!Zend_Date::isDate($row[$index], $this->_format))
        {
            return array('Data inválida.');
        }


        return array();
    }

}
