<?php
class Cdc_Template
{
    public static function link($url, $content, $title = null)
    {
        if (null !== $title)
        {
            $title = ' title="' . r($title) . '"';
        }
        return '<a href="' . $url . '"' . $title . '>' . $content . '</a>';
    }


    public static function singleImage($item, $index, $usePreset = true, $title = null)
    {
        if (array_key_exists($index, $item))
        {
            if (!empty($item[$index]))
            {
                $img = current(current($item[$index]));

                if (null == $title)
                {
                    $title = $img['titulo'];
                }

                if ($usePreset)
                {
                    $path = $index . '/';
                }
                else
                {
                    $path = null;
                }

                return '<img src="' . Config::$upload . $path . $img['nome'] . '"' . ' alt="' . r($title) . '">';
            }
        }

        return null;
    }

}