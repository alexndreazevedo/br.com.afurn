<?php

class Cdc_Pager
{

    public static function getFrom($total, $page, $limit)
    {
        if (!$page)
        {
            return 0;
        }
        if (!$total)
        {
            return 0;
        }
        $result = (($page - 1) * $limit) + 1;
        if ($result == 0)
        {
            return 1;
        }
        return $result;
    }

    public static function getTo($total, $page, $limit)
    {
        if (!$page)
        {
            $page = 1;
        }
        $n    = $page * $limit;
        if ($n > $total)
        {
            return $total;
        }
        return $n;
    }

    public static function renderSimple($base_url, $total, $page, $limit = null, $max_visible = 6, $titles = array(), $noempty = false, $page_var = 'p')
    {
        if (!$total)
        {
            return '';
        }
        self::filter($total, $page, $limit);

        $pages = ceil($total / $limit);

        if ($noempty && ($pages < 2))
        {
            return '';
        }

        if ($page > $pages)
        {
            $page = $pages;
        }

        $url = parse_url($base_url);

        $q = array();
        if (isset($url['query']))
        {
            parse_str($url['query'], $q);
        }
        $q[$page_var] = '';

        if (isset($url['fragment']))
        {
            $fragment = '#' . $url['fragment'];
        }
        else
        {
            $fragment = null;
        }

        $defaultTitles = array(
            'first'    => 'Primeira',
            'previous' => 'Anterior',
            'next'     => 'Próxima',
            'last'     => 'Última'
        );


        $titles = array_merge($defaultTitles, $titles);

        $url     = $url['path'] . '?' . http_build_query($q) . $fragment;
        $paginas = array();

        $disabledCounter = 1;

        if ($page <= 1)
        {
            $paginas['disabled-' . $disabledCounter++] = '<a href="#">' . $titles['first'] . '</a>';
            $paginas['disabled-' . $disabledCounter++] = '<a href="#">' . $titles['previous'] . '</a>';
        }
        else
        {
            $paginas[] = '<a href="' . $url . 1 . '">' . $titles['first'] . '</a>';
            $paginas[] = '<a href="' . $url . ($page - 1) . '">' . $titles['previous'] . '</a>';
        }

        $diff = (int) !($max_visible % 2);

        if ($diff)
        {
            $half = ceil($max_visible / 2);
        }
        else
        {
            $half = floor($max_visible / 2);
        }


        $slide = $half - $diff;

        $inicial = $page - $slide;
        $final   = $page + $slide + $diff;

        if ($inicial <= 1)
        {
            if ($inicial != 1)
            {
                $inicial = abs($inicial) + 1;
            }
            else
            {
                $inicial = 0;
            }
            $final += $inicial;
            $inicial = 1;
        }

        if ($final >= $pages)
        {
            $inicial -= $final - $pages;
            $final = $pages;
        }

        if ($inicial < 1)
        {
            $inicial = 1;
        }

        for ($i = $inicial; $i <= $final; $i++)
        {
            if ($i == $page)
            {
                $paginas['active'] = '<a href="#">' . $i . '</a>';
            }
            else
            {
                $paginas[] = '<a href="' . $url . $i . '">' . $i . '</a>';
            }
        }

        if ($page == $pages)
        {
            $paginas['disabled-' . $disabledCounter++] = '<a href="#">' . $titles['next'] . '</a>';
            $paginas['disabled-' . $disabledCounter++] = '<a href="#">' . $titles['last'] . '</a>';
        }
        else
        {
            $paginas[] = '<a href="' . $url . ($page + 1) . '">' . $titles['next'] . '</a>';
            $paginas[] = '<a href="' . $url . ($pages) . '">' . $titles['last'] . '</a>';
        }

        $first   = 'first';
        $last    = 'last';
        $class   = '';
        $saida   = '';
        $counter = 1;
        $count   = count($paginas);
        $active = 1;
        $total   = 0;
        foreach ($paginas as $k => $v)
        {
            if (is_numeric($k) || $k == 'active')
            {
                $total++;
            }
        }
        foreach ($paginas as $k => $p)
        {
            if ($counter == 1)
            {
                $class .= $first;
            }
            if ($counter == $count)
            {
                $class .= $last;
            }

            if (is_numeric($k) || $k == 'active')
            {
                if ($active == 1)
                {
                    $class .= ' first-enabled';
                }
                if ($active == $total)
                {
                    $class .= ' last-enabled';
                }
                $active++;
            }

            if ((string) $k == 'active')
            {
                // bug maldito
                $class .= ' active';
            }
            elseif (!is_numeric($k))
            {
                $class .= ' disabled';
            }

            $saida .= '<li class="' . $class . '">' . $p . '</li>';
            $class = '';
            $counter++;
        }

        return '<ul>' . $saida . '</ul>';
    }

    public static function render($base_url, $total, $page, $limit = null, $max_visible = 6, $titles = array(), $noempty = false, $page_var = 'p')
    {
        if (!$total)
        {
            return '';
        }
        self::filter($total, $page, $limit);

        $pages = ceil($total / $limit);

        if ($noempty && ($pages < 2))
        {
            return '';
        }

        if ($page > $pages)
        {
            $page = $pages;
        }

        $url = parse_url($base_url);

        $q = array();
        if (isset($url['query']))
        {
            parse_str($url['query'], $q);
        }
        $q[$page_var] = '';

        if (isset($url['fragment']))
        {
            $fragment = '#' . $url['fragment'];
        }
        else
        {
            $fragment = null;
        }

        $defaultTitles = array(
            'first'    => 'Primeira',
            'previous' => 'Anterior',
            'next'     => 'Próxima',
            'last'     => 'Última'
        );

        $titles = array_merge($defaultTitles, $titles);

        $url     = $url['path'] . '?' . http_build_query($q) . $fragment;
        $paginas = array();

        $disabledCounter = 1;

        if ($page <= 1)
        {
            $paginas['disabled-' . $disabledCounter++] = '<span class="pagerControl first"><i class="icon icon-fast-backward icon-white"></i><span class="pagerCaption">' . $titles['first'] . '</span></span>';
            $paginas['disabled-' . $disabledCounter++] = '<span class="pagerControl prev"><i class="icon icon-backward icon-white"></i><span class="pagerCaption">' . $titles['previous'] . '</span></span>';
        }
        else
        {
            $paginas[] = '<a class="pagerControl first" href="' . $url . 1 . '"><i class="icon icon-fast-backward"></i><span class="pagerCaption">' . $titles['first'] . '</span></a>';
            $paginas[] = '<a class="pagerControl prev" href="' . $url . ($page - 1) . '"><i class="icon icon-backward"></i><span class="pagerCaption">' . $titles['previous'] . '</span></a>';
        }

        $diff = (int) !($max_visible % 2);

        if ($diff)
        {
            $half = ceil($max_visible / 2);
        }
        else
        {
            $half = floor($max_visible / 2);
        }


        $slide = $half - $diff;

        $inicial = $page - $slide;
        $final   = $page + $slide + $diff;

        if ($inicial <= 1)
        {
            if ($inicial != 1)
            {
                $inicial = abs($inicial) + 1;
            }
            else
            {
                $inicial = 0;
            }
            $final += $inicial;
            $inicial = 1;
        }

        if ($final >= $pages)
        {
            $inicial -= $final - $pages;
            $final = $pages;
        }

        if ($inicial < 1)
        {
            $inicial = 1;
        }

        for ($i = $inicial; $i <= $final; $i++)
        {
            if ($i == $page)
            {
                $paginas['current'] = '<span class="pagerSeek current">' . $i . '</span>';
            }
            else
            {
                $paginas[] = '<a class="pagerSeek" href="' . $url . $i . '">' . $i . '</a>';
            }
        }


        if ($page == $pages)
        {
            $paginas['disabled-' . $disabledCounter++] = '<span class="pagerControl next"><span class="pagerCaption">' . $titles['next'] . '</span><i class="icon icon-forward icon-white"></i></span>';
            $paginas['disabled-' . $disabledCounter++] = '<span class="pagerControl last"><span class="pagerCaption">' . $titles['last'] . '</span><i class="icon icon-fast-forward icon-white"></i></span>';
        }
        else
        {
            $paginas[] = '<a class="pagerControl next" href="' . $url . ($page + 1) . '"><span class="pagerCaption">' . $titles['next'] . '</span><i class="icon icon-forward"></i></a>';
            $paginas[] = '<a class="pagerControl last" href="' . $url . ($pages) . '"><span class="pagerCaption">' . $titles['last'] . '</span><i class="icon icon-fast-forward"></i></a>';
        }

        $first   = 'first';
        $last    = 'last';
        $class   = '';
        $saida   = '';
        $counter = 1;
        $count   = count($paginas);
        $current = 1;
        $total   = 0;
        foreach ($paginas as $k => $v)
        {
            if (is_numeric($k) || $k == 'current')
            {
                $total++;
            }
        }
        foreach ($paginas as $k => $p)
        {
            if ($counter == 1)
            {
                $class .= $first;
            }
            if ($counter == $count)
            {
                $class .= $last;
            }

            if (is_numeric($k) || $k == 'current')
            {
                if ($current == 1)
                {
                    $class .= ' first-enabled';
                }
                if ($current == $total)
                {
                    $class .= ' last-enabled';
                }
                $current++;
            }

            if ((string) $k == 'current')
            {
                // bug maldito
                $class .= ' current';
            }
            elseif (!is_numeric($k))
            {
                $class .= ' disabled';
            }

            $saida .= '<li class="' . $class . '">' . $p . '</li>';
            $class = '';
            $counter++;
        }

        return '<ul>' . $saida . '</ul>';
    }

    public static function filter(&$total, &$page, &$limit)
    {
        $total = abs($total);
        $page  = abs($page);
        if (!$page)
        {
            $page  = 1;
        }
        $limit = abs($limit);
        if (!$limit)
        {
            $limit = 25;
        }
    }

    public static function roundLimit($limit)
    {

        if ($limit <= 25)
        {
            return 25;
        }
        if ($limit <= 50)
        {
            return 50;
        }
        elseif ($limit <= 100)
        {
            return 100;
        }
        return 150;
    }

}
