<?php

class Cdc_OutputFormatter
{

    public static function date($value, $attribs, $input, $def, $encode, $params)
    {
        if (!$value)
        {
            return $value;
        }
        if (isset($params[0]))
        {
            $dateFormat = $params[0];
        }
        else
        {
            $dateFormat = 'd/m/Y';
        }
        return date($dateFormat, strtotime(str_replace('/', '-', $value)));
    }

}
