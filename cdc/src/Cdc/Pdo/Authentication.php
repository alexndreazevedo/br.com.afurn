<?php
/**
 * Cdc Toolkit
 *
 * Copyright 2012 Eduardo Marinho
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * 	 http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * @author Eduardo Marinho
 * @package Cdc
 * @subpackage Cdc_Pdo
 */

/**
 * Autenticação genérica usando uma conexão PDO
 */
class Cdc_Pdo_Authentication extends Cdc_Pdo_Container
{

    /**
     * Hasher
     *
     * @var Cdc_Vendor_PasswordHash
     */
    protected $hasher;

    /**
     * Nome da tabela de usuários.
     *
     * Observe que nada impede injection de joins aqui.
     *
     * Tanto pode ser colocado apenas um nome de tabela como o nome
     * mais uma cláusula join completa.
     *
     * @var string
     */
    protected $table        = 'usuario';

    /**
     * Nome da coluna que serve como "login", o padrão é "email"
     *
     * @var string
     */
    protected $identity     = 'email';

    /**
     * Nome da coluna que guarda o hash de senha (60 caracteres)
     *
     * @var string
     */
    protected $password     = 'senha';

    /**
     * Nome da coluna de onde se extrairá a saudação inicial ao usuário
     *
     * Bem vindo, <fulano>. Fulano sai da coluna com o nome indicado aqui.
     * O padrão é "nome".
     *
     * @var string
     */
    protected $greeting     = 'nome';

    /**
     * Colunas extras para serem armazenadas na sessão, e que estão também na tabela de usuario.
     * @var array
     */
    protected $extraColumns = array('id', 'nome', 'administrador');

    /**
     * Nome da coluna que guarda um token que pode ser utilizado para recuperação de senha.
     * Deve ser varchar(72)
     * @var string
     */
    protected $token         = 'token';

    /*
     * Nome da coluna que guarda a data de expiração do token.
     *
     * @var string
     *
     */
    protected $tokenValidity = 'token_validade';

    /**
     * Nome da coluna auto_increment
     * @var string
     */
    protected $id            = 'id';

    /**
     * Construtor.
     *
     * @param object $pdo PDO ou outra classe que implemente a interface de PDO
     * @param Cdc_Vendor_PasswordHash $args
     */
    public function __construct($pdo = null, $args = null) {
        parent::__construct($pdo, $args);

        if (!$args)
        {
            $args = Config::$hasher;
        }

        $this->hasher = $args;
    }

    public function getHasher()
    {
        return $this->hasher;
    }

    public function getTable()
    {
        return $this->table;
    }

    public function setTable($table)
    {
        $this->table = $table;
    }

    public function getIdentity()
    {
        return $this->identity;
    }

    public function setIdentity($identity)
    {
        $this->identity = $identity;
    }

    public function getPassword()
    {
        return $this->password;
    }

    public function setPassword($password)
    {
        $this->password = $password;
    }

    public function getGreeting()
    {
        return $this->greeting;
    }

    public function setGreeting($greeting)
    {
        $this->greeting = $greeting;
    }

    public function getExtraColumns()
    {
        return $this->extraColumns;
    }

    public function setExtraColumns($extraColumns)
    {
        $this->extraColumns = $extraColumns;
    }

    public function getToken()
    {
        return $this->token;
    }

    public function setToken($token)
    {
        $this->token = $token;
    }

    public function getTokenValidity()
    {
        return $this->tokenValidity;
    }

    public function setTokenValidity($tokenValidity)
    {
        $this->tokenValidity = $tokenValidity;
    }

    public function getId()
    {
        return $this->id;
    }

    public function setId($id)
    {
        $this->id = $id;
    }

    protected function store($row)
    {
        $_SESSION['authenticated'] = session_name(); // proteção contra session hijack
        $_SESSION                  = array_merge($_SESSION, $row);
    }

    public static function isAuthenticated()
    {
        return array_key_exists('authenticated', $_SESSION) && ($_SESSION['authenticated'] === session_name());
    }

    public function reset()
    {
        $_SESSION = array();
    }

    public function logout($session_name = null, $session_cookie_params = null)
    {
        if (!$session_name)
        {
            $session_name = session_name();
        }
        if (!$session_cookie_params)
        {
            $session_cookie_params = session_get_cookie_params();
        }

        session_destroy();

        setcookie($session_name, null, time() - 3600, $session_cookie_params['path'], $session_cookie_params['domain'], $session_cookie_params['secure'], $session_cookie_params['httponly']);
    }

    public function login($identity, $password)
    {
        $columns = $this->getExtraColumns();

        $unset = array();

        if (false === array_search($this->getIdentity(), $columns))
        {
            $columns[] = $unset[]   = $this->getIdentity();
        }

        if (false === array_search($this->getPassword(), $columns))
        {
            $columns[] = $unset[]   = $this->getPassword();
        }



        $sql = vsprintf('select %s from %s where %s = ?', array(
            implode(', ', $columns),
            $this->getTable(),
            $this->getIdentity())
        );

        $stmt = $this->getPdo()->prepare($sql);
        $stmt->bindValue(1, $identity);

        $stmt->execute();

        $row = $stmt->fetch(PDO::FETCH_ASSOC);

        $result = $this->getHasher()->checkPassword($password, $row[$this->getPassword()]);

        if ($result)
        {
            foreach ($unset as $u)
            {
                unset($row[$u]);
            }

            $this->store($row);
        }

        return $result;
    }

    public function loginById($id)
    {
        $columns = $this->getExtraColumns();

        $unset = array();
        if (false === array_search($this->getId(), $columns))
        {
            $columns[] = $unset[]   = $this->getId();
        }

        $sql = vsprintf('select %s from %s where %s = ?', array(
            implode(', ', $columns),
            $this->getTable(),
            $this->getId())
        );

        $stmt = $this->getPdo()->prepare($sql);
        $stmt->bindValue(1, $id);

        $stmt->execute();

        $row = $stmt->fetch(PDO::FETCH_ASSOC);

        if (!$row)
        {
            return false;
        }

        foreach ($unset as $u)
        {
            unset($row[$u]);
        }

        $this->store($row);

        return true;
    }

    /**
     * Gera um token com 72 caracteres usando uma string qualquer como semente.
     *
     * A mesma semente não deve gerar o mesmo token duas vezes.
     *
     * @param string $seed
     * @return string
     */
    public static function createToken($seed, $hasher)
    {

        $crypt = $hasher->HashPassword($seed);
        $identifiersRemoved = substr($crypt, 7);
        return base64_encode($identifiersRemoved);
    }

    public function loginByIdAndToken($id, $token)
    {
        $columns = $this->getExtraColumns();

        $unset = array();
        if (false === array_search($this->getToken(), $columns))
        {
            $columns[] = $unset[]   = $this->getToken();
        }

        if (false === array_search($this->getId(), $columns))
        {
            $columns[] = $unset[]   = $this->getId();
        }

        $sql = vsprintf('select %s from %s where %s = ? and %s = ? and %s >= ?', array(
            implode(', ', $columns),
            $this->getTable(),
            $this->getToken(),
            $this->getId(),
            $this->getTokenValidity())
        );

        $stmt = $this->getPdo()->prepare($sql);
        $stmt->bindValue(1, $token);
        $stmt->bindValue(2, $id);
        $stmt->bindValue(3, time());

        $stmt->execute();

        $row = $stmt->fetch(PDO::FETCH_ASSOC);

        if (!$row)
        {
            return false;
        }

        foreach ($unset as $u)
        {
            unset($row[$u]);
        }

        $this->store($row);

        return true;
    }

}

