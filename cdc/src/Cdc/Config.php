<?php

/**
 * This would also work as simple global variables, but I need IDE autocomplete
 */
abstract class Cdc_Config
{

    public static $debug;
    public static $base_url;
    public static $upload;
    public static $upload_abs;
    public static $root;
    public static $root_abs;
    public static $baseTitle;
    public static $db;
    public static $default_db;
    public static $timezone;
    public static $vendor_abs;
    public static $cdc_abs;
    public static $loader;
    public static $modules;
    public static $resources;
    public static $session_params;
    public static $dispatcher;
    public static $labels;
    public static $hasher;
    public static $zend_abs;
    public static $wideimage_abs;
    public static $acl = array();
    public static $admin_base_route = 'admin';
    public static $auto_remove_magic_quotes = false;

    public static $default_user_data = array(
        'nome'  => 'Default System Administrator',
        'email' => 'admin@example.com',
        'senha' => 'changeme',
    );

    public static $default_login_controller = 'Base_Controller_Login';

    public static $default_user_smtp = array();

    public static $file_instances = array();
    public static $file_regions = array();

    private function __construct()
    {

    }

    public static function source($settings, $application = null)
    {
        Config::setEnvironment($settings, $application);

        include Config::$cdc_abs . 'Cdc/Core.php';
        include Config::$vendor_abs . 'autoload.php';

        self::$zend_abs = Config::$vendor_abs . 'zendframework/zendframework1/library/';

        set_include_path(get_include_path() . PATH_SEPARATOR . Config::$zend_abs);

        if (Config::$auto_remove_magic_quotes)
        {
            turn_off_magic_quotes();
        }

        Config::addDefaultLoaders();

        Config::registerDatabaseConnections();

        Config::addModuleLoaders();

        Config::loadModuleResources();

        self::$hasher = new Cdc_Vendor_PasswordHash;

        date_default_timezone_set(Config::$timezone);
    }

    public static function startSessionFor($controller = 'Base_Controller_Login', $auth = null)
    {
        if (!array_key_exists($controller, Config::$session_params))
        {
            throw new Cdc_Exception_SessionSettingsNotFound;
        }
        $settings = Config::$session_params[$controller];

        if (array_key_exists('name', $settings))
        {
            session_name($settings['name']);
            if (isset($_REQUEST[$settings['name']]))
            {
                session_id($_REQUEST[$settings['name']]);
            }
            unset($settings['name']);
        }

        $session_params = array_merge(session_get_cookie_params(), $settings);

        call_user_func_array('session_set_cookie_params', $session_params);

        session_start();

        Cdc_Core::boot();

        if ($auth)
        {
            call_user_func_array(array($controller, 'configureAuth'), array($auth));
        }

    }

    protected static function setEnvironment($settings, $application)
    {
        if (null !== $application)
        {
            if (!array_key_exists($application, $settings))
            {
                die('No settings found for application "' . $application . '".' . PHP_EOL);
            }
            $app_settings = $settings[$application];
            unset($settings[$application]);
            $settings     = Cdc_ArrayHelper::array_merge_recursive_distinct($settings, $app_settings);
        }

        foreach ($settings as $key => $value)
        {
            if (!property_exists('Config', $key))
            {
                throw new Exception('Please define and document the setting "' . $key . '" in your Config class.');
            }
            Config::$$key = $value;
        }
    }

    protected static function registerDatabaseConnections()
    {
        foreach (Config::$db as $key => $value)
        {
            Cdc_Pdo_Pool::register($key, $value['dsn'], $value['user'], $value['pass']);
        }
    }

    public static function connection()
    {
        if (Config::$debug)
        {
            $mode = Cdc_Pdo_Pool::DEBUG;
        }
        else
        {
            $mode = Cdc_Pdo_Pool::NORMAL;
        }
        return Cdc_Pdo_Pool::getConnection(Config::$default_db, $mode);
    }

    protected static function addDefaultLoaders()
    {
        Config::$loader->addNamespace('Cdc', Config::$cdc_abs);
        Config::$loader->addNamespace('Zend', Config::$zend_abs);
    }

    protected static function addModuleLoaders()
    {
        foreach (Config::$modules as $k => $v)
        {
            Config::$loader->addNamespace($k, $v);
        }
    }

    public static function configureRouter(Cdc_Router $router)
    {
        foreach (Config::$modules as $key => $value)
        {
            if ($key == '')
            {
                $sep = ''; // @TODO: this won't work with namespaces
            }
            else
            {
                $sep = '_';
            }

            $routes = $key . $sep . 'Metadata_Routes';

            if (class_exists($routes))
            {
                call_user_func(array($routes, 'setup'), $router);
            }
        }
    }

    protected static function loadModuleResources()
    {
        foreach (Config::$modules as $key => $value)
        {
            if ($key == '')
            {
                $sep = ''; // @TODO: this also won't work with namespaces
            }
            else
            {
                $sep = '_';
            }

            $resources = $key . $sep . 'Metadata_Resources';
            if (class_exists($resources))
            {
                call_user_func(array($resources, 'setup'));
            }
        }
    }

    public static function createAcl($pdo, $usuario_id)
    {

        if (!Config::$acl)
        {
            $grupos = $pdo->prepare('select grupo_id as id from usuario_grupo where usuario_id = ?');
            $grupos->bindValue(1, $usuario_id);
            try
            {
                $grupos->execute();
            }
            catch (PDOException $e)
            {
                return;
            }

            $grupos = $grupos->fetchAll(PDO::FETCH_COLUMN);

            if (!$grupos)
            {
                return;
            }

            $sql = new Cdc_Sql_Select($pdo);

            $sql->cols = array('recurso_id', 'recurso_id');
            $sql->from = array('grupo_recurso');
            $sql->where = array('grupo_id in' => $grupos);

            $result = $sql->stmt()->fetchAll(PDO::FETCH_KEY_PAIR);

            Config::$acl = array_intersect_key($result, Config::$resources);
        }
    }

    public static function resourceAllowed($resource, $skip = false)
    {
        if ($skip)
        {
            return true;
        }
        return isset(Config::$acl[$resource]);
    }

    public static function routeAllowed(Cdc_Route $route, $pdo, $usuario_id, $skip_check = false)
    {
        Config::createAcl($pdo, $usuario_id);

        if ($skip_check)
        {
            return true;
        }

        $target = $route->getTarget();

        if (!array_key_exists('resource', $target))
        {
            return false; // A pessoa tem que colocar explicitamente que a rota é aberta, usando o valor "none"
        }

        if ($target['resource'] == 'none')
        { // Recurso aberto
            return true;
        }

        if ($target['resource'] == 'authenticated')
        {
            // Apenas para usuários autenticados
            return Cdc_Pdo_Authentication::isAuthenticated();
        }

        if ($target['resource'] == 'auto')
        {
            $parameters         = $route->getParameters();
            $target['resource'] = $parameters['r'];
        }

        return array_key_exists($target['resource'], self::$acl);
    }

}
