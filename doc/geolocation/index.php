<!DOCTYPE html>
<html lang="pt-BR">
  <head>
    <meta charset="utf-8">
    <title>GeoLocation API</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
	<script type="text/javascript" src="http://maps.googleapis.com/maps/api/js?key=AIzaSyD3lueX0hUUeb7WEDaMmD3tn5YmcFDlcQg&sensor=false"></script>
	<script>
		var gl;

		function displayPosition(position) {
		  var p = document.getElementById("p");
		  p.innerHTML = '<table border="0"><tr><th>Timestamp</th><td>' + position.timestamp + '</td></tr>' + 
		  '<tr><th>Latitude (WGS84)</th><td>' + position.coords.latitude + '</td></tr>' +
		  '<tr><th>Longitude (WGS84)</th><td>' + position.coords.longitude + '</td></tr></table>' + 
	      '<img src="http://maps.googleapis.com/maps/api/staticmap?center=' + position.coords.latitude + ',' + position.coords.longitude + '&zoom=16&scale=2&size=300x400&markers=color:orange|label:R|' + position.coords.latitude + ',' + position.coords.longitude + '&sensor=false" width="100%" />';
		}

		function displayError(positionError) {
		  alert("error");
		}

		try {
		  if (typeof navigator.geolocation === 'undefined'){
			gl = google.gears.factory.create('beta.geolocation');
		  } else {
			gl = navigator.geolocation;
		  }
		} catch(e) {}

		if (gl) {
		  gl.getCurrentPosition(displayPosition, displayError);
		} else {
		  alert("Geolocation services are not supported by your web browser.");
		}
	</script>
</head>
<body>
	<div id="p"></div>
</body>
</html>