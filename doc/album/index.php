﻿<?php
header('Content-type: text/html; charset=UTF-8');

include realpath(dirname(dirname(__DIR__))) . DIRECTORY_SEPARATOR . 'init.php';

require Config::$vendor_abs . 'facebook' . DIRECTORY_SEPARATOR . 'php-sdk' . DIRECTORY_SEPARATOR . 'src' . DIRECTORY_SEPARATOR . 'facebook.php';

$controller = new Controller;

$appId = $controller->appId;
$appSecret = $controller->appSecret;

$query = array();
$limit = 30;

$page_id = filter_input(INPUT_GET, 'page_id');
$other = filter_input(INPUT_GET, 'other');
$page_id = (empty($page_id) or $page_id == 'outra') ? $other : $page_id;

$album_id = filter_input(INPUT_GET, 'album_id');
$p = (int) filter_input(INPUT_GET, 'p');

$p = ($p <= 0) ? 1 : $p;
$offset = ($p - 1) * $limit;

$pages = array(
	'nytimes' => 'The New York Times',
	'folhadesp' => 'Folha de São Paulo',
	'mashable' => 'Mashable',
	'revistainfo' => 'INFO',
	'PortaliMasters' => 'iMasters',
	'CodeSchool' => 'Code School',
	'criatives' => 'Criatives',
	'dcriativo' => 'Desafio Criativo',
	'comunicadores.info' => 'Comunicadores',
	'Publistagram' => 'Publistagram',
	'vidadeprogramador.com.br' => 'Vida de Programador',
	'tirinhassuporte' => 'Vida de Suporte',
	'EuFacoprogramas' => 'Eu Faço Programas',
	'awwwards' => 'Awwwards',
	'DepositoDeTirinhas' => 'Depósito de Tirinhas',
	'oficinadanet' => 'Oficina da Net',
	'CSSDesignAwards' => 'CSS Design Awards',
);

?>
<!DOCTYPE html>
<html lang="pt-BR">
  <head>
    <meta charset="utf-8">
    <title>Facebook API - Álbum</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">

    <link href="http://twitter.github.com/bootstrap/assets/css/bootstrap.css" rel="stylesheet">
    <link href="http://twitter.github.com/bootstrap/assets/css/bootstrap-responsive.css" rel="stylesheet">
    <link href="http://lokeshdhakar.com/projects/lightbox2/css/lightbox.css" rel="stylesheet">

    <script src="http://code.jquery.com/jquery-1.10.2.min.js"></script>
    <script src="http://lokeshdhakar.com/projects/lightbox2/js/lightbox-2.6.min.js"></script>
	<script>
		$(document).ready(function(){
			$('#page_id select').change(function(){
				if($('#page_id option:selected').val() == 'outra')
				{
					$('#outra').show().find('input[type=text]').focus();
					$('#page_id label').text('Opções');
				}
				else
				{
					$('#outra').hide();
					$('#page_id label').text('Página');
				}
			});
			$('#outra input[type=text]').keyup(function(){
				if($(this).val() == '')
				{
					$('#page_id').show();
				}
				else
				{
					$('#page_id').hide();
				}
			});
			$('#outra input[type=text]').blur(function(){
				if($(this).val() == '')
				{
					$('#page_id').show();
				}
				else
				{
					$('#page_id').hide();
				}
			});
		});
	</script>
</head>
<body>
	<div class="container">
		<div class="row">
		  <div class="span">
			<h3>Facebook API - Álbum</h3>
                <div class="row">
                    <hr>
                    <div class="span12">
                        <fieldset>
							<?php
								try{
									$facebook = new Facebook(array(
										'appId'  => $appId,
										'secret' => $appSecret,
										'cookie' => true,
									));

									echo '<legend>Selecione uma página</legend>';
									?>
									<form method="GET" class="form-horizontal">
										<div class="control-group" id="page_id">
											<label class="control-label" for="formacao">Página</label>
											<div class="controls">
												<select name="page_id">
													<?php
													
														foreach($pages as $index => $name)
														{
															echo '<option value="' . $index . '"';
															
															if($page_id == $index)
															{
																echo ' selected="selected"';
															}
															
															echo '>' . $name . '</option>';
														}
													?>
													<option value="outra">Selecionar manualmente...</option>
												</select>
											</div>
										</div>
										<div class="control-group" id="outra" style="display: none">
											<label class="control-label" for="formacao">Página</label>
											<div class="controls">
												<div class="input-prepend">
													<span class="add-on"><b class="text-info">&nbsp;&nbsp;facebook.com&nbsp;</b></span>
													<input type="text" name="other" value="" class="span4" placeholder="Endereço da Página">
												</div>
											</div>
										</div>
										<div class="modal-footer">
											<input type="submit" value="Pronto!" class="btn btn-large btn-primary">
										</div>
									</form>
											
									<?php
									
									if(!empty($page_id))
									{

										$query['page'] = $facebook->api('/' . $page_id . '?fields=id,name,username,likes,link,picture,albums.fields(id,name,cover_photo,place,location,count,type)&locale=pt_BR');
										
										$page = $query['page'];
										
										echo '<legend><img src="' . $page['picture']['data']['url'] . '" width="30">&nbsp;<a href="?page_id=' . $page_id . '">' . $page['name'] . '</a><small class="muted">&nbsp;[<a href="https://facebook.com/' . $page['username'] . '" target="_blank">facebook.com/' . $page['username'] . '</a>]</small></legend>';
										
										if(empty($album_id))
										{
											if(!isset($query['page']['albums']['data']))
											{
												throw new Exception('Sem acesso ao álbum de fotos.');
											}
											
											$albuns = $query['page']['albums']['data'];
											
											echo '<blockquote><p>Álbuns de ' . $page['name'] . '</p></blockquote>';

											$chunks = array_chunk($albuns, 6);
											
											foreach($chunks as $chunk)
											{
												echo '<ul class="thumbnails">';
												
												foreach($chunk as $album)
												{
													if(!isset($album['cover_photo']))
													{
														throw new Exception('Sem acesso ao álbum de fotos.');
													}
												
													$query['photos'] = $facebook->api('/' . $album['cover_photo'] . '?fields=id,name,picture,source&locale=pt_BR');
												
													$photo = $query['photos'];
													$count = $album['count'];

													if(!array_key_exists('name', $photo))
													{
														$photo['name'] = '';
													}

													echo '<li class="span2">';
													echo '<div class="thumbnail">';
													echo '<a href="?page_id=' . $page_id . '&album_id=' . $album['id'] . '" title="' . $photo['name'] . '">';
													echo '<img src="http://upload.wikimedia.org/wikipedia/commons/c/ce/Transparent.gif" width="100%" style="background-image:url(\'' . $photo['picture'] . '\')" />';
													echo '</a>';
													echo '<a href="?page_id=' . $page_id . '&album_id=' . $album['id'] . '"><small><b>' . $album['name'] . '</b></small></a>';
													echo '&nbsp;<small class="muted">' . $count . ' ' . (($count == 1) ? 'foto' : 'fotos') . '</small><br>';
													echo '</div></li>';
												}
												
												echo '</ul>';
											}
										}
										else
										{
											$query['photos'] = $facebook->api('/' . $album_id . '?fields=name,description,photos.fields(name,picture,source,icon,place,position,created_time).limit(' . $limit . ').offset(' . $offset . '),count&locale=pt_BR');
											
											$album = $query['photos']['name'];
											$photos = $query['photos']['photos']['data'];
											$count = $query['photos']['count'];
											
											if(!isset($query['photos']['photos']['data']))
											{
												throw new Exception('Sem acesso às fotos.');
											}
											
											echo '<blockquote><p>' . $album . '</p>';
											
											$description = (isset($query['photos']['description'])) ? '<small>' . $query['photos']['description'] . '</small>' : '';
											
											echo '</blockquote>';

											$chunks = array_chunk($photos, 6);

											foreach($chunks as $chunk)
											{
												echo '<ul class="thumbnails">';
												
												foreach($chunk as $photo)
												{
													if(!array_key_exists('name', $photo))
													{
														$photo['name'] = '';
													}
													echo '<li class="span2">';
													echo '<div class="thumbnail">';
													echo '<a href="' . $photo['source'] . '" title="' . $photo['name'] . '" data-lightbox="' . $album_id . '">';
													echo '<img src="http://upload.wikimedia.org/wikipedia/commons/c/ce/Transparent.gif" width="100%" style="background-image:url(\'' . $photo['picture'] . '\')" />';
													echo '</a>';
													
													// Servidor sem suporte | Disponivel a partir do PHP 5.3
													/*
													$since = new DateTime($photo['created_time']);
													$today = new DateTime();
													
													$diff = $since->diff($today);
													
													if($diff->y > 1) {
														$interval = $diff->y . ' anos';
													} elseif($diff->y > 0) {
														$interval = $diff->y . ' ano';
													} elseif($diff->y == 0 and $diff->m > 1) {
														$interval = $diff->m . ' meses';
													} elseif($diff->y == 0 and $diff->m > 0) {
														$interval = $diff->m . ' mês';
													} elseif($diff->y == 0 and $diff->m == 0 and $diff->d > 7) {
														$interval = floor($diff->d / 7) . ' semanas';
													} elseif($diff->y == 0 and $diff->m == 0 and $diff->d > 1) {
														$interval = $diff->d . ' dias';
													} elseif($diff->y == 0 and $diff->m == 0 and $diff->d > 0) {
														$interval = $diff->d . ' dia';
													} elseif($diff->y == 0 and $diff->m == 0 and $diff->d == 0 and $diff->h > 1) {
														$interval = $diff->h . ' horas';
													} elseif($diff->y == 0 and $diff->m == 0 and $diff->d == 0 and $diff->h > 0) {
														$interval = $diff->h . ' hora';
													} elseif($diff->y == 0 and $diff->m == 0 and $diff->d == 0 and $diff->h == 0 && $diff->i > 1) {
														$interval = $diff->i . ' minutos';
													} elseif($diff->y == 0 and $diff->m == 0 and $diff->d == 0 and $diff->h == 0 && $diff->i > 0) {
														$interval = $diff->i . ' minuto';
													} elseif($diff->y == 0 and $diff->m == 0 and $diff->d == 0 and $diff->h == 0 && $diff->i == 0 && $diff->s > 1) {
														$interval = $diff->s . ' segundos';
													} elseif($diff->y == 0 and $diff->m == 0 and $diff->d == 0 and $diff->h == 0 && $diff->i == 0 && $diff->s > 0) {
														$interval = ' alguns instantes';
													}
													
													echo '<img src="' . $photo['icon'] . '" style="display: inline; margin-right: 5px;"><small><abbr title="' . $since->format('d/m/Y') . ' às ' . $since->format('H:i:s') . '" class="initialism muted">há ' . $interval . '</abbr></small>';
													*/
													
													echo '</div></li>';
												}
											
												echo '</ul>';
											}
											
											if($count > $limit)
											{
												$total = ceil($count / $limit);
												$items = 15;
												
												echo '<div class="pagination"><ul>';
												echo '<li><a href="?page_id=' . $page_id . '&album_id=' . $album_id . '&p=1">Primeiro</a></li>';

												if($p > 1)
												{
													echo '<li><a href="?page_id=' . $page_id . '&album_id=' . $album_id . '&p=' . ($p - 1) . '">&#171; Anterior</a></li>';
												}
												
												for($i = 1; $i <= $total; $i++)
												{
													if($p < (($items / 2) + 1))
													{
														if($i == $p)
														{
															echo '<li><a href="?page_id=' . $page_id . '&album_id=' . $album_id . '&p=' . $i . '"><b>' . $i . '</b></a></li>';
														}
														elseif($i <= $items)
														{
															echo '<li><a href="?page_id=' . $page_id . '&album_id=' . $album_id . '&p=' . $i . '">' . $i . '</a></li>';
														}
													}
													elseif($p > ($total - ($items / 2) - 1))
													{
														if($i == $p)
														{
															echo '<li><a href="?page_id=' . $page_id . '&album_id=' . $album_id . '&p=' . $i . '"><b>' . $i . '</b></a></li>';
														}
														elseif($i >= ($total - $items))
														{
															echo '<li><a href="?page_id=' . $page_id . '&album_id=' . $album_id . '&p=' . $i . '">' . $i . '</a></li>';
														}
													}
													else
													{
														if($i == $p)
														{
															echo '<li><a href="?page_id=' . $page_id . '&album_id=' . $album_id . '&p=' . $i . '"><b>' . $i . '</b></a></li>';
														}
														elseif($i > ($p - ($items / 2)) and $i < ($p + ($items / 2)))
														{
															echo '<li><a href="?page_id=' . $page_id . '&album_id=' . $album_id . '&p=' . $i . '">' . $i . '</a></li>';
														}
													}
												}

												if(($p + 1) <= $total)
												{
													echo '<li><a href="?page_id=' . $page_id . '&album_id=' . $album_id . '&p=' . ($p + 1) . '">Próximo &#187;</a></li>';
												}

												echo '<li><a href="?page_id=' . $page_id . '&album_id=' . $album_id . '&p=' . $total . '">Último</a></li>';
												echo '</ul></div>';
											}
											
										}
									}
								}
								catch(Exception $e)
								{
									echo '<div class="alert alert-error fade in">' . $e->getMessage() . '</div>';
									echo '<div class="alert alert-info fade in">Experimente limpar os cookies do seu navegador.</div>';
								}
							?>
                        </fieldset>
                    </div>
                </div>
			</div>
		</div>
	</div>
    <br>
    <br>
</body>
</html>
<!--
Page ID
	Albums
		Album ID
		Album Description
		Album Cover
			Photos
				Photo ID
				Photo Description
				Photo Thumbnail
				Photo Source
-->