<?php

class Contato_Email extends Datastore
{

    public function init()
    {
        $this->struct = array(
            'contato' => array(
                'type' => Cdc_Definition::TYPE_RELATION,
                'statement_type' => Cdc_Definition::STATEMENT_SELECT,
                Cdc_Definition::OPERATION => array(
                    'read' => array(),
                    'send' => array(),
                    'item' => array(),
                    'create' => array(
                        'statement_type' => Cdc_Definition::STATEMENT_INSERT,
                    ),
                    'update' => array(
                        'statement_type' => Cdc_Definition::STATEMENT_UPDATE,
                    ),
                    'delete' => array(
                        'statement_type' => Cdc_Definition::STATEMENT_DELETE,
                    ),
                ),
            ),
            'id' => self::primaryColumn(),
            'nome' => array(
                'type' => Cdc_Definition::TYPE_COLUMN,
                Cdc_Definition::TYPE_WIDGET => array(
                    'attributes' => array(
                        'required' => 'required',
                        'maxlength' => 255,
                    ),
                ),
                Cdc_Definition::OPERATION => array(
                    //'read' => array(),
                    'send' => array(),
                    'item' => array(),
                    'create' => array(),
                    'update' => array(),
                ),
                Cdc_Definition::TYPE_RULE => array(
                    array('Cdc_Rule_Trim'),
                    array('Cdc_Rule_Length', array(1, 255)),
                ),
            ),
            'secao' => array(
                'type' => Cdc_Definition::TYPE_COLUMN,
                Cdc_Definition::TYPE_WIDGET => array(
                    'widget' => 'select',
                    'options' => Contato::$titulos,
                    'attributes' => array(
                        'required' => 'required',
                    ),
                ),
                Cdc_Definition::OPERATION => array(
                    'read' => array(
                        Cdc_Definition::FORMATTER => array(array('Cdc_CellDataFormatter', 'arrayValue'), array(Contato::$titulos)),
                    ),
                    'item' => array(),
                    'create' => array(),
                    'update' => array(),
                ),
                Cdc_Definition::TYPE_RULE => array(
                    array('Cdc_Rule_Required'),
                    array('Cdc_Rule_Trim'),
                    array('Cdc_Rule_ArrayKeyExists'),
                ),
            ),
            'email' => array(
                'type' => Cdc_Definition::TYPE_COLUMN,
                Cdc_Definition::TYPE_WIDGET => array(
                    'attributes' => array(
                        'required' => 'required',
                        'maxlength' => 255,
                    ),
                ),
                Cdc_Definition::OPERATION => array(
                    'read' => array(),
                    'send' => array(),
                    'item' => array(),
                    'create' => array(),
                    'update' => array(),
                ),
                Cdc_Definition::TYPE_RULE => array(
                    array('Cdc_Rule_Trim'),
                    array('Cdc_Rule_Length', array(1, 255)),
                ),
            ),
        );
    }

}