<?php
		
Zend_Loader::loadClass('Zend_Mail');

abstract class Contato_Abstract
{

    const TYPE_TEXT = 'text';
    const TYPE_HTML = 'html';

    public $template = 'contato/contato.phtml';

    public $index = 'contato';
    public $pageTitle = 'Contato';
    public $title = '';

    public $to = array();

    public $input = array();

    public $rule;

    public $transport = null;

    public $struct;

    /**
     *
     * @var Zend_Mail
     */
    public $mail;

    protected $definition;

    public function __construct($input)
    {
		set_time_limit(300);
	
        $this->input = $input;
        $this->mail = new Zend_Mail('utf8');
        $this->init();

        $this->definition = new Cdc_Definition($this->struct, 'contato');

        $queryResult = $this->definition->query(Cdc_Definition::TYPE_RULE)->fetch();

        $this->rule = new Cdc_Rule($queryResult);

    }

    public function send()
    {
        $this->mail->setSubject($this->getSubject());

        if ($this->getMode() == self::TYPE_HTML)
        {
            $this->mail->setBodyHtml($this->getBody(), 'utf8');
        }
        else
        {
            $this->mail->setBodyText($this->getBody(), 'utf8');
        }

        $from = $this->getFrom();
        $this->mail->setFrom(current($from));

        $to = $this->to;
        foreach ($to as $key => $value)
        {
            $this->mail->addTo($value);
        }

        if(!empty(Config::$default_user_smtp))
        {
            $smtp = Config::$default_user_smtp;
            $server = $smtp['server'];
            unset($smtp['server']);
            $this->transport = new Zend_Mail_Transport_Smtp($server, $smtp);
        }

        return $this->mail->send($this->transport);
    }

    public function validate()
    {
        return $this->rule->invoke($this->input);
    }

    abstract public function init();

    abstract public function getFrom();

    abstract public function getTo();

    abstract public function getSubject();

    abstract public function getBody();

    /**
     * Html ou texto.
     */
    abstract public function getMode();


}