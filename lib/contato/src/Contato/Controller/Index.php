<?php

class Contato_Controller_Index extends Controller
{

    public $pageTitle = 'Contato';
    public $title = 'Contato';
    public $isFront = false;
    public $sidebar = true;

    public $form;

    public function init()
    {
        parent::init();
        $route = Config::$dispatcher->getMatchedRoute();

        $target = $route->getTarget();

        if (!array_key_exists('form', $target))
        {
            throw new Cdc_Exception_RouteWithoutForm;
        }


        $form_class = $target['form'];

        $this->form = new $form_class($_POST);

        $this->index = $this->form->index;

        $this->pageTitle = $this->form->pageTitle;

        $this->title = $this->form->title;
    }

    public function process()
    {
        if ($this->form->validate())
        {
            $to = $this->getContact(Contato::$titulos_id[$this->form->slug]);

            if(!is_null($to))
            {
                $this->form->to = $to;
            }

            try
            {
                $this->form->send();
                event('Sua mensagem foi enviada com sucesso.');
            }
            catch (Exception $e)
            {
                event('Erro: ' . $e->getMessage(), LOG_ERR);
            }

        }

        Cdc_ConstraintMessagePrinter::event($this->form->rule->getMessages(), Config::$labels);

    }

    public function content()
    {

        $share = array(
            'title' => $this->pageTitle . ' - ' . Config::$baseTitle,
            'url' => Config::$base_url . substr($this->link('faleconosco'), 1),
        );

        $this->share = array_merge($this->share, $share);

        if ($_SERVER['REQUEST_METHOD'] == 'POST')
        {
            $this->process();
        }

        $form = new Cdc_Form($this->form->struct, array(), $this->form->input);
        $render = $form->render($this->getTemplate($this->form->template));

        if(isset($this->form->slug))
        {
            return $this->renderText($this->form->slug, 'slug') . $render;
        }

        return $render;
    }

}