<?php

class Contato_Metadata_Schema
{

    public static function setup(\Doctrine\DBAL\Schema\Schema $schema)
    {

        self::createContatoStructures($schema);

    }

    public static function createContatoStructures(\Doctrine\DBAL\Schema\Schema $schema)
    {
        $contato = $schema->createTable('contato');
        $contato->addColumn('id', 'integer');
        $contato->setPrimaryKey(array('id'));
        $contato->addColumn('nome', 'string', array('length' => '255', 'notnull' => false));
        $contato->addColumn('secao', 'string', array('length' => 2, 'notnull' => false));
        $contato->addColumn('email', 'string', array('length' => '225'));
    }

}