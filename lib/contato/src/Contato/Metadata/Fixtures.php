<?php

class Contato_Metadata_Fixtures
{

    public static function setup()
    {

        $pdo = Cdc_Pdo_Pool::getConnection(Config::$default_db);

        $model = new Contato_Email($pdo, 'create');
        $definition = new Cdc_Definition($model->struct, 'create');
        $model->setDefinition($definition);

        $sql = $model->createQuery();

        $sql->cols = array(
            'secao' => '1',
            'nome' => Config::$default_user_data['nome'],
            'email' => Config::$default_user_data['email'],
        );

        $model->execute($sql);

    }

}