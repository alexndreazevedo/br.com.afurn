<?php

class Contato_Metadata_Routes
{

    public static function setup($router)
    {
        $router->map(Config::$admin_base_route . '/Contato_Email', array('class' => 'Contato_Controller_Email', 'resource' => 'auto'), array('name' => 'admin_contato'));
    }

}
