(function($){
    var methods = {
        init : function(options) {
            var plugin = this;

            var els = $('.inline', plugin);

            var runBlur = function(i, item) {
                var diz = $(item);
                if (diz.val().length)
                {
                    diz.prev('label').hide();

                }
                else
                {
                    diz.prev('label').show();
                }
            }


            els.focus(function() {
                $(this).prev('label').hide();
            }).blur(function() {
                runBlur(null, this);
            });

            els.each(runBlur);

            return this;
        }
    };

    $.fn.inlineLabel = function( method ) {

        // Method calling logic
        if ( methods[method] ) {
            return methods[ method ].apply( this, Array.prototype.slice.call( arguments, 1 ));
        } else if ( typeof method === 'object' || ! method ) {
            return methods.init.apply( this, arguments );
        } else {
            $.error( 'Method ' +  method + ' does not exist on jQuery.inlineLabel' );
        }

    };

})(jQuery);