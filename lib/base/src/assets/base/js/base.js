function getLocation()
{
    var gl;

    try {
        if (typeof navigator.geolocation === 'undefined')
        {
            gl = google.gears.factory.create('beta.geolocation');
        }
        else
        {
            gl = navigator.geolocation;
        }
    } catch(e) {}

    if (gl)
    {
        gl.getCurrentPosition(function(position){
            document.getElementById('latitude').value = position.coords.latitude;
            document.getElementById('longitude').value = position.coords.longitude;
        },
		function(error) {},
        { maximumAge: 600000, timeout: 10000 });
    }
}

$(function() {

    

    if($('#latitude').length && $('#longitude').length)
    {
        if($('#latitude').val() == '' && $('#longitude').val() == '')
        {
            getLocation();
        }
    }

    $('[rel=tooltip]').tooltip();
    
    $.datepicker.setDefaults($.datepicker.regional['pt-BR']);
    if ($('input.datetime').length)
    {
        $('input.datetime').datetimepicker({
            duration: '',
            showTime: true,
            constrainInput: false,
            changeMonth: true,
            changeYear: true,
            minDate: -100

        });
    }

    if ($('input.date').length)
    {
        $('input.date').mask('99/99/9999').datepicker({
            duration: '',
            showTime: false,
            constrainInput: false,
            dateFormat: 'dd/mm/yy',
            yearRange: '-100Y:+10Y',
            changeMonth: true,
            changeYear: true
        });
    }
	
	$('.spinner').spinner({
		step: 0.01,
		numberFormat: "n"
	});
	
	$('.money').maskMoney({thousands:'.', decimal:',', allowZero:true});

    /*
    $('.cep').mask('99.999-999');
    $('.cpf').mask('999.999.999-99');
    */

   (function () {
        var config = {
            toolbar: 'Custom',
            scayt_sLang: 'pt_BR',
            toolbar_Custom: [
                { name: 'document', items : [ 'Source' ] },
                { name: 'clipboard', items : [ 'Cut','Copy','Paste','PasteText','PasteFromWord','-','Undo','Redo' ] },
                { name: 'basicstyles', items : [ 'Bold','Italic','Underline','Subscript','Superscript','-','RemoveFormat' ] },
                { name: 'styles', items : [ 'Styles','Format','Font','FontSize' ] },
                { name: 'colors', items : [ 'TextColor','BGColor' ] },
                { name: 'paragraph', items : [ 'NumberedList','BulletedList','-','Outdent','Indent','-','Blockquote','-','JustifyLeft','JustifyCenter','JustifyRight','JustifyBlock' ] },
                { name: 'links', items : [ 'Link','Unlink' ] },
                { name: 'insert', items : [ 'cdcInsertImage','Flash','Table','Smiley','SpecialChar','PageBreak' ] },
                { name: 'tools', items : [ 'Maximize', 'ShowBlocks','-','About' ] },

                // Configuracao Original
                //{ name: 'document', items : [ 'Source' /*,'-','Save','NewPage','DocProps','Preview','Print' */ ] },
                //{ name: 'clipboard', items : [ 'Cut','Copy','Paste','PasteText','PasteFromWord','-','Undo','Redo' ] },
                //{ name: 'editing', items : [ 'Find','Replace','-','SelectAll','-','SpellChecker', 'Scayt' ] },
                //{ name: 'links', items : [ 'Link','Unlink' ] },
                //{ name: 'insert', items : [ 'cdcInsertImage','Flash','Table','Smiley','SpecialChar','PageBreak' ] },
                //{ name: 'tools', items : [ 'Maximize', 'ShowBlocks','-','About' ] },
                //'/',
                //{ name: 'basicstyles', items : [ 'Bold','Italic','Underline','Strike','Subscript','Superscript','-','RemoveFormat' ] },
                //{ name: 'styles', items : [ 'Styles','Format','Font','FontSize' ] },
                //{ name: 'colors', items : [ 'TextColor','BGColor' ] },
                //{ name: 'paragraph', items : [ 'NumberedList','BulletedList','-','Outdent','Indent','-','Blockquote','-','JustifyLeft','JustifyCenter','JustifyRight','JustifyBlock' ] },
            ],
            removePlugins: 'font,forms',
            extraPlugins: 'cdc'
        };
        $('.rich').ckeditor(config);
    })();

    var renderFileList = function(el) {

        if($(el).val() != '')
        {
            var val = $.parseJSON(decodeURIComponent($(el).val()));
            var list = $(el).siblings('.list');
            var prop;
            for (prop in val)
            {
                var newEl = $('<a class="colorbox" href="' + UPLOAD + val[prop].nome + '"><img src="' + UPLOAD + '__preview__/' + val[prop].nome + '"></a>');//.colorbox({maxWidth: '500', maxHeight: '300'});
                var newElContainer = $('<div class="fileitem draggable">');
                newElContainer.append(newEl);
                newElContainer.append('<a href="#" class="removefile" data-fileid="' + prop + '"><i class="fa fa-trash-o"></i></a>');
                list.append(newElContainer);
            }
        }
    }

    $('.file_area input').each(function(i, item) {
        renderFileList(item);
    });

    $('.sortable').sortable({
        activate: function( event, ui ) {
            $(this).addClass('active');
        },
        deactivate: function( event, ui ) {
            $(this).removeClass('active');
        }
    });

    //$('.droppable').droppable();

    $('#openfm').click(function() {
        var filemanager = $(document.getElementById('filemanager_modal'));

        filemanager.find('iframe').attr('src', FILE_MANAGER.replace('__FILE_MANAGER__REGION__', $(this).parent().prev().attr('name')));

		/*
        filemanager.dialog({
            width: '70%',
            height: 600,
            modal: true,
            buttons: [
                {
                    text: 'Fechar',
                    click: function() { $(this).dialog("close"); $('.sortable').sortable(); }
                }
            ]

        });
		*/
        filemanager.show();

        return false;
    });

    $('#filemanager_modal legend .close').click(function() {
        var filemanager = $(document.getElementById('filemanager_modal'));
        filemanager.hide();
    });

	$('#file_editor_cancel').click(function(){
		javascript:history.back(-1);
	});
	
    $('.removefile').on('click', function() {
        var input = $(this).parents('.file_area').find('input');
        var val = $.parseJSON(decodeURIComponent(input.val()));
        var index = $(this).attr('data-fileid');
        delete val[index];
        input.val(encodeURIComponent(JSON.stringify(val)));
        $(this).parent().remove();
        return false;
    });

});

function autoResize(id, ifwidth, ifheight){
    var newheight;
    var newwidth;

    if(document.getElementById(id)){
        if(ifheight) newheight = document.getElementById(id).contentWindow.document.body.scrollHeight + 20;
        if(ifwidth) newwidth = document.getElementById(id).contentWindow.document.body.scrollWidth;
    }

    if(ifheight) document.getElementById(id).height= (newheight) + "px";
    if(ifwidth) document.getElementById(id).width= (newwidth) + "px";
}

function openFileManager(modal, name)
{
    var filemanager = $(document.getElementById('filemanager_modal'));
	
    filemanager.find('iframe').attr('src', FILE_MANAGER.replace('__FILE_MANAGER__REGION__', name.prev().attr('name')));
	filemanager.find('iframe').attr('src', FILE_MANAGER.replace('__FILE_MANAGER__REGION__', $(this).prev().attr('name')));

    if(modal)
    {
        filemanager.dialog({
            width: '70%',
            height: 600,
            modal: true,
            buttons: [
                {
                    text: 'Fechar',
                    click: function() { name.dialog("close"); $('.sortable').sortable(); }
                }
            ]

        });
    }
    else
    {
        filemanager.show();
    }

    return false;
}