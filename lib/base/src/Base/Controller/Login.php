<?php

class Base_Controller_Login extends Controller
{

    public $pageTitle = 'Identificação';

    public $routeLogin = 'admin_login';
    public $routeDestination = 'admin_home';

    public $input;

    public function init()
    {
        if (Cdc_Pdo_Authentication::isAuthenticated())
        {
            header('Location: ' . $this->link($this->routeDestination));
            die;
        }
        self::configureAuth($this->args['auth']);
        $this->input = $_POST;
    }

    public static function configureAuth(Cdc_Pdo_Authentication $auth)
    {

    }

    public function process($data)
    {
        $auth = $this->args['auth'];

        $identity = f($this->input, $this->args['auth']->getIdentity());
        $password = f($this->input, $this->args['auth']->getPassword());

        if($auth->login($identity, $password))
        {
            $dest = $this->link($this->routeLogin);
            if ($_SERVER['REQUEST_URI'] != $dest)
            {
                $dest = $_SERVER['REQUEST_URI'];
            }

            flash('Bem-vindo, ' . $_SESSION[$auth->getGreeting()] . '.');
            header('Location: ' . $dest);
            die;
        }
        event('Usuário e/ou senha incorretos.', LOG_ERR);
    }

    public function content($template = null, $def = null)
    {
        if (!$template)
        {
            $template = 'layout/login.phtml';
            $return = false;
        }
        else
        {
            $return = true;
        }

        if ($_SERVER['REQUEST_METHOD'] == 'POST')
        {
            $this->process($_POST);
        }
        else
        {
            event('Por favor, identifique-se para ter acesso ao sistema.', LOG_INFO);
        }

        $def or $def = array(
            'email' => array(
                Cdc_Definition::TYPE_WIDGET => array(
                    'widget'        => 'email',
                    'attributes' => array(
                        'required' => 'required',
                    ),
                ),
            ),
            'senha'    => array(
                Cdc_Definition::TYPE_WIDGET => array(
                    'widget'        => 'password',
                    'attributes' => array(
                        'required'    => 'required',
                    ),
                ),
            ),
            'remember_me' => array(
                Cdc_Definition::TYPE_WIDGET => array(
                    'widget' => 'boolean',
                ),

            ),
        );

        $options = array(
            'controller' => $this,
        );

        $form = new Cdc_Form($def, $options, $_POST);
        header('HTTP/1.1 403 Forbidden');

        $result = $form->render($this->getTemplate($template));

        // Caso seja passado algum template, a responsabilidade do layout é de quem passou
        if ($return)
        {
            return $result;
        }

        // neste módulo o layout é o próprio formulário. Evitar que seja chamado o layout.
        echo $result;
        die;
    }

}
