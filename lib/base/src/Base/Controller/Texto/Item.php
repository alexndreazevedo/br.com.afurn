<?php

class Base_Controller_Texto_Item extends Controller
{

    public function content()
    {
        $slug = f($this->urlParams, 'slug', FILTER_SANITIZE_STRIPPED);
        $m    = $this->getModel('Texto', 'item');
        
        $result = Cdc_ArrayHelper::current($m->hydrateResultOf($m->createQuery(array('where' => array('slug =' => $slug)))));

        if (!$slug)
        {
            $default = current(Cdc_ArrayHelper::current($m->hydrateResultOf($m->createQuery(array('where' => array('id =' => reset(Texto::$paginas)))))));

            header('Location: ' . $this->link('institucional', array('slug' => $default['slug'])));
            die;
        }

        if (!$result)
        {
            return;
        }

        $item = current($result);

        if (!in_array($item['id'], Texto::$paginas))
        {
            return;
        }

        $this->pageTitle = $item['nome'];
        $this->title = $item['nome'];
        $this->index = $slug;

        ob_start();
        include $this->getTemplate('texto/item.phtml');
        return ob_get_clean();
    }

}
