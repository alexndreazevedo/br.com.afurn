<?php

class Base_Controller_Texto extends Base_Crud
{

    public $createButtonEnabled = false;

    public $layoutTemplate = 'layout/admin.phtml';

    public $relation = 'Texto';

    public function crud_read()
    {
        return $this->_read();
    }

    public function crud_update()
    {
        return $this->_update();
    }

}