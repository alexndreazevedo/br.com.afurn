<?php

class Base_Controller_Admin extends Base_Crud
{

    public $layoutTemplate = 'layout/admin.phtml';

    public function crud_read()
    {
        return $this->_read();
    }

    public function crud_create()
    {
        return $this->_create();
    }

    public function crud_update()
    {
        return $this->_update();
    }

    public function crud_delete()
    {
        return $this->_delete();
    }
    
}