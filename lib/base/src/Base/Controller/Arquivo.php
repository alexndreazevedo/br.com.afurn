<?php

class Base_Controller_Arquivo extends Base_Crud
{

    public $fileHandler = 'Base_Arquivo';

    public function content()
    {
        $this->relation = $this->fileHandler;

        return parent::content();
    }

    public function crud_read()
    {
        $this->datastore->dialog = true;
        $this->l = 10;
        $this->order = array('criado' => 'desc');
        $content = parent::_read();
        $controller = $this;
        include $this->getTemplate('dialog.phtml');
        die;
    }

    public function crud_update()
    {
        $content = parent::_update();
        $controller = $this;
        include $this->getTemplate('dialog.phtml');
        die;
    }

    public function crud_delete()
    {
        parent::_delete();

        $action = $this->link('admin', array('r' => $this->relation), array('op' => $this->op, 'id' => $this->id, 'region_id' => f(INPUT_GET, 'region_id')));

        $controller = $this;
        
        $model = $this->getModel('Base_Arquivo', 'item');
        $arquivo = current(current($model->hydrateResultOf($model->createQuery(array('where' => array('id =' => f(INPUT_GET, 'id')))))));
        if (!$arquivo)
        {
            $arquivo = array();
        }

        include $this->getTemplate('file_delete.phtml');
        die;
    }

    public function crud_chooseInstance()
    {

        $pdo = $this->getPdo();

        $instances = $this->getSavedInstances($pdo);

        $i = array();
        foreach ($instances as $key => $value)
        {
            $i[Config::$upload . $key . '/' . $this->item['nome']] = label($key);
        }

        $def = array(
            'instance_chooser' => array(
                Cdc_Definition::TYPE_WIDGET => array(
                    'widget' => 'select',
                    'options' => $i,
                    'attributes' => array(
                        'required' => 'required',
                    ),
                ),
            ),
        );

        $form = new Cdc_Form($def, array(), array());

        echo $form->render($this->getTemplate('instance_chooser.phtml'));

        die;
    }

    public function getSavedInstances($pdo)
    {
        $sql = new Cdc_Sql_Select($pdo);
        $sql->cols = array('id', 'dados');
        $sql->from = array('arquivo_instancia');
        $sql->where = array('arquivo_id =' => $this->id);

        $saved_data = $sql->stmt()->fetchAll(PDO::FETCH_KEY_PAIR);

        foreach ($saved_data as $key => $value)
        {
            $saved_data[$key] = Base_Arquivo::decodeInstanceData($value);
        }
        return $saved_data;
    }

    public function crud_operate()
    {
        $pdo = Cdc_Pdo_Pool::getConnection(Config::$default_db);

        $instances = Config::$file_instances;

        $saved_data = $this->getSavedInstances($pdo);

        if ($_SERVER['REQUEST_METHOD'] == 'POST')
        {
            $sql = new Cdc_Sql_Update($pdo);

            $sql->from = array('arquivo_instancia');

            $sql->cols = array(
                'arquivo_id' => $this->id,
            );

            $sql->where = array(
                'arquivo_id = ' => $this->id,
            );

            $data = Zend_Json::decode($_POST['settings']);

            $instancesToSave = array();

            $pdo->beginTransaction();
            foreach ($data as $key => $value)
            {

                if ($value == $saved_data[$key])
                {
                    continue;
                }

                $sql->cols['id'] = $key;
                $sql->cols['dados'] = Base_Arquivo::encodeInstanceData($value);
                $sql->where['id = '] = $key;

                try
                {
                    $sql->stmt();
                    $instancesToSave[$key] = $value;
                }
                catch (Exception $e)
                {
                    flash($e->getMessage(), LOG_ERR);
                }
            }

            try
            {
                Cdc_Upload::createInstances($this->item, $instancesToSave, false, false);
                $pdo->commit();
                flash('As alterações foram salvas.');
            }
            catch (Exception $e)
            {
                $pdo->rollback();
                flash($e->getMessage(), LOG_ERR);
            }


            header('Location: ' . $this->link('admin_arquivo'));
            die;

        }

        ob_start();
        include $this->getTemplate('file_editor.phtml');
        $content = ob_get_clean();

        $controller = $this;
        include $this->getTemplate('dialog.phtml');
        die;
    }
}
