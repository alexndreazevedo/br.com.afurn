<?php

class Base_Controller_Logout extends Controller {

    public $is_front = false;

    public function content() {
        $auth = new Cdc_Pdo_Authentication($this->getPdo(), Config::$hasher);
        $auth->logout();
        header('Location: ' . $this->link('admin_home'));
        die;
    }
}
