<?php

class Base_Controller_Upload extends Base_Crud
{

    public $fileHandler = 'Base_Arquivo';

    public $fileTypeHandler = 'Base_ArquivoTipo';

    public $options = array();

    public function content()
    {
        require Config::$vendor_abs . 'blueimp/jquery-file-upload/server/php/UploadHandler.php';

        $this->options = array(
            'upload_dir' => Config::$upload_abs,
            'upload_url' => Config::$upload,
            'user_dirs' => false,
        );

        $handler = new UploadHandler($this->options, false);

        $this->initialize($handler);
    }

    protected function initialize($handler) {
        switch ($_SERVER['REQUEST_METHOD']) {
            case 'OPTIONS':
            case 'HEAD':
                $handler->head();
                break;
            case 'GET':
                $handler->get();
                break;
            case 'PATCH':
            case 'PUT':
            case 'POST':
                $this->post($handler);
                break;
            case 'DELETE':
                $handler->delete();
                break;
            default:
                $handler->header('HTTP/1.1 405 Method Not Allowed');
        }
    }

    public function post($handler, $print_response = true)
    {

        $pdo = Cdc_Pdo_Pool::getConnection(Config::$default_db, Config::$debug);
        $fileHandler = $this->fileHandler;
        $model = new $fileHandler($pdo);
        $definition = new Cdc_Definition($model->struct, 'create');
        $model->setDefinition($definition);

        $return = array(
            'files' => array(
                'name' => $_FILES['files']['name'][0],
                'type' => $_FILES['files']['type'][0],
                'tmp_name' => $_FILES['files']['tmp_name'][0],
                'error' => $_FILES['files']['error'][0],
                'size' => $_FILES['files']['size'][0],
            ),
        );

        $file = Cdc_Upload::saveFile($return, 'files', Config::$upload_abs);

        $fileTypeHandler = $this->fileTypeHandler;

        $arquivoTipo = new $fileTypeHandler($pdo);
        $arquivoTipoDefinition = new Cdc_Definition($arquivoTipo->struct, 'item');
        $arquivoTipo->setDefinition($arquivoTipoDefinition);

        Cdc_Upload::validateFileType($file, $pdo, $arquivoTipo);

        $sql = $model->createQuery($file);

        $id = $model->hydrateResultOfExec($sql);

        $instances = Cdc_Upload::createInstances($file, Config::$file_instances);

        $sql = new Cdc_Sql_Insert(Cdc_Pdo_Pool::getConnection(Config::$default_db));
        $sql->from(array('arquivo_instancia'));
        $cols = array(
            'arquivo_id' => $id,
        );

        foreach ($instances as $key => $value)
        {
            $cols['id'] = $key;
            $cols['dados'] = Base_Arquivo::encodeInstanceData($value);
            $sql->cols($cols);
            try
            {
                $sql->stmt();
            }
            catch (Exception $e)
            {
                flash(label($key) . ': ' . $e->getMessage(), LOG_ERR);
            }
        }

        store_current_events();
        flash('Arquivo ' . $file['nome_original'] . ' recebido com sucesso.');

        return $this->generate_response(
            $handler,
            array('files' => $return),
            $print_response
        );
    }

    protected function generate_response($handler, $content, $print_response = true)
    {
        $json = json_encode($content);
        $redirect = isset($_REQUEST['redirect']) ?
            stripslashes($_REQUEST['redirect']) : null;
        if ($redirect) {
            $handler->header('Location: '.sprintf($redirect, rawurlencode($json)));
            return;
        }
        $handler->head();
        print $json; die;
    }
}
