<?php

class Base_Metadata_Fixtures
{

    public static function setup()
    {

        $pdo = Cdc_Pdo_Pool::getConnection(Config::$default_db);

        $model = new Texto($pdo, 'create');
        $definition = new Cdc_Definition($model->struct, 'create');
        $model->setDefinition($definition);

        $titulos = Texto::$titulos;

        $sql = $model->createQuery();

        foreach ($titulos as $key => $value)
        {
            $sql->cols['id'] = $key;
            $sql->cols['nome'] = $value;
            $model->execute($sql);
        }

        $u = new Base_Usuario($pdo);
        $definition = new Cdc_Definition($u->struct, 'create');
        $u->setDefinition($definition);

        $sql = $u->createQuery();

        $sql->cols = Config::$default_user_data;

        $r = $u->execute($sql);

        $id = $pdo->lastInsertId($u->getDefaultSequenceName());

        $u->getDefinition()->setOperation('update');


        $primary = $u->getDefinition()->query(Cdc_Definition::TYPE_COLUMN)->byKey('primary')->fetch(Cdc_Definition::MODE_SINGLE);
        $sql = $u->createQuery();

        $sql->cols = array('administrador' => true);
        $sql->where = array($primary . '=' => $id);
        $sql->stmt();

    }

}
