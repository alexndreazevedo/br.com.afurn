<?php

class Base_Metadata_Schema
{

    public static function setup(\Doctrine\DBAL\Schema\Schema $schema)
    {

        self::createUserManagementStructures($schema);
        self::createFileManagementStructures($schema);

        self::createBaseContentStructures($schema);

    }

    public static function createBaseContentStructures(\Doctrine\DBAL\Schema\Schema $schema)
    {
        $texto = $schema->createTable('texto');
        $texto->addColumn('id', 'integer');
        $texto->setPrimaryKey(array('id'));
        $texto->addColumn('nome', 'string', array('length' => '255', 'notnull' => false));
        $texto->addColumn('slug', 'string', array('length' => '225'));
        $texto->addColumn('corpo', 'text', array('notnull' => false));
    }


    public static function createUserManagementStructures(\Doctrine\DBAL\Schema\Schema $schema)
    {
        $usuario = $schema->createTable('usuario');
        $usuario->addColumn('id', 'integer', array('autoincrement' => true));
        $usuario->addColumn('nome', 'string', array('length' => '255'));
        $usuario->addColumn('email', 'string', array('length' => '255'));
        $usuario->addColumn('senha', 'string', array('length' => '60'));
        $usuario->addColumn('administrador', 'boolean', array('notnull' => false));
        $usuario->setPrimaryKey(array('id'));
        $usuario->addUniqueIndex(array('email'));

        $grupo = $schema->createTable('grupo');
        $grupo->addColumn('id', 'integer', array('autoincrement' => true));
        $grupo->addColumn('nome', 'string', array('length' => '255'));
        $grupo->addColumn('sistema', 'boolean');
        $grupo->setPrimaryKey(array('id'));

        $recurso = $schema->createTable('recurso');
        $recurso->addColumn('id', 'string', array('length' => '255'));
        $recurso->setPrimaryKey(array('id'));

        $usuario_grupo = $schema->createTable('usuario_grupo');
        $usuario_grupo->addColumn('usuario_id', 'integer');
        $usuario_grupo->addColumn('grupo_id', 'integer');
        $usuario_grupo->setPrimaryKey(array('usuario_id', 'grupo_id'));
        $usuario_grupo->addForeignKeyConstraint($usuario, array('usuario_id'), array('id'), array('onUpdate' => 'CASCADE', 'onDelete' => 'CASCADE'));
        $usuario_grupo->addForeignKeyConstraint($grupo, array('grupo_id'), array('id'), array('onUpdate' => 'CASCADE', 'onDelete' => 'CASCADE'));

        $grupo_recurso = $schema->createTable('grupo_recurso');
        $grupo_recurso->addColumn('grupo_id', 'integer');
        $grupo_recurso->addColumn('recurso_id', 'string', array('length' => '255'));
        $grupo_recurso->setPrimaryKey(array('grupo_id', 'recurso_id'));
        $grupo_recurso->addForeignKeyConstraint($grupo, array('grupo_id'), array('id'), array('onUpdate' => 'CASCADE', 'onDelete' => 'CASCADE'));
        $grupo_recurso->addForeignKeyConstraint($recurso, array('recurso_id'), array('id'), array('onUpdate' => 'CASCADE', 'onDelete' => 'CASCADE'));

    }

    public static function createFileManagementStructures(\Doctrine\DBAL\Schema\Schema $schema)
    {
        $arquivo_tipo = $schema->createTable('arquivo_tipo');
        $arquivo_tipo->addColumn('id', 'integer', array('autoincrement' => true));
        $arquivo_tipo->addColumn('nome', 'string', array('length' => '255'));
        $arquivo_tipo->addColumn('extensoes', 'string', array('length' => '255'));
        $arquivo_tipo->setPrimaryKey(array('id'));

        $arquivo = $schema->createTable('arquivo');
        $arquivo->addColumn('id', 'integer', array('autoincrement' => true));
        $arquivo->addColumn('nome', 'string', array('length' => '255'));
        $arquivo->addColumn('nome_original', 'string', array('length' => '255'));
        $arquivo->addColumn('titulo', 'string', array('length' => '255'));
        $arquivo->addColumn('descricao', 'text', array('notnull' => false));
        $arquivo->addColumn('creditos', 'string', array('length'  => 255, 'notnull' => false));
        $arquivo->addColumn('arquivo_tipo_id', 'integer');
        $arquivo->addColumn('criado', 'datetime');
        $arquivo->setPrimaryKey(array('id'));
        $arquivo->addUniqueIndex(array('nome'));
        $arquivo->addForeignKeyConstraint($arquivo_tipo, array('arquivo_tipo_id'), array('id'), array('onUpdate' => 'RESTRICT', 'onDelete' => 'RESTRICT'));

        $arquivo_instancia = $schema->createTable('arquivo_instancia');
        $arquivo_instancia->addColumn('id', 'string', array('length' => 255));
        $arquivo_instancia->addColumn('arquivo_id', 'integer');
        $arquivo_instancia->addColumn('dados', 'text', array('notnull' => false));
        $arquivo_instancia->setPrimaryKey(array('id', 'arquivo_id'));
        $arquivo_instancia->addForeignKeyConstraint($arquivo, array('arquivo_id'), array('id'), array('onUpdate' => 'CASCADE', 'onDelete' => 'CASCADE'));
    }

}
