<?php

class Base_Metadata_Routes
{

    public static function setup($router)
    {

        $router->map(Config::$admin_base_route . '/login', array('class' => 'Base_Controller_Login', 'resource' => 'none'), array('name' => 'admin_login'));
        $router->map(Config::$admin_base_route . '/logout', array('class' => 'Base_Controller_Logout', 'resource' => 'none'), array('name' => 'admin_logout'));
        $router->map(Config::$admin_base_route, array('class' => 'Base_Controller_Admin', 'resource' => 'authenticated'), array('name' => 'admin_home'));

        $router->map(Config::$admin_base_route . '/file', array('class' => 'Base_Controller_Arquivo', 'resource' => 'authenticated'), array('name' => 'arquivo'));
        $router->map(Config::$admin_base_route . '/file/browse', array('class' => 'Base_Controller_Arquivo', 'resource' => 'auto'), array('name' => 'file_selector'));
        $router->map(Config::$admin_base_route . '/uploadify', array('class' => 'Base_Controller_Upload', 'resource' => 'auto'), array('name' => 'uploadify'));
        $router->map(Config::$admin_base_route . '/upload', array('class' => 'Base_Controller_Upload', 'resource' => 'auto'), array('name' => 'upload'));
        $router->map(Config::$admin_base_route . '/Base_Arquivo', array('class' => 'Base_Controller_Arquivo', 'resource' => 'auto'), array('name' => 'admin_arquivo'));

        $router->map(Config::$admin_base_route . '/Base_Texto', array('class' => 'Base_Controller_Texto', 'resource' => 'auto'), array('name' => 'admin_texto'));


        $router->map(Config::$admin_base_route . '/:r', array('class' => 'Base_Controller_Admin', 'resource' => 'auto'), array('name' => 'admin'));

    }

}
