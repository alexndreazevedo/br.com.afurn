<?php

abstract class Base_Crud extends Controller
{

    public $relation;
    public $op;
    public $p;
    public $l;
    public $id;
    public $primary;
    public $filters = array();
    public $order = array();
    public $input = array();
    public $itemWhere;
    public $searchFormTemplate = null;
    public $createFormTemplate = null;
    public $updateFormTemplate = null;
    public $filterData         = array();
    public $item;
    public $createButtonEnabled = true;
    public $readButtonEnabled   = true;
    public $formOptions = array();
    public $showListCheckboxes = false;

    public $readQueryParams = array();
    public $routeResetSearch = 'admin';

    public function pageMenu()
    {
        $menu = array();

        if (method_exists(get_parent_class($this), 'pageMenu'))
        {
            $menu = parent::pageMenu();
        }

        if ($menu)
        {
            return $menu;
        }

        if (!$this->relation)
        {
            return array();
        }

        if ($this->readButtonEnabled)
        {
            $menu['read'] = array(
                'url' => $this->link('admin', array('r' => $this->relation), array('op'    => 'read')),
                'title' => 'Listagem',
            );
        }

        if ($this->createButtonEnabled)
        {
            $menu['create'] = array(
                'url' => $this->link('admin', array('r' => $this->relation), array('op'    => 'create')),
                'title' => 'Novo registro',
            );
        }

        return $menu;
    }

    /**
     * Datastore.
     * @var Cdc_Datastore
     */
    public $datastore;

    public function content()
    {
        $parameters = $this->urlParams;

        if (!$this->relation)
        {
            $this->relation = f($parameters, 'r');
        }

        if (!$this->op)
        {
            $this->op = f(INPUT_GET, 'op');
        }

        if (!$this->p)
        {
            $this->p = f(INPUT_GET, 'p');
        }

        if (!$this->l)
        {
            $this->l = Cdc_Pager::roundLimit(f(INPUT_GET, 'l'));
        }

        if (array_key_exists('order', $_GET))
        {
            $this->order = $_GET['order'];
        }

        if (!$this->op)
        {
            $this->op = 'read';
        }

        if (!$this->relation)
        {
            return '<p>Escolha uma opção no menu.</p>';
        }

        $datastore_name = $this->relation;

        $this->datastore = new $datastore_name(Config::connection());

        if (!($this->datastore instanceof Cdc_Datastore))
        {
            return null;
        }

        $definition = new Cdc_Definition($this->datastore->struct);
        $definition->setOperation($this->op);
        $this->datastore->setDefinition($definition);

        if (method_exists(get_parent_class($this), 'configureDatastore'))
        {
            parent::configureDatastore();
        }
        elseif (method_exists($this, 'configureDatastore'))
        {
            $this->configureDatastore();
        }




        $this->primary = $primary       = $this->datastore->getDefinition()->query(Cdc_Definition::TYPE_COLUMN)->byKey('primary')->fetch(Cdc_Definition::MODE_SINGLE);

        if (!$this->id)
        {
            $this->id = f(INPUT_GET, $primary);
        }


        /*
          if ($this->relation == 'Texto_Texto')
          {
          $this->index = 'texto-' . $this->id;
          }
          else
          {
          $this->index = $this->relation;
          }
         */

        if (!$this->index)
        {
            $this->index = $this->relation;
        }


        if ($this->id)
        {
            $this->itemWhere = array($primary . ' = ' => $this->id);
            $sql             = $this->datastore->createQuery(array('where'  => $this->itemWhere), 'item');
            $hresult = $this->datastore->hydrateResultOf($sql);

            if (!$hresult)
            {
                return null;
            }

            $input      = $this->item = current(current($hresult));

            if ($_POST)
            {
                $merge = array_merge($input, $_POST);
                $diff  = array_diff_key($input, $_POST);

                $skel = array_combine(array_keys($merge), array_fill(0, count($merge), null));

                $clearer = array_intersect_key($skel, $diff);

                $input = array_merge($merge, $clearer);

                unset($input[$primary]);
            }

            $this->input = $input;

            if (!$this->datastore->allow($this->input))
            {
                return null;
            }
        }
        else
        {
            $this->input = $_POST;
        }


        if (array_key_exists('filters', $_GET))
        {
            $definition->setOperation('item');
            $fields = $definition->query(Cdc_Definition::TYPE_COLUMN, Cdc_Definition::TYPE_ATTACHMENT)->byKey('search')->fetch();
            $definition->setOperation($this->op);
            foreach ($_GET['filters'] as $key => $value)
            {
                if (array_key_exists($key, $fields))
                {
                    if (isset($fields[$key]['search']['operator']))
                    {
                        $filterOperator = trim($fields[$key]['search']['operator']);
                    }
                    else
                    {
                        $filterOperator = 'like';
                    }


                    $this->filterData[$key] = $value;

                    if ($filterOperator == 'like' || $filterOperator == 'ilike')
                    {
                        if ($value === '')
                        {
                            continue;
                        }
                        $value = '%' . $value . '%';
                    }

                    if (isset($fields[$key]['search']['query_params']))
                    {
                        $this->readQueryParams = Cdc_ArrayHelper::array_merge_recursive_distinct($this->readQueryParams, $fields[$key]['search']['query_params']);
                    }

                    $this->filters[$key . ' ' . $filterOperator . ' '] = $value;
                }
            }
        }

        $pre_method = '_pre_' . $this->op;
        if (method_exists(get_parent_class($this), $pre_method))
        {
            parent::$pre_method();
        }
        elseif (method_exists($this, $pre_method))
        {
            $this->$pre_method();
        }

        $method = 'crud_' . $this->op;

        if (method_exists($this, $method))
        {
            $saida = $this->$method();
        }
        else
        {
            return null;
        }

        return $saida;
    }

    protected function _read()
    {
        $args = array(
            'limit' => array('page'  => $this->p, 'limit' => $this->l),
            'where' => $this->filters,
            'order' => $this->order
        );

        $args = Cdc_ArrayHelper::array_merge_recursive_distinct($args, $this->readQueryParams);

        $sql = $this->datastore->createQuery($args);

        $lista = $this->datastore->hydrateResultOf($sql);

        $total = key($lista);

        $this->pageTitle = label($this->relation . '_read');

        if (!$total)
        {
            return '<div class="well">Nenhum registro encontrado.</div>';
        }

        // descartar total
        $lista = reset($lista);

        $caption = 'Total: ' . $total;

        $definition = $this->datastore->getDefinition();

        $hidden     = $definition->query(Cdc_Definition::TYPE_COLUMN)->byKey('hide')->fetch(Cdc_Definition::MODE_KEY_ONLY);
        $formatters = Cdc_ArrayHelper::pluck($definition->query(Cdc_Definition::TYPE_COLUMN, Cdc_Definition::TYPE_ATTACHMENT)->byKey(Cdc_Definition::FORMATTER)->fetch(), Cdc_Definition::FORMATTER);
        $providers  = Cdc_ArrayHelper::pluck($definition->query(Cdc_Definition::TYPE_COLUMN)->byKey(Cdc_Definition::PROVIDER)->fetch(), Cdc_Definition::PROVIDER);

        $primary = $this->datastore->getDefinition()->query(Cdc_Definition::TYPE_COLUMN)->byKey('primary')->fetch(Cdc_Definition::MODE_SINGLE);

        $options = array(array($this->datastore, 'getHtmlOptions'), array('controller' => $this));


        $pager = Cdc_Pager::renderSimple($_SERVER['REQUEST_URI'], $total, $this->p, $this->l, 13, array(), true);

        if ($pager)
        {
            $pager = '<div class="pagination">' . $pager . '</div>';
        }

        if (!$this->showListCheckboxes)
        {
            $primary = null;
        }

        return $this->searchForm()
                . '<form method="post">' .
                Cdc_Table::render($caption, $lista, $pager, $options, $formatters, $providers, $hidden, $primary)
                . '</form>';
    }

    public function searchForm()
    {
        $definition = $this->datastore->getDefinition();
        $definition->setOperation('item');
        $fields     = $definition->query(Cdc_Definition::TYPE_COLUMN, Cdc_Definition::TYPE_ATTACHMENT)->byKey('search')->fetch();

        if (!$fields)
        {
            return '';
        }

        $resetUrl = $this->link($this->routeResetSearch, array('r' => $this->relation), array('op'  => $this->op));
        $form = new Cdc_Form($fields, array('search_form' => 'filters', 'resetUrl'    => $resetUrl), $this->filterData);

        return $form->render($this->searchFormTemplate);
    }

    function _create($update = false)
    {

        $this->pageTitle = label($this->relation . '_create');

        $definition = $this->datastore->getDefinition();
        if ($_SERVER['REQUEST_METHOD'] == 'POST')
        {

            $ruleQueryResult = $definition->query(Cdc_Definition::TYPE_RULE)->fetch();
            $rules           = new Cdc_Rule($ruleQueryResult);
            if ($rules->invoke($this->input))
            {
                if ($update)
                {
                    $sql = $this->datastore->createQuery(array('cols'  => $this->input, 'where' => $this->itemWhere));
                    $msg    = 'O registro foi atualizado.';
                }
                else
                {
                    $sql = $this->datastore->createQuery($this->input);
                    $msg = 'O registro foi criado.';
                }

                $this->datastore->getPdo()->beginTransaction();
                try
                {

                    $id = $this->datastore->hydrateResultOfExec($sql, $this->input);

                    $this->datastore->getPdo()->commit();
                    flash($msg);
                    header('Location: ' . $this->getRedirect($id));
                    die;
                }
                catch (Exception $e)
                {
                    $this->datastore->getPdo()->rollBack();
                    event($e->getMessage(), LOG_ERR);
                }
            }
            else
            {
                Cdc_ConstraintMessagePrinter::event($rules->getMessages(), Config::$labels);
            }
        }

        $def = $definition->query(Cdc_Definition::TYPE_WIDGET)->fetch();

        $form = new Cdc_Form($def, $this->formOptions, $this->input);

        if ($update)
        {
            $tpl = $this->updateFormTemplate;
        }
        else
        {
            $tpl = $this->createFormTemplate;
        }

        if ($tpl)
        {
            return $form->render($this->getTemplate($tpl));
        }

        return $form->render();

    }

    public function getRedirect($id = null)
    {
        if (in_array($this->op, array('create', 'update', 'delete')))
        {
            return $this->link('admin', array('r' => $this->relation), array('op' => 'read'));
        }
        return $_SERVER['REQUEST_URI'];
    }

    protected function _update()
    {
        $result          = $this->_create(true);
        $this->pageTitle = label($this->relation . '_update');
        return $result;
    }

    protected function _delete()
    {
        $this->pageTitle = label($this->relation . '_delete');

        if ($_SERVER['REQUEST_METHOD'] == 'POST')
        {
            $primary = $this->primary;
            $query   = $this->datastore->createQuery($this->itemWhere);
            $this->datastore->getPdo()->beginTransaction();
            try
            {
                $this->datastore->hydrateResultOfExec($query, array($this->primary => $this->id));

                $this->datastore->getPdo()->commit();
                flash('O registro foi excluído.');
                header('Location: ' . $this->getRedirect($this->id));
                die;
            }
            catch (Exception $e)
            {
                $this->datastore->getPdo()->rollBack();
                event($e->getMessage(), LOG_ERR);
            }
        }

        $def = array();

        $options = array();

        $form = new Cdc_Form($def, $options, $this->input);

        return '<h4>Clique para confirmar a exclusão</h4>' . $form->render();
    }

}
