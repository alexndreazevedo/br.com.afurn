<?php

class Base_Texto extends Datastore
{

    public function init()
    {
        $this->struct = array(
            'texto' => array(
                'type' => Cdc_Definition::TYPE_RELATION,
                'statement_type' => Cdc_Definition::STATEMENT_SELECT,
                Cdc_Definition::OPERATION => array(
                    'read' => array(),
                    'create' => array(
                        'statement_type' => Cdc_Definition::STATEMENT_INSERT,
                    ),
                    'item' => array(),
                    'update' => array(
                        'statement_type' => Cdc_Definition::STATEMENT_UPDATE,
                    ),
                ),
            ),
            'id' => array(
                'type' => Cdc_Definition::TYPE_COLUMN,
                'primary' => true,
                'hide' => true,
                Cdc_Definition::OPERATION => array(
                    'read' => array(),
                    'item' => array(),
                    'create' => array(),
                    'update' => array(),
                ),
            ),
            'slug' => array(
                'type' => Cdc_Definition::TYPE_COLUMN,
                'hide' => true,
                Cdc_Definition::OPERATION => array(
                    'read' => array(),
                    'item' => array(),
                ),
            ),
            'nome' => array(
                'type' => Cdc_Definition::TYPE_COLUMN,
                'search' => array(
                    'operator' => 'like',
                ),
                Cdc_Definition::TYPE_WIDGET => array(
                    'attributes' => array(
                        'required' => 'required',
                        'maxlength' => 255,
                    ),
                ),
                Cdc_Definition::OPERATION => array(
                    'read' => array(),
                    'item' => array(),
                    'update' => array(),
                ),
                Cdc_Definition::TYPE_RULE => array(
                    array(
                        'Cdc_Rule_Trim'),
                    array(
                        'Cdc_Rule_Length',
                        array(
                            1,
                            255)),
                ),
            ),
            'corpo' => array(
                'type' => Cdc_Definition::TYPE_COLUMN,
                Cdc_Definition::TYPE_WIDGET => array(
                    'widget' => 'rich',
                ),
                Cdc_Definition::OPERATION => array(
                    'item' => array(),
                    'update' => array(),
                ),
                Cdc_Definition::TYPE_RULE => array(
                    array(
                        'Cdc_Rule_Trim'),
                ),
            ),
        );
    }

    protected $slugs = null;

    public function slugs()
    {
        if (null === $this->slugs)
        {
            $sql = new Cdc_Sql_Select($this->getPdo());
            $sql->cols = array('id', 'slug', 'nome');
            $sql->from = array('texto');
            $this->slugs = Cdc_ArrayHelper::keyPair($sql->stmt()->fetchAll());
        }
        return $this->slugs;
    }

    public function getHtmlOptions($row, $rowset, $args)
    {
        if (!$this->allow($row))
        {
            return '';
        }

        $return = '';
        $link_args = array(
            'r' => $args['controller']->relation
        );
        $link_http_params = array(
            'op' => 'update',
            $args['controller']->primary => $row[$args['controller']->primary],
        );
        $return .= '<a class="btn" rel="tooltip" title="Editar" href="' . $args['controller']->link('admin', $link_args, $link_http_params) . '"><i class="fa fa-pencil"></i></a>';

        return '<div class="btn-group">' . $return . '</div>';
    }

    public function allow($row = array(), $rowset = array(), $index = null, $args = array())
    {
        return true;
    }

    public function execute($sql)
    {
        if ($sql->from == array(
            'texto') && ($sql instanceof Cdc_Sql_Insert || $sql instanceof Cdc_Sql_Update))
        {
            $sql->cols['slug'] = Cdc_Texto::slug($sql->cols['nome']);
        }

        // @TODO: Tratar slug repetido
        return parent::execute($sql);
    }

}