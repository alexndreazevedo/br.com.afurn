<?php

class Base_ArquivoTipo extends Datastore
{
    public function init()
    {
        $this->struct = array(
            'arquivo_tipo' => array(
                'type'                    => Cdc_Definition::TYPE_RELATION,
                'statement_type'          => Cdc_Definition::STATEMENT_SELECT,
                Cdc_Definition::OPERATION => array(
                    'read' => array(),
                    'item' => array(),
                    'create' => array(
                        'statement_type' => Cdc_Definition::STATEMENT_INSERT,
                    ),
                    'update'         => array(
                        'statement_type' => Cdc_Definition::STATEMENT_UPDATE,
                    ),
                    'delete'         => array(
                        'statement_type' => Cdc_Definition::STATEMENT_DELETE,
                    ),
                ),
            ),
            'id'             => array(
                'type'                    => Cdc_Definition::TYPE_COLUMN,
                'primary'                 => true,
                'hide'                    => true,
                Cdc_Definition::OPERATION => array(
                    'read' => array(),
                    'item' => array(),
                    'create' => array(),
                    'update' => array(),
                    'delete' => array(),
                ),
            ),
            'nome' => array(
                'type'                      => Cdc_Definition::TYPE_COLUMN,
                Cdc_Definition::TYPE_WIDGET => array(
                    'attributes' => array(
                        'required'                => 'required',
                        'maxlength'               => 255,
                    ),
                ),
                Cdc_Definition::OPERATION => array(
                    'read' => array(),
                    'item' => array(),
                    'create' => array(),
                    'update' => array(),
                ),
                Cdc_Definition::TYPE_RULE => array(
                    array('Cdc_Rule_Trim'),
                    array('Cdc_Rule_Length', array(1, 255)),
                ),
            ),
            'extensoes' => array(
                'type'                      => Cdc_Definition::TYPE_COLUMN,
                Cdc_Definition::TYPE_WIDGET => array(
                    'attributes' => array(
                        'required'                => 'required',
                        'maxlength'               => 255,
                    ),
                ),
                Cdc_Definition::OPERATION => array(
                    'read' => array(),
                    'item' => array(),
                    'create' => array(),
                    'update' => array(),
                ),
                Cdc_Definition::TYPE_RULE => array(
                    array('Cdc_Rule_Trim'),
                    array('Cdc_Rule_Length', array(1, 255)),
                ),
            ),
        );
    }
}
