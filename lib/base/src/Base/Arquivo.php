<?php

class Base_Arquivo extends Datastore
{

    /**
     *
     * Codificar os parâmetros usados na criação de uma instância de arquivo.
     *
     * PHP serialize() não presta. Salvar o json no osso no banco já me causou
     * problemas por causa de codificação de caracteres em certos bancos de dados.
     * Por isso tem que fazer toda essa maracutaia convoluta.
     *
     * @param array $data
     * @return string
     */
    public static function encodeInstanceData($data)
    {
        return base64_encode(Zend_Json::encode($data));
    }

    /**
     * Decodificar os dados codificados pelo método Base_Arquivo::encodeInstanceData.
     *
     * @param string $data
     * @return array
     */
    public static function decodeInstanceData($data)
    {
        return Zend_Json::decode(base64_decode($data));
    }

    public function formatArquivoTipo($row, $rowset, $index, $args)
    {
        static $arquivoTipos;
        if (!$arquivoTipos)
        {
            $arquivoTipos = $this->getPdo()->query('select id, nome from arquivo_tipo order by nome asc')->fetchAll(PDO::FETCH_KEY_PAIR);
        }

        return '<td class="' . $index . '">' . $arquivoTipos[$row[$index]] . '</td>';

    }

    public function providerPreview($lista, $args)
    {
        foreach ($lista as $key => $item)
        {
            $ext = pathinfo($item['nome'], PATHINFO_EXTENSION);

            if (in_array($ext, array('jpg', 'png', 'jpeg', 'gif')))
            {
                $lista[$key] = array_merge(array('preview' => '<a class="colorbox" href="' . Config::$upload . $item['nome'] . '"><img src="' . Config::$upload . '__preview__/' . $item['nome'] . '"></a>'), $item);
            }
            else
            {
                $lista[$key] = array_merge(array('preview' => '<a href="' . Config::$upload . $item['nome'] . '"><i class="fa fa-file"></i></a>'), $item);
            }


        }

        return $lista;
    }


    public function init()
    {
        $this->struct = array(
            'arquivo' => array(
                'type'                    => Cdc_Definition::TYPE_RELATION,
                'statement_type'          => Cdc_Definition::STATEMENT_SELECT,
                Cdc_Definition::OPERATION => array(
                    'read' => array(),
                    'item' => array(),
                    'operate' => array(),
                    'chooseInstance' => array(),
                    'create' => array(
                        'statement_type' => Cdc_Definition::STATEMENT_INSERT,
                    ),
                    'update'         => array(
                        'statement_type' => Cdc_Definition::STATEMENT_UPDATE,
                    ),
                    'delete'         => array(
                        'statement_type' => Cdc_Definition::STATEMENT_DELETE,
                    ),
                ),
            ),
            'id'             => array(
                'type'                    => Cdc_Definition::TYPE_COLUMN,
                'primary'                 => true,
                'hide'                    => true,
                Cdc_Definition::OPERATION => array(
                    'read' => array(
                        Cdc_Definition::PROVIDER => array(array($this, 'providerPreview')),
                    ),
                    'item' => array(),
                    'create' => array(),
                    'operate' => array(),
                    'chooseInstance' => array(),
                    'update' => array(),
                    'delete' => array(),
                ),
            ),
            'nome' => array(
                'type'                      => Cdc_Definition::TYPE_COLUMN,
                Cdc_Definition::TYPE_WIDGET => array(
                    'attributes' => array(
                        'required'                => 'required',
                        'maxlength'               => 255,
                        'readonly'             => 'readonly',
                    ),
                ),
                Cdc_Definition::OPERATION => array(
                    'read' => array(
                        'hide' => true,
                    ),
                    'item' => array(),
                    'operate' => array(),
                    'create' => array(),
                    'update' => array(),
                ),
                Cdc_Definition::TYPE_RULE => array(
                    array('Cdc_Rule_Trim'),
                    array('Cdc_Rule_Length', array(1, 255)),
                ),
            ),
            'nome_original' => array(
                'type'                      => Cdc_Definition::TYPE_COLUMN,
                Cdc_Definition::TYPE_WIDGET => array(
                    'attributes' => array(
                        'required'                => 'required',
                        'maxlength'               => 255,
                        'readonly'             => 'readonly',
                    ),
                ),
                Cdc_Definition::OPERATION => array(
                    'item' => array(),
                    'create' => array(),
                    'update' => array(),
                ),
                Cdc_Definition::TYPE_RULE => array(
                    array('Cdc_Rule_Trim'),
                    array('Cdc_Rule_Length', array(1, 255)),
                ),
            ),
            'titulo' => array(
                'type'                      => Cdc_Definition::TYPE_COLUMN,
                'search' => array(
                    'operator' => 'like',
                ),
                Cdc_Definition::TYPE_WIDGET => array(
                    'attributes' => array(
                        'required'                => 'required',
                        'maxlength'               => 255,
                    ),
                ),
                Cdc_Definition::OPERATION => array(
                    'read' => array(),
                    'item' => array(),
                    'create' => array(),
                    'update' => array(),
                ),
                Cdc_Definition::TYPE_RULE => array(
                    array('Cdc_Rule_Trim'),
                    array('Cdc_Rule_Length', array(1, 255)),
                ),
            ),
            'descricao' => array(
                'type'                      => Cdc_Definition::TYPE_COLUMN,
                Cdc_Definition::TYPE_WIDGET => array(
                    'widget'     => 'textarea',
                    'attributes' => array(
                        'cols'                    => 150,
                        'rows'                    => 10,
                    ),
                ),
                Cdc_Definition::OPERATION => array(
                    'item' => array(),
                    'create' => array(),
                    'update' => array(),
                ),
                Cdc_Definition::TYPE_RULE => array(
                    array('Cdc_Rule_Trim'),
                ),
            ),
            'creditos' => array(
                'type'                      => Cdc_Definition::TYPE_COLUMN,
                Cdc_Definition::TYPE_WIDGET => array(
                    'attributes' => array(
                        'maxlength'               => 255,
                    ),
                ),
                Cdc_Definition::OPERATION => array(
                    'item' => array(),
                    'create' => array(),
                    'update' => array(),
                ),
                Cdc_Definition::TYPE_RULE => array(
                    array('Cdc_Rule_Trim'),
                    array('Cdc_Rule_Length', array(0, 255)),
                ),
            ),
            'arquivo_tipo_id' => array(
                'type'                      => Cdc_Definition::TYPE_COLUMN,
                Cdc_Definition::TYPE_WIDGET => array(
                    'attributes' => array(
                        'required'                => 'required',
                        'maxlength'               => 55,
                    ),
                ),
                Cdc_Definition::OPERATION => array(
                    /*
                    'read' => array(
                        Cdc_Definition::FORMATTER => array(array($this, 'formatArquivoTipo')),
                    ),
                     * 
                     */
                    'operate' => array(),
                    'item' => array(),
                    'create' => array(),
                ),
                Cdc_Definition::TYPE_RULE => array(
                    array('Cdc_Rule_Trim'),
                    array('Cdc_Rule_Length', array(1, 55)),
                ),
            ),
            'criado' => array(
                'type'                      => Cdc_Definition::TYPE_COLUMN,
                Cdc_Definition::TYPE_WIDGET => array(
                    'widget'     => 'datetime',
                    'attributes' => array(
                        'required'                => 'required',
                        'maxlength'               => 20,
                    ),
                ),
                Cdc_Definition::OPERATION => array(
                    //'read' => array(),
                    'item' => array(),
                    'create' => array(),
                ),
                Cdc_Definition::TYPE_RULE => array(
                    array('Cdc_Rule_Trim'),
                    array('Cdc_Rule_Length', array(0, 20)),
                ),
            ),
        );
    }

    public function getHtmlOptions($row, $rowset, $args)
    {
        if (!$this->allow($row))
        {
            return '';
        }

        $return    = '';
        $link_args = array(
            'r'               => $args['controller']->relation
        );
        $link_http_params = array(
            $args['controller']->primary => $row[$args['controller']->primary],
        );

        $link_http_params['region_id'] = (f(INPUT_GET, 'region_id')) ? f(INPUT_GET, 'region_id') : '';

        $link_http_params['op'] = 'chooseInstance';
        $return .= '<div class="btn-group">';

        $return .= '<a href="' . $args['controller']->link('arquivo', $link_args, $link_http_params) . '" rel="tooltip" title="Usar este arquivo" class="file_select btn" data-toggle="modal" data-filename="' . $row['nome'] . '" data-fileid="' . $row['id'] . '"><i class="fa fa-plus-circle"></i></a>';

        $link_http_params['op'] = 'operate';
        $return .= '<a href="' . $args['controller']->link('arquivo', $link_args, $link_http_params) . '" rel="tooltip" title="Ajustar recortes" class="file_operate btn"><i class="fa fa-wrench"></i></a>';

        /*
        $link_http_params['op'] = 'update';
        $return .= '<a href="' . $args['controller']->link('arquivo', $link_args, $link_http_params) . '" rel="tooltip" title="Editar os dados do arquivo" class="file_update btn"><i class="fa fa-pencil"></i></a>';
         * 
         */

        $link_http_params['op'] = 'delete';
        $return .= '<a href="' . $args['controller']->link('arquivo', $link_args, $link_http_params) . '" rel="tooltip" title="Excluir arquivo" class="file_delete btn"><i class="fa fa-trash-o"></i></a>';

        $return .= '</div>';

        return $return;
    }

}