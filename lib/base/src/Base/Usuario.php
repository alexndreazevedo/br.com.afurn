<?php

class Base_Usuario extends Datastore
{

    public function init()
    {
        $this->struct = array(
            'usuario' => array(
                'type'                    => Cdc_Definition::TYPE_RELATION,
                'statement_type'          => Cdc_Definition::STATEMENT_SELECT,
                Cdc_Definition::OPERATION => array(
                    'read' => array(),
                    'item' => array(),
                    'create' => array(
                        'statement_type' => Cdc_Definition::STATEMENT_INSERT,
                    ),
                    'update'         => array(
                        'statement_type' => Cdc_Definition::STATEMENT_UPDATE,
                    ),
                    'delete'         => array(
                        'statement_type'                => Cdc_Definition::STATEMENT_DELETE,
                    ),
                ),
                Cdc_Definition::TYPE_ATTACHMENT => array(
                    'grupo' => array(
                        'query_params' => array(
                            'cols' => array('id', 'nome', 'usuario_id'),
                            'from' => array('grupo'),
                            'join' => array('usuario_grupo' => array('inner' => array('grupo.id = usuario_grupo.grupo_id'))),
                            'order' => array('nome'  => 'asc'),
                            'where' => array('usuario_grupo.usuario_id in #id#'),
                        ),
                        'exec_params' => array(
                            'relation_table' => 'usuario_grupo',
                            'relation_column_attachment' => 'grupo_id',
                            'attachment_table' => 'grupo',
                            'extra_columns' => array(
                            ),
                        ),
                        'index_key'      => 'id', // índice para este attachment
                        'parent_key'     => 'id',
                        'attachment_key' => 'usuario_id',
                    ),
                ),
            ),
            'id'             => array(
                'type'                    => Cdc_Definition::TYPE_COLUMN,
                'primary'                 => true,
                'hide'                    => true,
                Cdc_Definition::OPERATION => array(
                    'read' => array(),
                    'item' => array(),
                    'create' => array(),
                    'update' => array(),
                    'delete' => array(),
                ),
            ),
            'nome' => array(
                'type'                      => Cdc_Definition::TYPE_COLUMN,
                Cdc_Definition::TYPE_WIDGET => array(
                    'attributes' => array(
                        'required'                => 'required',
                        'maxlength'               => 255,
                        'autocomplete'            => 'off',
                    ),
                ),
                Cdc_Definition::OPERATION => array(
                    'read' => array(),
                    'item' => array(),
                    'create' => array(),
                    'update' => array(),
                ),
                Cdc_Definition::TYPE_RULE => array(
                    array('Cdc_Rule_Trim'),
                    array('Cdc_Rule_Length', array(1, 255)),
                ),
            ),
            'email' => array(
                'type'                      => Cdc_Definition::TYPE_COLUMN,
                Cdc_Definition::TYPE_WIDGET => array(
                    'widget'     => 'email',
                    'attributes' => array(
                        'maxlength'               => 255,
                        'autocomplete'            => 'off',
                    ),
                ),
                Cdc_Definition::OPERATION => array(
                    //'read' => array(),
                    'item' => array(),
                    'create' => array(),
                    'update' => array(),
                ),
                Cdc_Definition::TYPE_RULE => array(
                    array('Cdc_Rule_Trim'),
                    array('Cdc_Rule_Email'),
                ),
            ),
            'grupo' => array(
                'type' => Cdc_Definition::TYPE_ATTACHMENT,
                Cdc_Definition::TYPE_ATTACHMENT => 'grupo',
                Cdc_Definition::TYPE_WIDGET => array(
                    'widget'   => 'multiselect',
                    'callback' => array(array($this, 'fetchKeyValue'), array(array('from' => array('grupo'), 'cols' => array('id', 'nome'), 'order' => array('nome asc')))),
                ),
                Cdc_Definition::OPERATION => array(
                    /*
                    'read' => array(
                        Cdc_Definition::FORMATTER => array(array('Cdc_CellDataFormatter', 'formatArray'), array('nome')),
                    ),
                     * 
                     */
                    'item' => array(),
                    'create' => array(),
                    'update' => array(),
                ),
                Cdc_Definition::TYPE_RULE => array(
                    array('Cdc_Rule_ArrayKeyExists'),
                ),
            ),
            'senha_antiga' => array(
                'type'                      => Cdc_Definition::TYPE_COLUMN,
                Cdc_Definition::TYPE_WIDGET => array(
                    'widget'     => 'password',
                    'attributes' => array(
                        'maxlength'               => 32,
                    ),
                ),
                Cdc_Definition::OPERATION => array(
                    'change_password' => array(),
                ),
            ),
            'senha' => array(
                'type'                      => Cdc_Definition::TYPE_COLUMN,
                Cdc_Definition::TYPE_WIDGET => array(
                    'widget'     => 'password',
                    'attributes' => array(
                        'maxlength'               => 32,
                    ),
                ),
                Cdc_Definition::OPERATION => array(
                    'create' => array(),
                    'change_password' => array(),
                ),
                Cdc_Definition::TYPE_RULE => array(
                    array('Cdc_Rule_Required'),
                    array('Cdc_Rule_CompareFields', array('confirma_senha', label('confirma_senha'))),
                ),
            ),
            'confirma_senha' => array(
                'type'                      => Cdc_Definition::TYPE_COLUMN,
                Cdc_Definition::TYPE_WIDGET => array(
                    'widget'     => 'password',
                    'attributes' => array(
                        'maxlength'               => 32,
                    ),
                ),
                Cdc_Definition::OPERATION => array(
                    'create' => array(
                        'virtual'         => true,
                    ),
                    'change_password' => array(),
                ),
                Cdc_Definition::TYPE_RULE => array(
                    array('Cdc_Rule_Required'),
                    array('Cdc_Rule_CompareFields', array('confirma_senha', label('confirma_senha'))),
                ),
            ),
            'administrador' => array(
                'type'                      => Cdc_Definition::TYPE_COLUMN,
                Cdc_Definition::TYPE_WIDGET => array(
                    'widget'                  => 'boolean',
                    'default'                 => false,
                ),
                Cdc_Definition::OPERATION => array(
                    'read' => array(
                        Cdc_Definition::FORMATTER => array(array('Cdc_CellDataFormatter', 'boolean')),
                    ),
                    'item' => array(),
                ),
            ),
        );
    }

    public function allow($row = array(), $rowset = array(), $index = null, $args = array())
    {
        if (array_key_exists('administrador', $row) && $row['administrador'])
        {
            return false;
        }
        return parent::allow($row, $rowset, $index, $args);
    }

    public function execute($sql)
    {

        if (in_array($sql->getOperation(), array('create', 'change_password')))
        {
            $hasher = Config::$hasher;

            if (array_key_exists('senha', $sql->cols))
            {
                $sql->cols['senha'] = $hasher->HashPassword($sql->cols['senha']);
            }
            unset($sql->cols['confirma_senha']);
            if (array_key_exists('administrador', $sql->cols))
            {
                $sql->cols['administrador'] = 0;
            }

        }
        else
        {
            // Prevenir que a flag "Administrador" seja configurada por uma
            // forma que não seja diretamente no banco de dados
            unset($sql->cols['administrador']);
        }

        return parent::execute($sql);
    }

}