<?php

class Base_Grupo extends Datastore
{

    public function init()
    {
        $this->struct = array(
            'grupo' => array(
                'type'                    => Cdc_Definition::TYPE_RELATION,
                'statement_type'          => Cdc_Definition::STATEMENT_SELECT,
                Cdc_Definition::OPERATION => array(
                    'read' => array(),
                    'item' => array(),
                    'create' => array(
                        'statement_type' => Cdc_Definition::STATEMENT_INSERT,
                    ),
                    'update'         => array(
                        'statement_type' => Cdc_Definition::STATEMENT_UPDATE,
                    ),
                    'delete'         => array(
                        'statement_type'                => Cdc_Definition::STATEMENT_DELETE,
                    ),
                ),
                Cdc_Definition::TYPE_ATTACHMENT => array(
                    'recurso' => array(
                        'query_params' => array(
                            'cols' => array('id', 'grupo_id'),
                            'from' => array('recurso'),
                            'join' => array('grupo_recurso' => array('left' => array('recurso.id = grupo_recurso.recurso_id'))),
                            'order' => array('id'    => 'asc'),
                            'where' => array('grupo_recurso.grupo_id in #id#'),
                        ),
                        'exec_params' => array(
                            'relation_table' => 'grupo_recurso',
                            'relation_column_attachment' => 'recurso_id',
                            'attachment_table' => 'recurso',
                            'extra_columns' => array(
                            ),
                        ),
                        'index_key'      => 'id', // índice para este attachment
                        'parent_key'     => 'id',
                        'attachment_key' => 'grupo_id',
                    ),
                    'usuario'        => array(
                        'query_params' => array(
                            'cols' => array('id', 'nome', 'grupo_id'),
                            'from' => array('usuario'),
                            'join' => array('usuario_grupo' => array('left' => array('usuario.id = usuario_grupo.usuario_id'))),
                            'order' => array('nome'  => 'asc'),
                            'where' => array('usuario_grupo.grupo_id in #id#'),
                        ),
                        'exec_params' => array(
                            'relation_table' => 'usuario_grupo',
                            'relation_column_attachment' => 'usuario_id',
                            'attachment_table' => 'usuario',
                            'extra_columns' => array(
                            ),
                        ),
                        'index_key'      => 'id', // índice para este attachment
                        'parent_key'     => 'id',
                        'attachment_key' => 'grupo_id',
                    ),
                ),
            ),
            'id'             => array(
                'type'                    => Cdc_Definition::TYPE_COLUMN,
                'primary'                 => true,
                'hide'                    => true,
                Cdc_Definition::OPERATION => array(
                    'read' => array(),
                    'item' => array(),
                    'create' => array(),
                    'update' => array(),
                    'delete' => array(),
                ),
            ),
            'nome' => array(
                'type'                      => Cdc_Definition::TYPE_COLUMN,
                'search' => array(
                    'operator' => 'like',
                ),
                Cdc_Definition::TYPE_WIDGET => array(
                    'attributes' => array(
                        'required'                => 'required',
                        'maxlength'               => 255,
                        'autocomplete'            => 'off',
                    ),
                ),
                Cdc_Definition::OPERATION => array(
                    'read' => array(),
                    'item' => array(),
                    'create' => array(),
                    'update' => array(),
                ),
                Cdc_Definition::TYPE_RULE => array(
                    array('Cdc_Rule_Trim'),
                    array('Cdc_Rule_Length', array(1, 255)),
                ),
            ),
            'usuario' => array(
                'type' => Cdc_Definition::TYPE_ATTACHMENT,
                Cdc_Definition::TYPE_ATTACHMENT => 'usuario',
                Cdc_Definition::TYPE_WIDGET => array(
                    'widget'   => 'multiselect',
                    'callback' => array(array($this, 'fetchKeyValue'), array(array('from' => array('usuario'), 'cols' => array('id', 'nome'), 'order' => array('nome asc')))),
                ),
                Cdc_Definition::OPERATION => array(
                    'read' => array(
                        Cdc_Definition::FORMATTER => array(array('Cdc_CellDataFormatter', 'formatArray'), array('nome')),
                    ),
                    'item' => array(),
                    'create' => array(),
                    'update' => array(),
                ),
                Cdc_Definition::TYPE_RULE => array(
                    array('Cdc_Rule_ArrayKeyExists'),
                ),
            ),
            'recurso' => array(
                'type' => Cdc_Definition::TYPE_ATTACHMENT,
                Cdc_Definition::TYPE_ATTACHMENT => 'recurso',
                Cdc_Definition::TYPE_WIDGET => array(
                    'widget'                  => 'multiselect',
                    'options'                 => Config::$resources,
                ),
                Cdc_Definition::OPERATION => array(
                    'read' => array(
                        Cdc_Definition::FORMATTER => array(array('Cdc_CellDataFormatter', 'formatArray'), array('id')),
                    ),
                    'item' => array(),
                    'create' => array(),
                    'update' => array(),
                ),
                Cdc_Definition::TYPE_RULE => array(
                    array('Cdc_Rule_ArrayKeyExists'),
                ),
            ),
            'sistema' => array(
                'type'                      => Cdc_Definition::TYPE_COLUMN,
                Cdc_Definition::TYPE_WIDGET => array(
                    'widget'                  => 'boolean',
                    'default'                 => false,
                ),
                Cdc_Definition::OPERATION => array(
                    'read' => array(
                        Cdc_Definition::FORMATTER => array(array('Cdc_CellDataFormatter', 'boolean')),
                    ),
                    'item' => array(),
                ),
            ),
        );
    }

    public function allow($row = array(), $rowset = array(), $index = null, $args = array())
    {
        if ($row['sistema'])
        {
            return false;
        }
        return parent::allow($row, $rowset, $index, $args);
    }

    public function execute($sql)
    {

        // Prevenir que a flag "Sistema" seja configurada por uma forma que não
        // seja diretamente no banco de dados
        if (in_array($sql->getOperation(), array('create')))
        {
            $sql->cols['sistema'] = 0;
        }
        else
        {
            unset($sql->cols['sistema']);
        }

        return parent::execute($sql);
    }

}