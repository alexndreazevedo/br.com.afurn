rm db.sqlite

php bin\schema.php > schema.sql

sqlite3 db.sqlite < schema.sql

php bin\fixtures.php
rm schema.sql

rm -rf public_html\files
mkdir public_html\files