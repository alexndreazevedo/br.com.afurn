<?php
header('Content-type: text/html; charset=UTF-8');

class Migration
{
    const ORIGEM = 0;
    const DESTINO = 1;
    public static $run = false;
    protected static $db = array();

    public static function pdo($layer = self::ORIGEM, $driver = null, $user = null, $pass = null, $options = null)
    {
        if(func_num_args() > 1)
        {
            self::$db[$layer] = new PDO($driver, $user, $pass, $options);
        }

        return self::$db[$layer];
    }

    public function execute(&$stmt, $fetch = false)
    {
        try
        {
            if($stmt instanceof  PDOStatement)
            {
                if($stmt->execute() === false)
                {
                    print implode("\t", $stmt->errorInfo()) . "\n";
                }
                else
                {
                    if($fetch)
                    {
                        return $stmt->fetchAll($fetch);
                    }
                }
            }
            else
            {
                return 'Nao foi possivel executar a requisicao';
            }
        }
        catch(PDOException $e)
        {
            print $e->getMessage();
        }
    }

    public function __construct($argv = array())
    {
        $host = 'pgsql:host=localhost;dbname=marinho_afurn';
        $user = 'postgres';
        $pass = 'pgadmin';

        $file = 'sqlite:' . dirname(dirname(__FILE__)) . DIRECTORY_SEPARATOR . 'db.sqlite';

        self::pdo(self::ORIGEM, $host, $user, $pass);
        self::pdo(self::DESTINO, $file);

        $run = $argv[1];
        $fixtures = $argv[2];

        if(array_key_exists(1, $argv))
        {
            self::$run = (bool) $argv[1];
        }

        if(array_key_exists(2, $argv))
        {
            if((bool) $argv[2])
            {
                include __DIR__ . DIRECTORY_SEPARATOR . 'fixtures.php';
            }
        }

        //$this->usuarios();
        $this->noticias();
    }

    public function usuarios()
    {
        $origem = self::pdo(self::ORIGEM)->prepare('select * from usuario');
        $usuarios = $this->execute($origem, PDO::FETCH_OBJ);

        if(self::$run)
        {
            $destino = self::pdo(self::DESTINO)->prepare('insert into usuario (nome, email, senha, administrador) values (:nome, :email, :senha, :administrador)');

            self::pdo(self::DESTINO)->beginTransaction();

            foreach ($usuarios as $usuario)
            {
                $destino->bindValue(':nome', $usuario->usuario_nome);
                $destino->bindValue(':email', $usuario->usuario_email);
                $destino->bindValue(':senha', $usuario->usuario_senha);
                $destino->bindValue(':administrador', (in_array($usuario->usuario_tipo, array('administrador', 'desenvolvimento')) ? 1 : 0));

                $this->execute($destino);
            }

            self::pdo(self::DESTINO)->commit();

            $verificacao = self::pdo(self::DESTINO)->prepare('select id, nome from usuario');

            print 'Registros salvos';
            print_r($this->execute($verificacao, PDO::FETCH_KEY_PAIR));
        }
    }

    public function noticias()
    {
        $origem = self::pdo(self::ORIGEM)->prepare('select * from noticia');
        $noticias = $this->execute($origem, PDO::FETCH_OBJ);

        if(self::$run)
        {
            $destino = self::pdo(self::DESTINO)->prepare('insert into noticia (id, noticia_categoria_id, titulo, subtitulo, slug, resumo, texto, criacao, atualizacao, publicado) values (:id, :noticia_categoria_id, :titulo, :subtitulo, :slug, :resumo, :texto, :criacao, :atualizacao, :publicado)');

            self::pdo(self::DESTINO)->beginTransaction();

            $NOTICIAS = 0;
            $AFURN = 1;
            $GERAIS = 2;
            $UFRN = 3;

            $categorias = array(
                $NOTICIAS => 4,
                $AFURN => 1,
                $GERAIS=> 5,
                $UFRN => 2,
            );

            foreach ($noticias as $noticia)
            {
                $destino->bindValue(':id', $noticia->noticia_id);
                $destino->bindValue(':noticia_categoria_id', $categorias[$noticia->noticia_categoria]);
                $destino->bindValue(':titulo', $noticia->noticia_titulo);
                $destino->bindValue(':subtitulo', '');
                $destino->bindValue(':slug', $noticia->slug);
                $destino->bindValue(':resumo', $noticia->noticia_resumo);
                $destino->bindValue(':texto', $noticia->noticia_texto);
                $destino->bindValue(':criacao', $noticia->noticia_data);
                $destino->bindValue(':atualizacao', $noticia->noticia_data);
                $destino->bindValue(':publicado', $noticia->noticia_publicado);

                $this->execute($destino);
            }

            self::pdo(self::DESTINO)->commit();
        
            $verificacao = self::pdo(self::DESTINO)->prepare('select id, titulo from noticia');

            print 'Registros salvos';
            print_r($this->execute($verificacao, PDO::FETCH_KEY_PAIR));
        }
    }
}

$migration = new Migration($argv);