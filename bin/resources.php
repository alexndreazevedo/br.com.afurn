<?php

$base = dirname(dirname(__FILE__));
include $base . '/boot.php';

$db = Cdc_Pdo_Pool::getConnection(Config::$default_db);

$existentes = $db->query('select id, id from recurso order by id')->fetchAll(PDO::FETCH_KEY_PAIR);

$stmt = $db->prepare('insert into recurso (id) values (?)');

$bolo = Config::$resources;

foreach ($bolo as $key => $value)
{
    unset($existentes[$key]);
    try
    {
        $stmt->bindValue(1, $key);
        $stmt->execute();
    }
    catch (Exception $e)
    {
        echo 'Recurso "' . $value . '" já existe', PHP_EOL;
    }
}

if (!empty($existentes))
{
    echo 'Recursos órfãos: ' . implode(', ', $existentes), PHP_EOL;
}
