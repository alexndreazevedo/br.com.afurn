<?php
include dirname(dirname(__FILE__)) . '/init.php';

$hash = Config::$hasher;

if (!isset($argv[1]))
{
    die('Uso: ' . $argv[0] . ' <senha>' . PHP_EOL);
}
echo $argv[1], ' => ', $hash->hashPassword($argv[1]), PHP_EOL;