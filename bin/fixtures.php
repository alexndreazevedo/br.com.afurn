<?php

include dirname(dirname(__FILE__)) . DIRECTORY_SEPARATOR . 'init.php';

foreach (Config::$modules as $key => $value)
{
    if ($key == '')
    {
        $sep = ''; // @TODO: this also won't work with namespaces
    }
    else
    {
        $sep = '_';
    }

    $resources = $key . $sep . 'Metadata_Fixtures';
    if (class_exists($resources))
    {
        try
        {
            call_user_func(array($resources, 'setup'));
        }
        catch (Exception $e)
        {
            echo $e->getMessage(), PHP_EOL, $e->getTraceAsString(), PHP_EOL, PHP_EOL;
        }


    }
}
