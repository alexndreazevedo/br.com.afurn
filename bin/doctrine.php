<?php

include dirname(dirname(__FILE__)) . DIRECTORY_SEPARATOR . 'init.php';

include $environment['vendor_abs'] . DIRECTORY_SEPARATOR . 'autoload.php';

$config = new \Doctrine\DBAL\Configuration;

$connectionParams = array(
    'pdo' => Cdc_Pdo_Pool::getConnection(),
);

$conn = \Doctrine\DBAL\DriverManager::getConnection($connectionParams, $config);


$helperSet = new \Symfony\Component\Console\Helper\HelperSet(array(
    'db' => new \Doctrine\DBAL\Tools\Console\Helper\ConnectionHelper($conn)
));

$cli = new \Symfony\Component\Console\Application('Doctrine Command Line Interface', Doctrine\DBAL\Version::VERSION);
$cli->setCatchExceptions(true);
$cli->setHelperSet($helperSet);
$cli->addCommands(array(
    // DBAL Commands
    new \Doctrine\DBAL\Tools\Console\Command\RunSqlCommand(),
    new \Doctrine\DBAL\Tools\Console\Command\ImportCommand(),
    new \Doctrine\DBAL\Tools\Console\Command\ReservedWordsCommand(),
));
$cli->run();