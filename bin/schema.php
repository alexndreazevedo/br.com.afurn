<?php

include dirname(dirname(__FILE__)) . DIRECTORY_SEPARATOR . 'init.php';

include Config::$vendor_abs . 'autoload.php';

$config = new \Doctrine\DBAL\Configuration;

$connectionParams = array(
    'pdo' => Cdc_Pdo_Pool::getConnection(),
);

$conn = \Doctrine\DBAL\DriverManager::getConnection($connectionParams, $config);


$schema = new \Doctrine\DBAL\Schema\Schema;

foreach (Config::$modules as $key => $value)
{
    if ($key == '')
    {
        $sep = ''; // @TODO: this also won't work with namespaces
    }
    else
    {
        $sep = '_';
    }

    $resources = $key . $sep . 'Metadata_Schema';
    if (class_exists($resources))
    {
        call_user_func(array($resources, 'setup'), $schema);
    }
}

$sql = $schema->toSql($conn->getDatabasePlatform());

foreach ($sql as $s)
{
    echo $s, ';' . PHP_EOL;
}


