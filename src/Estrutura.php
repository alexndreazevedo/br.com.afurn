<?php

class Estrutura extends Datastore
{
    public $sedes = null;

    const COMPLEXO_MIRASSOL = 1;
    const SEDE_BUZIOS = 2;
    const SEDE_SANTACRUZ = 3;
    const POSTO_RIBEIRA = 4;

    public static $titulos = array(

        self::COMPLEXO_MIRASSOL => 'Complexo em Mirassol',
        self::SEDE_BUZIOS => 'Sede em Búzios',
        self::SEDE_SANTACRUZ => 'Sede em Santa Cruz',
        self::POSTO_RIBEIRA => 'Posto de Atend. Ribeira',

    );

    public static $texto = array(

        self::COMPLEXO_MIRASSOL => 'AFURN – A Associação dos Funcionários da Universidade Federal do Rio Grande do Norte (AFURN) tem como principal objetivo desenvolver atividades de socialização, que proporcionem qualidade de vida e bem estar para seus associados e dependentes. Para um atendimento personalizado a entidade dispõe de sede administrativa, na qual oferece toda assistência ao associado, com profissionais qualificados e treinados.
      A sede administrativa está localizada na Rua das Violetas, 628 B, Mirassol. Mais informações pelo telefone: 3231-6330.',
        self::SEDE_BUZIOS => 'A AFURN também oferece ao seu associado espaço para lazer e diversão, é a Sede Social de Búzios que possui estrutura completa de clube para festa e eventos sociais, com restaurante, piscina, salão de festa, jardim e estacionamento.
       A sede social é localizada na Praia de Búzios, munícipio de Nísia Floresta, a apenas 20 Km de Natal.',
        self::SEDE_SANTACRUZ => 'A AFURN oferece ao seu associado que trabalha e reside no interior do Estado sua Sede Social de Santa Cruz, localizada à 115 Km de Natal.
O espaço possui estrutura completa para festa e eventos sociais, com restaurante, salão de festa, jardim e estacionamento.',
        self::POSTO_RIBEIRA => '',

    );

    public static $localizacao = array(
        self::COMPLEXO_MIRASSOL => array(
            'endereco'  => 'Rua das Violetas, 628-B - Mirassol - Natal/RN - CEP: 59078-160',
            'latitude'  => '-5.843624',
            'longitude' => '-35.206375',
        )
    );

    public function init()
    {
        $this->struct = array(
            'estrutura' => array(
                'type' => Cdc_Definition::TYPE_RELATION,
                'statement_type' => Cdc_Definition::STATEMENT_SELECT,
                Cdc_Definition::OPERATION => array(
                    'read' => array(),
                    'item' => array(),
                    'create' => array(
                        'statement_type' => Cdc_Definition::STATEMENT_INSERT,
                    ),
                    'update'         => array(
                        'statement_type' => Cdc_Definition::STATEMENT_UPDATE,
                    ),
                    'delete'         => array(
                        'statement_type' => Cdc_Definition::STATEMENT_DELETE,
                    ),
                ),
                Cdc_Definition::TYPE_ATTACHMENT => array(
                    'arquivo' => self::arquivoAttachment('estrutura'),
                ),
            ),
            'id' => self::primaryColumn(),
            'imagem' => self::arquivoColumn(),
            'slug' => self::slugColumn(),
            'nome' => self::setRequired(self::textColumn()),
            'endereco' => self::setRequired(self::textColumn(null, null, 255, 0, array(
                'item' => array(),
                'create' => array(),
                'update' => array(),
            ))),
            'latitude' => array(
                'type' => Cdc_Definition::TYPE_COLUMN,
                Cdc_Definition::TYPE_WIDGET => array(
                    'attributes' => array(
                        'id' => 'latitude',
                    ),
                ),
                Cdc_Definition::OPERATION => array(
                    'item' => array(),
                    'create' => array(),
                    'update' => array(),
                ),
                Cdc_Definition::TYPE_RULE => array(
                    array('Cdc_Rule_Trim'),
                ),
            ),
            'longitude' => array(
                'type' => Cdc_Definition::TYPE_COLUMN,
                Cdc_Definition::TYPE_WIDGET => array(
                    'attributes' => array(
                        'id' => 'longitude',
                    ),
                ),
                Cdc_Definition::OPERATION => array(
                    'item' => array(),
                    'create' => array(),
                    'update' => array(),
                ),
                Cdc_Definition::TYPE_RULE => array(
                    array('Cdc_Rule_Trim'),
                ),
            ),
            'telefone' => self::setRequired(self::textColumn(null, null, 255, 0, array(
                'item' => array(),
                'create' => array(),
                'update' => array(),
            ))),
            'email' => self::setRequired(self::emailColumn()),
            'estrutura_responsavel_id' => array(
                'type' => Cdc_Definition::TYPE_COLUMN,
                Cdc_Definition::TYPE_WIDGET => array(
                    'widget' => 'select',
                    'callback' => array(array($this, 'fetchKeyValue'), array(array('from' => array('equipe'), 'cols' => array('id', 'nome'), 'order' => array('ordem' => 'asc')))),
                ),
                Cdc_Definition::TYPE_RULE => array(
                    array('Cdc_Rule_Trim'),
                    array('Cdc_Rule_ArrayKeyExists'),
                ),
                Cdc_Definition::OPERATION => array(
                    'read' => array(
                        Cdc_Definition::FORMATTER => array(array($this, 'formatter'), array($this->getPdo(), 'equipe')),
                    ),
                    'item' => array(
                        Cdc_Definition::FORMATTER => array(array($this, 'formatter'), array($this->getPdo(), 'equipe')),
                    ),
                    'create' => array(),
                    'update' => array(),
                ),
            ),
            'texto' => self::richColumn(),
        );
    }

    public function getHtmlOptions($row, $rowset, $args)
    {
        if (!$this->allow($row))
        {
            return '';
        }

        $return = '';
        $link_args = array(
            'r' => $args['controller']->relation
        );
        $link_http_params = array(
            'op' => 'update',
            $args['controller']->primary => $row[$args['controller']->primary],
        );
        $return .= '<a class="btn" rel="tooltip" title="Editar" href="' . $args['controller']->link('admin', $link_args, $link_http_params) . '"><i class="fa fa-pencil"></i></a>';

        return '<div class="btn-group">' . $return . '</div>';
    }

    public function slugs()
    {
        if (null === $this->sedes)
        {
            $sql = new Cdc_Sql_Select($this->getPdo());
            $sql->cols = array('id', 'slug', 'nome');
            $sql->from = array('estrutura');
            $this->sedes = Cdc_ArrayHelper::keyPair($sql->stmt()->fetchAll());
        }
        return $this->sedes;
    }
    public function execute($sql)
    {
        if ($sql->getOperation() == 'create' || $sql->getOperation() == 'update')
        {
            $sql->cols['slug'] = Cdc_Texto::slug($sql->cols['nome']);
        }

        return parent::execute($sql);
    }

}