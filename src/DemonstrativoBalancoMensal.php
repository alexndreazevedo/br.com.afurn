<?php

class DemonstrativoBalancoMensal extends Datastore
{
    public static $meses = array();
    public static $anos = array();

    public function init()
    {
        self::$meses = Datastore::$meses;

        foreach(range(2010, date('Y')) as $ano)
        {
            self::$anos[$ano] = $ano;
        }

        $this->struct = array(
            'demonstrativo_balancomensal' => array(
                'type' => Cdc_Definition::TYPE_RELATION,
                'statement_type' => Cdc_Definition::STATEMENT_SELECT,
                Cdc_Definition::OPERATION => array(
                    'read' => array(),
                    'item' => array(),
                    'create' => array(
                        'statement_type' => Cdc_Definition::STATEMENT_INSERT,
                    ),
                    'update' => array(
                        'statement_type' => Cdc_Definition::STATEMENT_UPDATE,
                    ),
                    'delete' => array(
                        'statement_type' => Cdc_Definition::STATEMENT_DELETE,
                    ),
                ),
            ),
            'id' => self::primaryColumn(),
            'demonstrativo_balancomensal_ano' => array(
                'type' => Cdc_Definition::TYPE_COLUMN,
                'search' => array(
                    'operator' => '=',
                ),
                Cdc_Definition::TYPE_WIDGET => array(
                    'widget' => 'select',
                    'options' => self::$anos,
                    'attributes' => array(
                        'required' => 'required',
                    ),
                ),
                Cdc_Definition::OPERATION => array(
                    'read' => array(
                        Cdc_Definition::FORMATTER => array(array('Cdc_CellDataFormatter', 'arrayValue'), array(self::$anos)),
                    ),
                    'item' => array(),
                    'create' => array(),
                    'update' => array(),
                ),
                Cdc_Definition::TYPE_RULE => array(
                    array('Cdc_Rule_Required'),
                    array('Cdc_Rule_Trim'),
                    array('Cdc_Rule_ArrayKeyExists'),
                ),
            ),
            'demonstrativo_balancomensal_mes' => array(
                'type' => Cdc_Definition::TYPE_COLUMN,
                'search' => array(
                    'operator' => '=',
                ),
                Cdc_Definition::TYPE_WIDGET => array(
                    'widget' => 'select',
                    'options' => self::$meses,
                    'attributes' => array(
                        'required' => 'required',
                    ),
                ),
                Cdc_Definition::OPERATION => array(
                    'read' => array(
                        Cdc_Definition::FORMATTER => array(array('Cdc_CellDataFormatter', 'arrayValue'), array(self::$meses)),
                    ),
                    'item' => array(),
                    'create' => array(),
                    'update' => array(),
                ),
                Cdc_Definition::TYPE_RULE => array(
                    array('Cdc_Rule_Required'),
                    array('Cdc_Rule_Trim'),
                    array('Cdc_Rule_ArrayKeyExists'),
                ),
            ),
        'demonstrativo_fluxocaixa_id' => array(
            'type' => Cdc_Definition::TYPE_COLUMN,
            'search' => array(
                'operator' => '=',
            ),
            Cdc_Definition::TYPE_WIDGET => array(
                'widget' => 'select',
                'callback' => array(array($this, 'fetchKeyValue'), array(array('from' => array('demonstrativo_fluxocaixa fc'), 'cols' => array('fc.id', DemonstrativoCategoria::replace('c.nome || \' - \' || fc.nome')), 'order' => array('c.ordem' => 'asc', 'fc.ordem' => 'asc'), 'join' => array('demonstrativo_categoria c' => array('inner' => array('fc.demonstrativo_categoria_id = c.id'))), 'where' => array('fc.publicado =' => true, 'c.publicado =' => true)))),
            ),
            Cdc_Definition::TYPE_RULE => array(
                array('Cdc_Rule_Trim'),
                array('Cdc_Rule_ArrayKeyExists'),
            ),
            Cdc_Definition::OPERATION => array(
                'item' => array(
                    Cdc_Definition::FORMATTER => array(array($this, 'formatter'), array($this->getPdo(), 'demonstrativo_fluxocaixa', DemonstrativoCategoria::replace('c.nome || \' - \' || fc.nome'))),
                ),
                'create' => array(),
                'update' => array(),
            ),
        ),
            'valor' => self::setRequired(self::textColumn('money')),
            'detalhes' => self::textareaColumn(),
            'ordem' => self::orderColumn(),
            'publicado' => self::publishedColumn(),
        );
    }

    public function getHtmlOptions($row, $rowset, $args)
    {
        if (!$this->allow($row))
        {
            return '';
        }

        $return = '';
        $link_args = array(
            'r' => $args['controller']->relation
        );
        $link_http_params = array(
            'op' => 'update',
            $args['controller']->primary => $row[$args['controller']->primary],
        );
        $return .= '<a class="btn" rel="tooltip" title="Editar" href="' . $args['controller']->link('admin', $link_args, $link_http_params) . '"><i class="fa fa-pencil"></i></a>';

        return '<div class="btn-group">' . $return . '</div>';
    }
}