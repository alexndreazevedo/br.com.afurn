<?php

class Equipe extends Datastore
{
    const TIPO_PRESIDENTE = 1;
    const TIPO_DIRETORIA = 2;
    const TIPO_ASSESSORIA = 3;
    const TIPO_FUNCIONARIO = 4;

    public static $tipo = array(
        self::TIPO_PRESIDENTE => 'Presidência',
        self::TIPO_DIRETORIA => 'Diretoria',
        self::TIPO_ASSESSORIA => 'Assessoria',
        self::TIPO_FUNCIONARIO => 'Funcionário',
    );

    public function init()
    {
        $this->struct = array(
            'equipe' => array(
                'type' => Cdc_Definition::TYPE_RELATION,
                'statement_type' => Cdc_Definition::STATEMENT_SELECT,
                Cdc_Definition::OPERATION => array(
                    'read' => array(),
                    'item' => array(),
                    'create' => array(
                        'statement_type' => Cdc_Definition::STATEMENT_INSERT,
                    ),
                    'update' => array(
                        'statement_type' => Cdc_Definition::STATEMENT_UPDATE,
                    ),
                    'delete' => array(
                        'statement_type' => Cdc_Definition::STATEMENT_DELETE,
                    ),
                ),
                Cdc_Definition::TYPE_ATTACHMENT => array(
                    'arquivo' => self::arquivoAttachment('equipe'),
                ),
            ),
            'id' => self::primaryColumn(),
            'imagem' => self::arquivoColumn(),
            'nome' => self::setRequired(array_merge(array('search' => array('operator' => 'like')), self::textColumn())),
            'email' => self::emailColumn(),
            'cargo' => self::setRequired(array_merge(array('search' => array('operator' => 'like')), self::textColumn())),
            'cargo_tipo_id' => array(
                'type' => Cdc_Definition::TYPE_COLUMN,
                'search' => array(
                    'operator' => '=',
                ),
                Cdc_Definition::TYPE_WIDGET => array(
                    'widget' => 'select',
                    'options' => self::$tipo,
                    'attributes' => array(
                        'required' => 'required',
                    ),
                ),
                Cdc_Definition::OPERATION => array(
                    'read' => array(
                        Cdc_Definition::FORMATTER => array(array('Cdc_CellDataFormatter', 'arrayValue'), array(self::$tipo)),
                    ),
                    'item' => array(),
                    'create' => array(),
                    'update' => array(),
                ),
                Cdc_Definition::TYPE_RULE => array(
                    array('Cdc_Rule_Required'),
                    array('Cdc_Rule_Trim'),
                    array('Cdc_Rule_ArrayKeyExists'),
                ),
            ),
            'exercicio_inicio' => self::dateColumn(array(
                'item' => array(),
                'create' => array(),
                'update' => array(),
            )),
            'exercicio_fim' => self::dateColumn(array(
                'item' => array(),
                'create' => array(),
                'update' => array(),
            )),
            'detalhes' => self::textareaColumn(),
            'ordem' => self::orderColumn(),
            'publicado' => self::publishedColumn(),
        );
    }

    public function getHtmlOptions($row, $rowset, $args)
    {
        if (!$this->allow($row))
        {
            return '';
        }

        $return = '';
        $link_args = array(
            'r' => $args['controller']->relation
        );
        $link_http_params = array(
            'op' => 'update',
            $args['controller']->primary => $row[$args['controller']->primary],
        );
        $return .= '<a class="btn" rel="tooltip" title="Editar" href="' . $args['controller']->link('admin', $link_args, $link_http_params) . '"><i class="fa fa-pencil"></i></a>';

        return '<div class="btn-group">' . $return . '</div>';
    }
}