<?php

use \Doctrine\DBAL\Schema\Schema;
use \Doctrine\DBAL\Schema\Table;

class Metadata_Schema
{

    public static function addPrimary(Table $table)
    {
        $table->addColumn('id', 'integer', array('autoincrement' => true));
        $table->setPrimaryKey(array('id'));
    }

    public static function addForeignKey(Table $table, Table $foreignTable, $column, $action = 'RESTRICT')
    {
        $col = $table->addColumn($column, 'integer', array('notnull' => false));
        $table->addForeignKeyConstraint($foreignTable, array($column), array('id'), array('onUpdate' => 'CASCADE', 'onDelete' => $action));
        return $col;
    }

    public static function createGenericTable(Schema $schema, $nome, $titleColumn = 'nome')
    {
        $table = $schema->createTable($nome);
        self::addPrimary($table);
        if($titleColumn !== false)
        {
            $table->addColumn($titleColumn, 'string', array('length' => 255, 'notnull' => true));
        }
        return $table;
    }

    public static function createRelation(Schema $schema, $name, Table $table1, $col1, Table $table2, $col2, $onDelete1 = 'CASCADE', $onDelete2 = 'CASCADE')
    {
        $relation = $schema->createTable($name);
        $relation->addColumn($col1, 'integer');
        $relation->addColumn($col2, 'integer');
        $relation->setPrimaryKey(array($col1, $col2));
        $relation->addForeignKeyConstraint($table1, array($col1), array('id'), array('onUpdate' => 'CASCADE', 'onDelete' => $onDelete1));
        $relation->addForeignKeyConstraint($table2, array($col2), array('id'), array('onUpdate' => 'CASCADE', 'onDelete' => $onDelete2));

        return $relation;
    }

    public static function createArquivoTable(Schema $schema, Table $table, $n2n, $column)
    {
        $arquivo = $schema->getTable('arquivo');
        $arq_table = $schema->createTable($n2n);
        $arq_table->addColumn($column, 'integer');
        $arq_table->addColumn('arquivo_id', 'integer');
        $arq_table->addColumn('regiao', 'string', array('length' => 25));
        $arq_table->addColumn('ordem', 'integer', array('notnull' => false));
        $arq_table->addForeignKeyConstraint($table, array($column), array('id'), array('onUpdate' => 'CASCADE', 'onDelete' => 'CASCADE'));
        $arq_table->addForeignKeyConstraint($arquivo, array('arquivo_id'), array('id'), array('onUpdate' => 'CASCADE', 'onDelete' => 'CASCADE'));
        $arq_table->setPrimaryKey(array($column, 'arquivo_id', 'regiao'));
    }

    public static function setup(Schema $schema)
    {
        $vitrine = self::createGenericTable($schema, 'vitrine', 'titulo');
        $vitrine->addColumn('texto', 'text', array('notnull' => false));
        $vitrine->addColumn('ordem', 'integer', array('notnull' => false));
        $vitrine->addColumn('publicado', 'boolean', array('notnull' => false));
        $vitrine->addColumn('url', 'text', array('notnull' => false));
        $vitrine->addColumn('janela_destino', 'string', array('length' => 255));
        self::createArquivoTable($schema, $vitrine, 'vitrine_arquivo', 'vitrine_id');

        $noticia_categoria = self::createGenericTable($schema, 'noticia_categoria');
        $noticia_categoria->addColumn('slug', 'string', array('length' => 220, 'notnull' => false));

        $noticia = self::createGenericTable($schema, 'noticia', 'titulo');
        $noticia->addColumn('subtitulo', 'string', array('length' => 255));
        $noticia->addColumn('slug', 'string', array('length' => 220, 'notnull' => false));
        self::addForeignKey($noticia, $noticia_categoria, 'noticia_categoria_id', 'CASCADE');
        $noticia->addColumn('resumo', 'text', array('notnull' => false));
        $noticia->addColumn('texto', 'text', array('notnull' => false));
        $noticia->addColumn('criacao', 'text', array('notnull' => false)); // @TODO adicionar fuleragem de data
        $noticia->addColumn('atualizacao', 'text', array('notnull' => false)); // @TODO adicionar fuleragem de data
        $noticia->addColumn('publicado', 'boolean', array('notnull' => false));
        self::createArquivoTable($schema, $noticia, 'noticia_arquivo', 'noticia_id');

        $equipe = self::createGenericTable($schema, 'equipe');
        $equipe->addColumn('email', 'string', array('length' => 255, 'notnull' => false));
        $equipe->addColumn('cargo', 'string', array('length' => 255, 'notnull' => false));
        $equipe->addColumn('cargo_tipo_id', 'string', array('length' => 3, 'notnull' => false));
        $equipe->addColumn('exercicio_inicio', 'text', array('notnull' => false)); // @TODO adicionar fuleragem de data
        $equipe->addColumn('exercicio_fim', 'text', array('notnull' => false)); // @TODO adicionar fuleragem de data
        $equipe->addColumn('detalhes', 'text', array('notnull' => false));
        $equipe->addColumn('ordem', 'integer', array('length' => 3, 'notnull' => false, 'default' => 0));
        $equipe->addColumn('publicado', 'boolean', array('notnull' => false));
        self::createArquivoTable($schema, $equipe, 'equipe_arquivo', 'equipe_id');

        $estrutura = self::createGenericTable($schema, 'estrutura');
        $estrutura->addColumn('slug', 'string', array('length' => 220, 'notnull' => false));
        self::addForeignKey($estrutura, $equipe, 'estrutura_responsavel_id', 'CASCADE');
        $estrutura->addColumn('endereco', 'string', array('length' => 255, 'notnull' => false));
        $estrutura->addColumn('latitude', 'string', array('notnull' => false, 'length' => 255));
        $estrutura->addColumn('longitude', 'string', array('notnull' => false, 'length' => 255));
        $estrutura->addColumn('telefone', 'string', array('length' => 255, 'notnull' => false));
        $estrutura->addColumn('email', 'string', array('length' => 255, 'notnull' => false));
        $estrutura->addColumn('texto', 'text', array('notnull' => false));
        self::createArquivoTable($schema, $estrutura, 'estrutura_arquivo', 'estrutura_id');

        $servico = self::createGenericTable($schema, 'servico');
        $servico->addColumn('slug', 'string', array('length' => 220, 'notnull' => false));
        self::addForeignKey($servico, $estrutura, 'servico_estrutura_id', 'CASCADE');
        self::addForeignKey($servico, $equipe, 'servico_responsavel_id', 'CASCADE');
        $servico->addColumn('texto', 'text', array('notnull' => false));
        self::createArquivoTable($schema, $servico, 'servico_arquivo', 'servico_id');

        $parceria = self::createGenericTable($schema, 'parceria');
        $parceria->addColumn('descricao', 'text', array('notnull' => false));
        $parceria->addColumn('endereco', 'string', array('length' => 255, 'notnull' => false));
        $parceria->addColumn('telefone', 'string', array('length' => 255, 'notnull' => false));
        $parceria->addColumn('email', 'string', array('length' => 255, 'notnull' => false));
        $parceria->addColumn('ordem', 'integer', array('length' => 3, 'notnull' => false, 'default' => 0));
        $parceria->addColumn('parceria_tipo_id', 'string', array('length' => 3, 'notnull' => false));
        $parceria->addColumn('publicado', 'boolean', array('notnull' => false));
        self::createArquivoTable($schema, $parceria, 'parceria_arquivo', 'parceria_id');

        $demonstrativo_categoria = self::createGenericTable($schema, 'demonstrativo_categoria');
        $demonstrativo_categoria->addColumn('demonstrativo_tipo_id', 'string', array('length' => 3, 'notnull' => false));
        $demonstrativo_categoria->addColumn('detalhes', 'text', array('notnull' => false));
        $demonstrativo_categoria->addColumn('ordem', 'integer', array('length' => 3, 'notnull' => false, 'default' => 0));
        $demonstrativo_categoria->addColumn('publicado', 'boolean', array('notnull' => false));

        $demonstrativo_fluxocaixa = self::createGenericTable($schema, 'demonstrativo_fluxocaixa');
        self::addForeignKey($demonstrativo_fluxocaixa, $demonstrativo_categoria, 'demonstrativo_categoria_id', 'CASCADE');
        $demonstrativo_fluxocaixa->addColumn('detalhes', 'text', array('notnull' => false));
        $demonstrativo_fluxocaixa->addColumn('ordem', 'integer', array('length' => 3, 'notnull' => false, 'default' => 0));
        $demonstrativo_fluxocaixa->addColumn('publicado', 'boolean', array('notnull' => false));

        $demonstrativo_balancomensal = self::createGenericTable($schema, 'demonstrativo_balancomensal', 'valor');
        self::addForeignKey($demonstrativo_balancomensal, $demonstrativo_fluxocaixa, 'demonstrativo_fluxocaixa_id', 'CASCADE');
        $demonstrativo_balancomensal->addColumn('demonstrativo_balancomensal_mes', 'string', array('length' => 3, 'notnull' => false));
        $demonstrativo_balancomensal->addColumn('demonstrativo_balancomensal_ano', 'string', array('length' => 4, 'notnull' => false));
        $demonstrativo_balancomensal->addColumn('detalhes', 'text', array('notnull' => false));
        $demonstrativo_balancomensal->addColumn('ordem', 'integer', array('length' => 3, 'notnull' => false, 'default' => 0));
        $demonstrativo_balancomensal->addColumn('publicado', 'boolean', array('notnull' => false));

        // ANTIGA DEFINIÇÃO DE TABELAS
        /*
        $empreendimento = self::createGenericTable($schema, 'empreendimento');
        $empreendimento->addColumn('slug', 'string', array('length' => 220, 'notnull' => false));
        $empreendimento->addColumn('descricao', 'text', array('notnull' => false));
        $empreendimento->addColumn('destaque', 'boolean', array('notnull' => false));
        self::createArquivoTable($schema, $empreendimento, 'empreendimento_arquivo', 'empreendimento_id');

        $oportunidade = self::createGenericTable($schema, 'oportunidade');
        $oportunidade->addColumn('slug', 'string', array('length' => 220, 'notnull' => false));
        $oportunidade->addColumn('descricao', 'text', array('notnull' => false));
        $oportunidade->addColumn('destaque', 'boolean', array('notnull' => false));
        self::createArquivoTable($schema, $oportunidade, 'oportunidade_arquivo', 'oportunidade_id');
         *
         */

        /*
        $faixa = self::createGenericTable($schema, 'faixa');
        $faixa->addColumn('ordem', 'integer', array('notnull' => false));

        $tipo = self::createGenericTable($schema, 'tipo');
        $tipo->addColumn('ordem', 'integer', array('notnull' => false));

        $area = self::createGenericTable($schema, 'area');
        $area->addColumn('ordem', 'integer', array('notnull' => false));

        $quarto = self::createGenericTable($schema, 'quarto');
        $quarto->addColumn('ordem', 'integer', array('notnull' => false));

        $bairro = self::createGenericTable($schema, 'bairro');
        $bairro->addColumn('ordem', 'integer', array('notnull' => false));


        $imovel = self::createGenericTable($schema, 'imovel');
        $imovel->addColumn('slug', 'string', array('length' => 220, 'notnull' => false));
        $imovel->addColumn('codigo', 'string', array('notnull' => false, 'length' => 255));
        $imovel->addColumn('valor', 'decimal', array('precision' => 19, 'scale' => 4));
        $imovel->addColumn('interesse_id', 'string', array('length' => 2, 'notnull' => false));
        self::addForeignKey($imovel, $faixa, 'faixa_id', 'CASCADE');
        self::addForeignKey($imovel, $tipo, 'tipo_id', 'CASCADE');
        self::addForeignKey($imovel, $area, 'area_id', 'CASCADE');
        self::addForeignKey($imovel, $quarto, 'quarto_id', 'CASCADE');
        self::addForeignKey($imovel, $bairro, 'bairro_id', 'CASCADE');
        $imovel->addColumn('cidade', 'string', array('notnull' => false, 'length' => 255));
        $imovel->addColumn('estado_id', 'string', array('length' => 2, 'notnull' => false));
        $imovel->addColumn('latitude', 'string', array('notnull' => false, 'length' => 255));
        $imovel->addColumn('longitude', 'string', array('notnull' => false, 'length' => 255));
        $imovel->addColumn('caracteristicas', 'text', array('notnull' => false));
        $imovel->addColumn('descricao', 'text', array('notnull' => false));
        $imovel->addColumn('destaque', 'boolean', array('notnull' => false));
        self::createArquivoTable($schema, $imovel, 'imovel_arquivo', 'imovel_id');
        */

    }

}
