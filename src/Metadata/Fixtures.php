<?php

class Metadata_Fixtures
{

    public static function setup()
    {
        $pdo = Config::connection();

        self::create($pdo);
        self::update($pdo);
    }

    public static function create($pdo)
    {
        $create = array(
            'Base_ArquivoTipo' => array(
                array(
                    'nome' => 'Imagem',
                    'extensoes' => 'jpg jpeg gif png',
                ),
            ),
    	    'Base_Usuario' => array(
                array(
                     'nome' => 'AFURN',
                     'email' => 'contato@afurn.com.br',
                     'senha' => 'Afurn!@#',
                     'administrador' => true,
                ),
            ),
        );

        // INSERIR EMAILS DE CONTATO AUTOMATICAMENTE
        foreach (Contato::$titulos as $key => $value)
        {
            $create['Contato_Email'][] = array(
                'secao' => $key,
                'nome' => $value,
                'email' => 'contato@afurn.com.br',
            );
        }


        if(Config::$debug)
        {
            // INSERIR EQUIPE
            $equipe = array(
                array(
                    'nome' => 'Nilberto Galvão',
                    'email' => 'nilberto@hotmail.com',
                    'cargo' => 'Presidente',
                    'cargo_tipo_id' => Equipe::TIPO_PRESIDENTE,
                ),
                array(
                    'nome' => 'Nilberto Medeiros',
                    'email' => 'nilberto@yahoo.com',
                    'cargo' => 'Secretário',
                    'cargo_tipo_id' => Equipe::TIPO_DIRETORIA,
                ),
                array(
                    'nome' => 'Nilberto Azevedo',
                    'email' => 'nilberto@gmail.com',
                    'cargo' => 'Servente',
                    'cargo_tipo_id' => Equipe::TIPO_FUNCIONARIO,
                ),
            );

            foreach ($equipe as $id => $pessoa)
            {
                $create['Equipe'][] = array_merge(array('ordem' => $id, 'publicado' => true), $pessoa);
            }

            // INSERIR NOTICIAS
            /*
            $total = 30;
            $params = array(
                'type' => 'all-meat',
                'paras' => $total,
                'start-with-lorem' => 1,
            );
            $url = 'https://baconipsum.com/api/';
            $curl = curl_init();
            $options = array(
                CURLOPT_HEADER => 0,
                CURLOPT_SSL_VERIFYPEER => false,
                CURLOPT_RETURNTRANSFER => true,
                CURLOPT_POST => true,
                CURLOPT_POSTFIELDS => $params,
                CURLOPT_URL => $url,
            );
            curl_setopt_array($curl, $options);
            $return = curl_exec($curl);
            $noticias = json_decode($return);
             *
             */

            $categorias = array(
                'AFURN',
                'UFRN',
                'Concursos',
                'Diversão e lazer',
                'Servidores',
                'Terceira idade',
            );

            foreach ($categorias as $id => $categoria)
            {
                $create['NoticiaCategoria'][] = array(
                    'nome' => $categoria,
                );
            }

            /*
            foreach ($noticias as $id => $noticia)
            {
                $create['Noticia'][] = array(
                    'titulo' => substr($noticia, 0, 70),
                    'subtitulo' => substr($noticia, 0, 100),
                    'noticia_categoria_id' => rand(1, count($categorias)),
                    'resumo' => substr($noticia, 0, 180) . '...',
                    'texto' => '<p>' . $noticia . '</p><p>' . $noticia . '</p><p>' . $noticia . '</p><p>' . $noticia . '</p><p>' . $noticia . '</p><p>' . $noticia . '</p><p>' . $noticia . '</p><p>' . $noticia . '</p>',
                );
            }
             *
             */

            // INSERIR DEMONSTRATIVO
            $demonstrativo_categoria = array(
                array(
                    'nome' => 'Contribuições',
                    'demonstrativo_tipo_id' => DemonstrativoCategoria::TIPO_ENTRADA,
                ),
                array(
                    'nome' => 'Outras Entradas de Caixa',
                    'demonstrativo_tipo_id' => DemonstrativoCategoria::TIPO_ENTRADA,
                ),
                array(
                    'nome' => 'Despesas',
                    'demonstrativo_tipo_id' => DemonstrativoCategoria::TIPO_SAIDA,
                ),
                array(
                    'nome' => 'Pessoal e Serviços',
                    'demonstrativo_tipo_id' => DemonstrativoCategoria::TIPO_SAIDA,
                ),
                array(
                    'nome' => 'Impostos e Tributos',
                    'demonstrativo_tipo_id' => DemonstrativoCategoria::TIPO_SAIDA,
                ),
            );

            $i = 1;

            $demonstrativo_fluxocaixa = array(
                array(
                    'demonstrativo_categoria_id' => 1,
                    'nome' => 'Averbações',
                ),
                array(
                    'demonstrativo_categoria_id' => 1,
                    'nome' => 'Fornecedores Conveniados',
                ),
                array(
                    'demonstrativo_categoria_id' => 3,
                    'nome' => 'Água',
                ),
                array(
                    'demonstrativo_categoria_id' => 3,
                    'nome' => 'Energia Elétricia',
                ),
                array(
                    'demonstrativo_categoria_id' => 3,
                    'nome' => 'Aluguel',
                ),
                array(
                    'demonstrativo_categoria_id' => 3,
                    'nome' => 'Complexo Mirassol',
                ),
                array(
                    'demonstrativo_categoria_id' => 3,
                    'nome' => 'Sede em Búzios',
                ),
                array(
                    'demonstrativo_categoria_id' => 3,
                    'nome' => 'Sede em Santa Cruz',
                ),
                array(
                    'demonstrativo_categoria_id' => 3,
                    'nome' => 'Posto de Atendimento da Ribeira',
                ),
                array(
                    'demonstrativo_categoria_id' => 3,
                    'nome' => 'Manutenção e Conservação',
                ),
                array(
                    'demonstrativo_categoria_id' => 3,
                    'nome' => 'Restaurante',
                ),
                array(
                    'demonstrativo_categoria_id' => 3,
                    'nome' => 'Despesas Bancárias',
                ),
                array(
                    'demonstrativo_categoria_id' => 4,
                    'nome' => 'Assistência à Saúde',
                ),
                array(
                    'demonstrativo_categoria_id' => 4,
                    'nome' => 'Assistência à Educação',
                ),
                array(
                    'demonstrativo_categoria_id' => 4,
                    'nome' => 'Assistência ao Esporte',
                ),
                array(
                    'demonstrativo_categoria_id' => 4,
                    'nome' => 'Serviços Prestados',
                ),
                array(
                    'demonstrativo_categoria_id' => 4,
                    'nome' => 'Folha de Pagamento',
                ),
                array(
                    'demonstrativo_categoria_id' => 4,
                    'nome' => 'Estagiários',
                ),
                array(
                    'demonstrativo_categoria_id' => 4,
                    'nome' => 'Encargos',
                ),
                array(
                    'demonstrativo_categoria_id' => 4,
                    'nome' => 'FGTS',
                ),
                array(
                    'demonstrativo_categoria_id' => 4,
                    'nome' => 'INSS',
                ),
                array(
                    'demonstrativo_categoria_id' => 4,
                    'nome' => 'Vale-transporte',
                ),
                array(
                    'demonstrativo_categoria_id' => 5,
                    'nome' => 'IPTU',
                ),
                array(
                    'demonstrativo_categoria_id' => 5,
                    'nome' => 'Taxa de Ocupação',
                ),
            );

            foreach ($demonstrativo_categoria as $id => $categoria)
            {
                $create['DemonstrativoCategoria'][] = array_merge(array('ordem' => $id, 'publicado' => true), $categoria);
            }

            foreach ($demonstrativo_fluxocaixa as $id => $fluxocaixa)
            {
                $create['DemonstrativoFluxoCaixa'][] = array_merge(array('ordem' => $id, 'publicado' => true), $fluxocaixa);
            }
        }

        // INSERIR SEDES AUTOMATICAMENTE
        foreach (Estrutura::$titulos as $key => $value)
        {
            $localizacao = array();

            if(array_key_exists($key, Estrutura::$localizacao))
            {
                $localizacao = Estrutura::$localizacao[$key];
            }

            $create['Estrutura'][] = array_merge(array(
                'nome' => $value,
                'texto' => Estrutura::$texto[$key],
            ), $localizacao);
        }

        // INSERIR SERVIÇOS AUTOMATICAMENTE
        foreach (Servico::$titulos as $key => $value)
        {
            $create['Servico'][] = array(
                'nome' => $value,
                'servico_estrutura_id' => Servico::$localidade[$key],
                'texto' => Servico::$texto[$key],
            );
        }

        foreach ($create as $key => $value)
        {
            $model = new $key($pdo);
            $definition = new Cdc_Definition($model->struct, 'create');
            $model->setDefinition($definition);
            $sql = $model->createQuery();

            foreach ($value as $index)
            {
                if(is_array($index))
                {
                    foreach ($index as $k => $v)
                    {
                        $sql->cols[$k] = $v;
                    }
                }
                else
                {
                    $sql->cols['nome'] = $index;
                }

                $model->execute($sql);
            }

        }
    }

    public static function update($pdo)
    {
        $update = array();

        // INSERIR CORPO DOS TEXTOS AUTOMATICAMENTE
        $textos = Texto::$titulos;
        foreach ($textos as $key => $value)
        {
            $update['Texto'][$key] = array(
                'nome' => $value,
                'slug' => Cdc_Texto::slug($value),
                'corpo' => Texto::$corpo[$key],
            );
        }

        foreach ($update as $key => $value)
        {
            $model = new $key($pdo);
            $definition = new Cdc_Definition($model->struct, 'update');
            $model->setDefinition($definition);
            $sql = $model->createQuery();

            foreach ($value as $index => $content)
            {
                if(is_array($content))
                {
                    foreach ($content as $k => $v)
                    {
                        $sql->cols[$k] = $v;
                    }

                }
                else
                {
                    $sql->cols['nome'] = $content;
                }

                $sql->where = array(
                    'id = ' => $index,
                );

                $model->execute($sql);
            }

        }
    }

}
