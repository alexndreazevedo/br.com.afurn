<?php

class Metadata_Routes
{

    public static function setup($router)
    {
        $router->map('', array('class' => 'Controller_Index', 'resource' => 'none'), array('name' => 'home'));

        // INSTITUCIONAL
        $router->map('institucional', array('class' => 'Base_Controller_Texto_Item', 'resource' => 'none'), array('name' => 'institucional'));
        $router->map('institucional/:slug', array('class' => 'Base_Controller_Texto_Item', 'resource' => 'none'), array('name' => 'institucional'));
        $router->map('demonstrativo', array('class' => 'Controller_Institucional_Demonstrativo', 'resource' => 'none'), array('name' => 'demonstrativo'));

        // AFURN
        $router->map('afurn', array('class' => 'Controller_Afurn', 'resource' => 'none'), array('name' => 'afurn'));
        $router->map('afurn/presidentes', array('class' => 'Controller_Afurn_Presidentes', 'resource' => 'none'), array('name' => 'presidentes'));
        $router->map('afurn/diretoria-equipe', array('class' => 'Controller_Afurn_DiretoriaEquipe', 'resource' => 'none'), array('name' => 'diretoria_equipe'));
        $router->map('afurn/assessoria', array('class' => 'Controller_Afurn_Assessoria', 'resource' => 'none'), array('name' => 'assessoria'));
        $router->map('afurn/parceiros', array('class' => 'Controller_Afurn_Parceiros', 'resource' => 'none'), array('name' => 'parceiros'));
        $router->map('afurn/convenios', array('class' => 'Controller_Afurn_Convenios', 'resource' => 'none'), array('name' => 'convenios'));

        // NOTICIA
        $router->map('noticias', array('class' => 'Controller_Noticia_Index', 'resource' => 'none'), array('name' => 'noticia'));
        $router->map('noticias/:categoria', array('class' => 'Controller_Noticia_Categoria', 'resource' => 'none'), array('name' => 'noticia-categoria'));
        $router->map('noticias/:categoria/:slug', array('class' => 'Controller_Noticia_Item', 'resource' => 'none'), array('name' => 'noticia-item'));

        // ESTRUTURA
        $router->map('estrutura', array('class' => 'Controller_Estrutura_Item', 'resource' => 'none'), array('name' => 'estrutura'));
        $router->map('estrutura/:slug', array('class' => 'Controller_Estrutura_Item', 'resource' => 'none'), array('name' => 'estrutura-item'));

        // SERVIÇO
        $router->map('servicos', array('class' => 'Controller_Servico_Item', 'resource' => 'none'), array('name' => 'servico'));
        $router->map('servicos/:slug', array('class' => 'Controller_Servico_Item', 'resource' => 'none'), array('name' => 'servico-item'));

        // ACONTECE
        $router->map('acontece', array('class' => 'Controller_Acontece_Index', 'resource' => 'none'), array('name' => 'acontece'));
        $router->map('acontece/galeria', array('class' => 'Controller_Acontece_Galeria_Index', 'resource' => 'none'), array('name' => 'acontece-galeria'));
        $router->map('acontece/galeria/:id', array('class' => 'Controller_Acontece_Galeria_Item', 'resource' => 'none'), array('name' => 'acontece-galeria-item'));
        $router->map('acontece/eventos', array('class' => 'Controller_Acontece_Eventos_Index', 'resource' => 'none'), array('name' => 'acontece-eventos'));
        $router->map('acontece/eventos/:slug', array('class' => 'Controller_Acontece_Eventos_Item', 'resource' => 'none'), array('name' => 'acontece-eventos-item'));

        // ASSOCIE-SE
        $router->map('associe-se', array('class' => 'Contato_Controller_Index', 'resource' => 'none', 'form' => 'Form_Associe'), array('name' => 'associe'));

        // FALE CONOSCO
        $router->map('faleconosco', array('class' => 'Contato_Controller_Index', 'resource' => 'none', 'form' => 'Form_Faleconosco'), array('name' => 'faleconosco'));
        //$router->map(':slug', array('class' => 'Base_Controller_Texto_Item', 'resource' => 'none'), array('name' => 'texto'));
     }

}
