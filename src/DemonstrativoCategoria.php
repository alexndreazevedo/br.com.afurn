<?php

class DemonstrativoCategoria extends Datastore
{
    const TIPO_ACUMULADO = 1;
    const TIPO_ENTRADA = 2;
    const TIPO_SAIDA = 3;

    public static $tipo = array(
        self::TIPO_ACUMULADO => 'Acumulado',
        self::TIPO_ENTRADA => 'Entradas',
        self::TIPO_SAIDA => 'Saídas',
    );

    public static $classes = array(
        self::TIPO_ACUMULADO => 'info',
        self::TIPO_ENTRADA => 'success',
        self::TIPO_SAIDA => 'error',
    );

    public static $replace = '';

    public static function replace($add = null)
    {
        if(empty(self::$replace))
        {
            self::$replace .= 'CASE ';
            foreach(self::$tipo as $key => $value)
            {
                self::$replace .= 'WHEN c.demonstrativo_tipo_id = ' . $key . ' THEN \'' . $value . '\'';
            }
            self::$replace .= ' END || \' - \' || ';
        }

        return self::$replace . $add;
    }

    public function init()
    {

        $this->struct = array(
            'demonstrativo_categoria' => array(
                'type' => Cdc_Definition::TYPE_RELATION,
                'statement_type' => Cdc_Definition::STATEMENT_SELECT,
                Cdc_Definition::OPERATION => array(
                    'read' => array(),
                    'item' => array(),
                    'create' => array(
                        'statement_type' => Cdc_Definition::STATEMENT_INSERT,
                    ),
                    'update' => array(
                        'statement_type' => Cdc_Definition::STATEMENT_UPDATE,
                    ),
                    'delete' => array(
                        'statement_type' => Cdc_Definition::STATEMENT_DELETE,
                    ),
                ),
            ),
            'id' => self::primaryColumn(),
            'nome' => self::setRequired(array_merge(array('search' => array('operator' => 'like')), self::textColumn())),
            'demonstrativo_tipo_id' => array(
                'type' => Cdc_Definition::TYPE_COLUMN,
                'search' => array(
                    'operator' => '=',
                ),
                Cdc_Definition::TYPE_WIDGET => array(
                    'widget' => 'select',
                    'options' => self::$tipo,
                    'attributes' => array(
                        'required' => 'required',
                    ),
                ),
                Cdc_Definition::OPERATION => array(
                    'read' => array(
                        Cdc_Definition::FORMATTER => array(array('Cdc_CellDataFormatter', 'arrayValue'), array(self::$tipo)),
                    ),
                    'item' => array(),
                    'create' => array(),
                    'update' => array(),
                ),
                Cdc_Definition::TYPE_RULE => array(
                    array('Cdc_Rule_Required'),
                    array('Cdc_Rule_Trim'),
                    array('Cdc_Rule_ArrayKeyExists'),
                ),
            ),
            'detalhes' => self::textareaColumn(),
            'ordem' => self::orderColumn(),
            'publicado' => self::publishedColumn(),
        );
    }

    public function getHtmlOptions($row, $rowset, $args)
    {
        if (!$this->allow($row))
        {
            return '';
        }

        $return = '';
        $link_args = array(
            'r' => $args['controller']->relation
        );
        $link_http_params = array(
            'op' => 'update',
            $args['controller']->primary => $row[$args['controller']->primary],
        );
        $return .= '<a class="btn" rel="tooltip" title="Editar" href="' . $args['controller']->link('admin', $link_args, $link_http_params) . '"><i class="fa fa-pencil"></i></a>';

        return '<div class="btn-group">' . $return . '</div>';
    }
}