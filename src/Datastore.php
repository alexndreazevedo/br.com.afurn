<?php

class Datastore extends Cdc_Datastore
{

    public $id;
    public $input;

    public static function relation()
    {
        return array(
            'type'                    => Cdc_Definition::TYPE_RELATION,
            'statement_type'          => Cdc_Definition::STATEMENT_SELECT,
            Cdc_Definition::OPERATION => array(
                'read' => array(),
                'item' => array(),
                'create' => array(
                    'statement_type' => Cdc_Definition::STATEMENT_INSERT,
                ),
                'update'         => array(
                    'statement_type' => Cdc_Definition::STATEMENT_UPDATE,
                ),
                'delete'         => array(
                    'statement_type' => Cdc_Definition::STATEMENT_DELETE,
                ),
            ),
        );
    }

    public static function prefixKeys($def, $prefix, $index, $data = false)
    {
        $result = array();
        if ($data)
        {
            foreach ($def as $key => $value)
            {
                $result["{$prefix}[{$index}][{$key}]"] = $value;
            }
        }
        else
        {
            foreach ($def as $key => $value)
            {
                if ($value['type'] == Cdc_Definition::TYPE_COLUMN)
                {
                    $result["{$prefix}[{$index}][{$key}]"] = $value;
                }

            }
        }

        return $result;
    }

    public static function primaryColumn($operations = null)
    {
        $operations or $operations = array(
            'read' => array(),
            'item' => array(),
            'create' => array(),
            'update' => array(),
            'delete' => array(),
        );

        return array(
            'type'                    => Cdc_Definition::TYPE_COLUMN,
            'primary'                 => true,
            'hide'                    => true,
            Cdc_Definition::OPERATION => $operations,
        );
    }

    public static function primaryColumnHidden($operations = null)
    {
        $operations or $operations = array(
            'read' => array(),
            'item' => array(),
            'create' => array(),
            'update' => array(),
            'delete' => array(),
        );

        return array(
            'type'                    => Cdc_Definition::TYPE_COLUMN,
            'primary'                 => true,
            Cdc_Definition::OPERATION => $operations,
            Cdc_Definition::TYPE_WIDGET => array(
                'widget'     => 'hidden',
                'attributes' => array(
                    'class' => 'id',
                ),
            ),
            Cdc_Definition::TYPE_RULE => array(
                array('Cdc_Rule_Trim'),
            ),
        );
    }

    public static function addAddressColumns(&$def)
    {
        $result = array(
            Cdc_Client_Cep::RESULT_CEP      => self::setRequired(self::textColumn(10)),
            Cdc_Client_Cep::RESULT_ENDERECO => self::setRequired(self::textColumn()),
            'numero'                        => self::textColumn(55),
            'complemento'                   => self::textColumn(),
            Cdc_Client_Cep::RESULT_BAIRRO   => self::setRequired(self::textColumn()),
            Cdc_Client_Cep::RESULT_CIDADE   => self::setRequired(self::textColumn()),
            Cdc_Client_Cep::RESULT_UF       => self::setRequired(self::simpleSelectColumn(self::$estados, 'RN'), false),
        );

        $def = array_merge($def, $result);
    }

    public static function setRequired($column, $htmlAttribute = true)
    {
        if ($htmlAttribute)
        {
            $column[Cdc_Definition::TYPE_WIDGET]['attributes']['required'] = 'required';
        }

        $column[Cdc_Definition::TYPE_RULE][] = array('Cdc_Rule_Required');

        return $column;
    }

    public static function passwordColumn($confirmation = 'confirma_senha', $operations = null)
    {
        $operations or $operations = array(
            'create' => array(),
            'change_password' => array(),
        );

        $result = array(
            'type'                      => Cdc_Definition::TYPE_COLUMN,
            Cdc_Definition::TYPE_WIDGET => array(
                'widget'     => 'password',
                'attributes' => array(
                    'maxlength'               => 32,
                ),
            ),
            Cdc_Definition::OPERATION => $operations,
            Cdc_Definition::TYPE_RULE => array(
                array('Cdc_Rule_Length', array(0, 32)),
            ),
        );

        if ($confirmation)
        {
            $result[Cdc_Definition::TYPE_RULE][] = array('Cdc_Rule_CompareFields', array($confirmation, label($confirmation)));
        }
        else
        {
            // Caso não haja confirmação significa que já é o campo de confirmação
            // que está sendo definido. Este atributo sinaliza que esta coluna deve
            // ser excluída na hora de construir o sql.
            $result[Cdc_Definition::OPERATION]['create']['virtual'] = true;
        }

        return $result;
    }

    public static function arquivoColumn()
    {
        return array(
            'type'                          => Cdc_Definition::TYPE_ATTACHMENT,
            Cdc_Definition::TYPE_ATTACHMENT => 'arquivo',
            'data_format'                   => 'json',
            Cdc_Definition::TYPE_WIDGET     => array(
                'widget'     => 'filemanager',
                'input_keys' => array('arquivo_id', 'regiao', 'nome'),
            ),
            Cdc_Definition::OPERATION => array(
                'item' => array(),
                'create' => array(),
                'update' => array(),
            ),
        );
    }

    public static function arquivoAttachment($table)
    {
        return array(
            'query_params' => array(
                'cols' => array('id', 'nome', 'regiao', 'arquivo_id', $table . '_id', 'titulo', 'descricao', 'creditos'),
                'from' => array('arquivo'),
                'join' => array($table . '_arquivo' => array('left' => array('arquivo.id = ' . $table . '_arquivo.arquivo_id'))),
                'order' => array('id'    => 'asc'),
                'where' => array($table . '_arquivo.' . $table . '_id in #id#'),
            ),
            'exec_params' => array(
                'relation_table'             => $table . '_arquivo',
                'relation_column_attachment' => 'arquivo_id',
                'attachment_table'           => 'arquivo',
                'extra_columns'              => array(
                    'regiao',
                ),
            ),
            'index_key'        => 'id', // índice para este attachment
            'parent_key'       => 'id',
            'attachment_key'   => $table . '_id',
            'distribution_key' => 'regiao',
        );
    }

    public static function dateColumn($operations = null)
    {
        $operations or $operations = array(
            'read' => array(
                Cdc_Definition::FORMATTER => array(
                    array('Cdc_CellDataFormatter', 'formatDate'), array('d-m-Y'),
                ),
            ),
            'item' => array(),
            // 'create' => array(),
            'update' => array(),
        );

        return array(
            'type'                      => Cdc_Definition::TYPE_COLUMN,
            Cdc_Definition::TYPE_WIDGET => array(
                'widget'          => 'date',
                'output_callback' => array(array('Cdc_OutputFormatter', 'date'), array('d/m/Y')),
                'attributes' => array(
                     'class' => 'span3',
                ),
            ),
            Cdc_Definition::OPERATION => $operations,
            Cdc_Definition::TYPE_RULE => array(
                array('Cdc_Rule_Trim'),
                // array('Cdc_Rule_Date', array('Y-m-d')),
            ),
        );
    }

    public static function textareaColumn($classes_str = null, $cols = 100, $rows = 10, $operations = null)
    {
        $operations or $operations = array(
            'item' => array(),
            'create' => array(),
            'update' => array(),
        );

        $result = array(
            'type'                      => Cdc_Definition::TYPE_COLUMN,
            Cdc_Definition::TYPE_WIDGET => array(
                'widget'     => 'textarea',
                'attributes' => array(
                    'cols'                    => $cols,
                    'rows'                    => $rows,
                ),
            ),
            Cdc_Definition::OPERATION => $operations,
            Cdc_Definition::TYPE_RULE => array(
                array('Cdc_Rule_Trim'),
            ),
        );

        if ($classes_str)
        {
            $result[Cdc_Definition::TYPE_WIDGET]['attributes']['class'] = $classes_str;
        }

        return $result;
    }

    public static function richColumn($operations = null)
    {
        $operations or $operations = array(
            'item' => array(),
            'create' => array(),
            'update' => array(),
        );

        $result = array(
            'type'                      => Cdc_Definition::TYPE_COLUMN,
            Cdc_Definition::TYPE_WIDGET => array(
                'widget'                  => 'rich',
            ),
            Cdc_Definition::OPERATION => $operations,
            Cdc_Definition::TYPE_RULE => array(
                array('Cdc_Rule_Trim'),
            ),
        );

        return $result;
    }

    public static function slugColumn($operations = null)
    {
        $operations or $operations = array(
            'item' => array(),
        );

        $result = array(
            'type'                      => Cdc_Definition::TYPE_COLUMN,
            Cdc_Definition::OPERATION => $operations,
        );

        return $result;
    }


    public static function textColumn($classes_str = null, $rules = null, $maxLength = 255, $minLength = 0, $operations = null)
    {
        $operations or $operations = array(
            'read' => array(),
            'item' => array(),
            'create' => array(),
            'update' => array(),
        );

        $result = array(
            'type'                      => Cdc_Definition::TYPE_COLUMN,
            Cdc_Definition::TYPE_WIDGET => array(
                'attributes' => array(
                    'maxlength' => $maxLength,
                ),
            ),
            Cdc_Definition::OPERATION => $operations,
            Cdc_Definition::TYPE_RULE => array(
                array('Cdc_Rule_Trim'),
                array('Cdc_Rule_Length', array($minLength, $maxLength)),
            ),
        );

        if ($classes_str)
        {
            $result[Cdc_Definition::TYPE_WIDGET]['attributes']['class'] = $classes_str;
        }

        if ($rules)
        {
            $result[Cdc_Definition::TYPE_RULE] = array_merge($result[Cdc_Definition::TYPE_RULE], $rules);
        }

        return $result;
    }

    public static function orderColumn()
    {
        return array(
            'type' => Cdc_Definition::TYPE_COLUMN,
            Cdc_Definition::TYPE_WIDGET => array(),
            Cdc_Definition::OPERATION => array(
                'read' => array(),
                'item' => array(),
                'create' => array(),
                'update' => array(),
            ),
            Cdc_Definition::TYPE_RULE => array(
                array('Cdc_Rule_Trim'),
                array('Cdc_Rule_Numeric'),
                array('Cdc_Rule_Length', array(0, 3)),
            ),
        );
    }

    public static function publishedColumn()
    {
        return array(
            'type' => Cdc_Definition::TYPE_COLUMN,
			'search' => array(
				'operator' => '=',
			),
            Cdc_Definition::TYPE_WIDGET => array(
                'widget' => 'boolean',
                'default' => true,
                'attributes' => array(
                    'value' => true,
                ),
            ),
            Cdc_Definition::OPERATION => array(
                'read' => array(
                    Cdc_Definition::FORMATTER => array(array('Cdc_CellDataFormatter', 'boolean')),
                ),
                'item' => array(),
                'create' => array(),
                'update' => array(),
            ),
        );
    }

    public static function simpleSelectColumn($values = array(), $default = null, $operations = null)
    {
        $operations or $operations = array(
            'item' => array(),
            'create' => array(),
            'update' => array(),
        );

        $result = array(
            'type'                      => Cdc_Definition::TYPE_COLUMN,
            Cdc_Definition::TYPE_WIDGET => array(
                'widget'                  => 'select',
            ),
            Cdc_Definition::OPERATION => $operations,
            Cdc_Definition::TYPE_RULE => array(
                array('Cdc_Rule_Trim'),
                array('Cdc_Rule_ArrayKeyExists'),
            ),
        );

        if (is_callable($values))
        {
            $result[Cdc_Definition::TYPE_WIDGET]['callback'] = $values;
        }
        else
        {
            $result[Cdc_Definition::TYPE_WIDGET]['options'] = $values;
        }

        return $result;
    }

    public function emailColumn()
    {
        return array(
            'type'                      => Cdc_Definition::TYPE_COLUMN,
            Cdc_Definition::TYPE_WIDGET => array(
                'widget'     => 'email',
                'attributes' => array(
                    'maxlength'               => 255,
                ),
            ),
            Cdc_Definition::OPERATION => array(
                //'read' => array(),
                'item' => array(),
                'create' => array(),
                'update' => array(),
            ),
            Cdc_Definition::TYPE_RULE => array(
                array('Cdc_Rule_Trim'),
                array('Cdc_Rule_Email'),
            ),
        );
    }

    public static $estados = array(
        'AC'             => 'Acre',
        'AL'             => 'Alagoas',
        'AP'             => 'Amapá',
        'AM'             => 'Amazonas',
        'BA'             => 'Bahia',
        'CE'             => 'Ceará',
        'DF'             => 'Distrito Federal',
        'ES'             => 'Espírito Santo',
        'GO'             => 'Goiás',
        'MA'             => 'Maranhão',
        'MG'             => 'Minas Gerais',
        'MT'             => 'Mato Grosso',
        'MS'             => 'Mato Grosso do Sul',
        'PA'             => 'Pará',
        'PB'             => 'Paraíba',
        'PR'             => 'Paraná',
        'PE'             => 'Pernambuco',
        'PI'             => 'Piauí',
        'RJ'             => 'Rio de Janeiro',
        'RN'             => 'Rio Grande do Norte',
        'RS'             => 'Rio Grande do Sul',
        'RO'             => 'Rondônia',
        'RR'             => 'Roraima',
        'SC'             => 'Santa Catarina',
        'SP'             => 'São Paulo',
        'SE'             => 'Sergipe',
        'TO'             => 'Tocantins',
    );


    public static $paises = array(
        1 => 'Afeganistão',
        2 => 'África do Sul',
        3 => 'Akrotiri',
        4 => 'Albânia',
        5 => 'Alemanha',
        6 => 'Andorra',
        7 => 'Angola',
        8 => 'Anguila',
        9 => 'Antárctida',
        10 => 'Antígua e Barbuda',
        11 => 'Antilhas Neerlandesas',
        12 => 'Arábia Saudita',
        13 => 'Arctic Ocean',
        14 => 'Argélia',
        15 => 'Argentina',
        16 => 'Arménia',
        17 => 'Aruba',
        18 => 'Ashmore and Cartier Islands',
        19 => 'Atlantic Ocean',
        20 => 'Austrália',
        21 => 'Áustria',
        22 => 'Azerbaijão',
        23 => 'Baamas',
        24 => 'Bangladeche',
        25 => 'Barbados',
        26 => 'Barém',
        27 => 'Bélgica',
        28 => 'Belize',
        29 => 'Benim',
        30 => 'Bermudas',
        31 => 'Bielorrússia',
        32 => 'Birmânia',
        33 => 'Bolívia',
        34 => 'Bósnia e Herzegovina',
        35 => 'Botsuana',
        36 => 'Brasil',
        37 => 'Brunei',
        38 => 'Bulgária',
        39 => 'Burquina Faso',
        40 => 'Burúndi',
        41 => 'Butão',
        42 => 'Cabo Verde',
        43 => 'Camarões',
        44 => 'Camboja',
        45 => 'Canadá',
        46 => 'Catar',
        47 => 'Cazaquistão',
        48 => 'Chade',
        49 => 'Chile',
        50 => 'China',
        51 => 'Chipre',
        52 => 'Clipperton Island',
        53 => 'Colômbia',
        54 => 'Comores',
        55 => 'Congo-Brazzaville',
        56 => 'Congo-Kinshasa',
        57 => 'Coral Sea Islands',
        58 => 'Coreia do Norte',
        59 => 'Coreia do Sul',
        60 => 'Costa do Marfim',
        61 => 'Costa Rica',
        62 => 'Croácia',
        63 => 'Cuba',
        64 => 'Dhekelia',
        65 => 'Dinamarca',
        66 => 'Domínica',
        67 => 'Egito',
        68 => 'Emirados Árabes Unidos',
        69 => 'Equador',
        70 => 'Eritreia',
        71 => 'Eslováquia',
        72 => 'Eslovénia',
        73 => 'Espanha',
        74 => 'Estados Unidos',
        75 => 'Estónia',
        76 => 'Etiópia',
        77 => 'Faroé',
        78 => 'Fiji',
        79 => 'Filipinas',
        80 => 'Finlândia',
        81 => 'França',
        82 => 'Gabão',
        83 => 'Gâmbia',
        84 => 'Gana',
        85 => 'Gaza Strip',
        86 => 'Geórgia',
        87 => 'Geórgia do Sul e Sandwich do Sul',
        88 => 'Gibraltar',
        89 => 'Granada',
        90 => 'Grécia',
        91 => 'Gronelândia',
        92 => 'Guame',
        93 => 'Guatemala',
        94 => 'Guernsey',
        95 => 'Guiana',
        96 => 'Guiné',
        97 => 'Guiné Equatorial',
        98 => 'Guiné-Bissau',
        99 => 'Haiti',
        100 => 'Honduras',
        101 => 'Hong Kong',
        102 => 'Hungria',
        103 => 'Iémen',
        104 => 'Ilha Bouvet',
        105 => 'Ilha do Natal',
        106 => 'Ilha Norfolk',
        107 => 'Ilhas Caimão',
        108 => 'Ilhas Cook',
        109 => 'Ilhas dos Cocos',
        110 => 'Ilhas Falkland',
        111 => 'Ilhas Heard e McDonald',
        112 => 'Ilhas Marshall',
        113 => 'Ilhas Salomão',
        114 => 'Ilhas Turcas e Caicos',
        115 => 'Ilhas Virgens Americanas',
        116 => 'Ilhas Virgens Britânicas',
        117 => 'Índia',
        118 => 'Indian Ocean',
        119 => 'Indonésia',
        120 => 'Irão',
        121 => 'Iraque',
        122 => 'Irlanda',
        123 => 'Islândia',
        124 => 'Israel',
        125 => 'Itália',
        126 => 'Jamaica',
        127 => 'Jan Mayen',
        128 => 'Japão',
        129 => 'Jersey',
        130 => 'Jibuti',
        131 => 'Jordânia',
        132 => 'Kuwait',
        133 => 'Laos',
        134 => 'Lesoto',
        135 => 'Letónia',
        136 => 'Líbano',
        137 => 'Libéria',
        138 => 'Líbia',
        139 => 'Listenstaine',
        140 => 'Lituânia',
        141 => 'Luxemburgo',
        142 => 'Macau',
        143 => 'Macedónia',
        144 => 'Madagáscar',
        145 => 'Malásia',
        146 => 'Malávi',
        147 => 'Maldivas',
        148 => 'Mali',
        149 => 'Malta',
        150 => 'Man, Isle of',
        151 => 'Marianas do Norte',
        152 => 'Marrocos',
        153 => 'Maurícia',
        154 => 'Mauritânia',
        155 => 'Mayotte',
        156 => 'México',
        157 => 'Micronésia',
        158 => 'Moçambique',
        159 => 'Moldávia',
        160 => 'Mónaco',
        161 => 'Mongólia',
        162 => 'Monserrate',
        163 => 'Montenegro',
        164 => 'Mundo',
        165 => 'Namíbia',
        166 => 'Nauru',
        167 => 'Navassa Island',
        168 => 'Nepal',
        169 => 'Nicarágua',
        170 => 'Níger',
        171 => 'Nigéria',
        172 => 'Niue',
        173 => 'Noruega',
        174 => 'Nova Caledónia',
        175 => 'Nova Zelândia',
        176 => 'Omã',
        177 => 'Pacific Ocean',
        178 => 'Países Baixos',
        179 => 'Palau',
        180 => 'Panamá',
        181 => 'Papua-Nova Guiné',
        182 => 'Paquistão',
        183 => 'Paracel Islands',
        184 => 'Paraguai',
        185 => 'Peru',
        186 => 'Pitcairn',
        187 => 'Polinésia Francesa',
        188 => 'Polónia',
        189 => 'Porto Rico',
        190 => 'Portugal',
        191 => 'Quénia',
        192 => 'Quirguizistão',
        193 => 'Quiribáti',
        194 => 'Reino Unido',
        195 => 'República Centro-Africana',
        196 => 'República Checa',
        197 => 'República Dominicana',
        198 => 'Roménia',
        199 => 'Ruanda',
        200 => 'Rússia',
        201 => 'Salvador',
        202 => 'Samoa',
        203 => 'Samoa Americana',
        204 => 'Santa Helena',
        205 => 'Santa Lúcia',
        206 => 'São Cristóvão e Neves',
        207 => 'São Marinho',
        208 => 'São Pedro e Miquelon',
        209 => 'São Tomé e Príncipe',
        210 => 'São Vicente e Granadinas',
        211 => 'Sara Ocidental',
        212 => 'Seicheles',
        213 => 'Senegal',
        214 => 'Serra Leoa',
        215 => 'Sérvia',
        216 => 'Singapura',
        217 => 'Síria',
        218 => 'Somália',
        219 => 'Southern Ocean',
        220 => 'Spratly Islands',
        221 => 'Sri Lanca',
        222 => 'Suazilândia',
        223 => 'Sudão',
        224 => 'Suécia',
        225 => 'Suíça',
        226 => 'Suriname',
        227 => 'Svalbard e Jan Mayen',
        228 => 'Tailândia',
        229 => 'Taiwan',
        230 => 'Tajiquistão',
        231 => 'Tanzânia',
        232 => 'Território Britânico do Oceano Índico',
        233 => 'Territórios Austrais Franceses',
        234 => 'Timor Leste',
        235 => 'Togo',
        236 => 'Tokelau',
        237 => 'Tonga',
        238 => 'Trindade e Tobago',
        239 => 'Tunísia',
        240 => 'Turquemenistão',
        241 => 'Turquia',
        242 => 'Tuvalu',
        243 => 'Ucrânia',
        244 => 'Uganda',
        245 => 'União Europeia',
        246 => 'Uruguai',
        247 => 'Usbequistão',
        248 => 'Vanuatu',
        249 => 'Vaticano',
        250 => 'Venezuela',
        251 => 'Vietname',
        252 => 'Wake Island',
        253 => 'Wallis e Futuna',
        254 => 'West Bank',
        255 => 'Zâmbia',
        256 => 'Zimbabué',
    );

    public static $meses = array(
		1 => 'Janeiro',
		2 => 'Fevereiro',
		3 => 'Março',
		4 => 'Abril',
		5 => 'Maio',
		6 => 'Junho',
		7 => 'Julho',
		8 => 'Agosto',
		9 => 'Setembro',
		10 => 'Outubro',
		11 => 'Novembro',
		12 => 'Dezembro',
    );
    private static $_formatterCache = array();

    public static function formatter($row, $rowset, $index, $args)
    {
        $pdo   = $args[0];
        $table = $args[1];
        if (isset($args[2]))
        {
            $titleCol = $args[2];
        }
        else
        {
            $titleCol = 'nome';
        }
        if (!array_key_exists($index, self::$_formatterCache))
        {
            $sql       = new Cdc_Sql_Select($pdo);
            $sql->cols = array('id', $titleCol);
            $sql->where = array('id in'    => Cdc_ArrayHelper::pluck($rowset, $index));
            $sql->from = array($table);

            self::$_formatterCache[$index] = $sql->stmt()->fetchAll(PDO::FETCH_KEY_PAIR);
        }

        if ($row[$index])
        {
            $nome = self::$_formatterCache[$index][$row[$index]];
        }
        else
        {
            $nome = '';
        }

        return '<td class="' . $index . '">' . $nome . '</td>';
    }

}