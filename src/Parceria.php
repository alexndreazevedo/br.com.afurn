<?php

class Parceria extends Datastore
{
    const TIPO_PARCEIRO = 1;
    const TIPO_CONVENIO = 2;

    public static $tipo = array(
        self::TIPO_PARCEIRO => 'Parceiro',
        self::TIPO_CONVENIO => 'Convênio',
    );

    public function init()
    {
        $this->struct = array(
            'parceria' => array(
                'type' => Cdc_Definition::TYPE_RELATION,
                'statement_type' => Cdc_Definition::STATEMENT_SELECT,
                Cdc_Definition::OPERATION => array(
                    'read' => array(),
                    'item' => array(),
                    'create' => array(
                        'statement_type' => Cdc_Definition::STATEMENT_INSERT,
                    ),
                    'update' => array(
                        'statement_type' => Cdc_Definition::STATEMENT_UPDATE,
                    ),
                    'delete' => array(
                        'statement_type' => Cdc_Definition::STATEMENT_DELETE,
                    ),
                ),
                Cdc_Definition::TYPE_ATTACHMENT => array(
                    'arquivo' => self::arquivoAttachment('parceria'),
                ),
            ),
            'id' => self::primaryColumn(),
            'imagem' => self::arquivoColumn(),
            'nome' => self::setRequired(array_merge(array('search' => array('operator' => 'like')), self::textColumn())),
            'parceria_tipo_id' => array(
                'type' => Cdc_Definition::TYPE_COLUMN,
                'search' => array(
                    'operator' => '=',
                ),
                Cdc_Definition::TYPE_WIDGET => array(
                    'widget' => 'select',
                    'options' => self::$tipo,
                    'attributes' => array(
                        'required' => 'required',
                    ),
                ),
                Cdc_Definition::OPERATION => array(
                    'read' => array(
                        Cdc_Definition::FORMATTER => array(array('Cdc_CellDataFormatter', 'arrayValue'), array(self::$tipo)),
                    ),
                    'item' => array(),
                    'create' => array(),
                    'update' => array(),
                ),
                Cdc_Definition::TYPE_RULE => array(
                    array('Cdc_Rule_Required'),
                    array('Cdc_Rule_Trim'),
                    array('Cdc_Rule_ArrayKeyExists'),
                ),
            ),
            'endereco' => self::setRequired(self::textColumn(null, null, 255, 0, array(
                'item' => array(),
                'create' => array(),
                'update' => array(),
            ))),
            'telefone' => self::setRequired(self::textColumn(null, null, 255, 0, array(
                'item' => array(),
                'create' => array(),
                'update' => array(),
            ))),
            'email' => self::setRequired(self::emailColumn()),
            'descricao' => self::textareaColumn(),
            'ordem' => self::orderColumn(),
            'publicado' => self::publishedColumn(),
        );
    }
}