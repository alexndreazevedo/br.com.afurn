<?php

class NoticiaCategoria extends Datastore
{
    public $categorias = null;

    public function init()
    {
        $this->struct = array(
            'noticia_categoria' => array(
                'type' => Cdc_Definition::TYPE_RELATION,
                'statement_type' => Cdc_Definition::STATEMENT_SELECT,
                Cdc_Definition::OPERATION => array(
                    'read' => array(),
                    'item' => array(),
                    'create' => array(
                        'statement_type' => Cdc_Definition::STATEMENT_INSERT,
                    ),
                    'update'         => array(
                        'statement_type' => Cdc_Definition::STATEMENT_UPDATE,
                    ),
                    'delete'         => array(
                        'statement_type' => Cdc_Definition::STATEMENT_DELETE,
                    ),
                ),
            ),
            'id' => self::primaryColumn(),
            'slug' => self::slugColumn(),
            'nome' => self::setRequired(self::textColumn()),
        );
    }

    public function slugs()
    {
        if (null === $this->categorias)
        {
            $sql = new Cdc_Sql_Select($this->getPdo());
            $sql->cols = array('id', 'slug', 'nome');
            $sql->from = array('noticia_categoria');
            $this->categorias = Cdc_ArrayHelper::keyPair($sql->stmt()->fetchAll());
        }
        return $this->categorias;
    }

    public function execute($sql)
    {
        if ($sql->getOperation() == 'create' || $sql->getOperation() == 'update')
        {
            $sql->cols['slug'] = Cdc_Texto::slug($sql->cols['nome']);
        }

        return parent::execute($sql);
    }
}