<?php
class Controller_Busca extends Controller
{
    public function content()
    {
        $this->pageTitle = 'Busca de Imóveis';
        $this->index = 'busca';
        $this->title = 'Busca de Imóveis';

        $model = $this->getModel('Imovel', 'result');
        $page = f(INPUT_GET, 'p');
        $limit = 20;

        $sql = $model->createQuery(array('limit' => array('limit' => $limit, 'page' => $page), 'order' => array('imovel.destaque' => 'desc', 'imovel.id' => 'desc')));

        self::applyFilters($sql, $_GET);

        foreach ($sql->cols as $key => $value) {
            $sql->cols[$key] = current($sql->from) . '.' . $value;
        }

        $joins = array('tipo', 'bairro', 'area', 'quarto', 'faixa');

        foreach($joins as $join)
        {
            $sql->join[$join] = array('inner' => array(current($sql->from) . '.' . $join . '_id = ' . $join . '.id'));
            $sql->cols[] = $join . '.nome as ' . $join;
        }

        $result = $model->hydrateResultOf($sql);

        $total = key($result);
        $pager = Cdc_Pager::renderSimple($_SERVER['REQUEST_URI'], $total, $page, $limit);

        $lista = Cdc_ArrayHelper::current($result);

        $share = array(
            'title' => $this->pageTitle . ' - ' . Config::$baseTitle,
            'url' => Config::$base_url . substr($this->link('busca'), 1),
        );

        $this->share = array_merge($this->share, $share);

        ob_start();
        include $this->getTemplate('busca/index.phtml');
        return ob_get_clean();
    }

}
