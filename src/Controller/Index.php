<?php

use Facebook\FacebookSession;
use Facebook\FacebookRequest;

class Controller_Index extends Controller
{

    public $isFront = true;
    private $showcase = 0;

    public function content()
    {
        $vitrineModel = $this->getModel('Vitrine', 'item');
        $vitrine = current($vitrineModel->hydrateResultOf($vitrineModel->createQuery(array(
            'where' => array('publicado =' => true),
            'order' => array('ordem asc')
        ))));

        $albuns = array();
        $events = array();

        FacebookSession::setDefaultApplication($this->appId, $this->appSecret);

        $session = FacebookSession::newAppSession();
        
        try
        {
            $session->validate();

            $query = (new FacebookRequest($session, 'GET', '/' . $this->facebookPage . '?fields=events{id,name,location,start_time,timezone,is_date_only},albums.limit(8)&locale=pt_BR'))->execute()->getGraphObject()->asArray();

            if(key_exists('albums', $query))
            {
                foreach($query['albums']->data as $album)
                {
                    if($album->type == 'normal')
                    {
                        $cover = (new FacebookRequest($session, 'GET', '/' . $album->cover_photo . '?fields=id,name,source&locale=pt_BR'))->execute()->getGraphObject()->asArray();

                        $albuns[] = array(
                            'id' => $album->id,
                            'name' => $album->name,
                            'cover_photo' => $cover,
                        );
                    }
                }
            }

            if(key_exists('events', $query))
            {
                foreach($query['events']->data as $event)
                {
                    $events[] = $event;
                }
            }
        }
        catch (FacebookRequestException $ex)
        {
            echo '<div class="alert alert-error fade in">' . $ex->getMessage() . '</div>';
        }
        catch (\Exception $ex)
        {
            echo '<div class="alert alert-error fade in">' . $ex->getMessage() . '</div>';
        }

        ob_start();
        include $this->getTemplate('home.phtml');
        return ob_get_clean();
    }

    public function alternar()
    {
        $valor = $this->showcase++;

        if($valor % 2 == 0)
        {
            return 'gray';
        }

        return 'page';
    }
}
