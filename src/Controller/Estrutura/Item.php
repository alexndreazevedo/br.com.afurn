<?php
class Controller_Estrutura_Item extends Controller
{
    public $pageTitle = 'Estrutura';
    public $icone = 'thumb-tack';

    public function content()
    {
        $m = $this->getModel('Estrutura', 'item');
        $slug = f($this->urlParams, 'slug');

        $this->index = $slug;

        if (!$slug)
        {
            $default = current(Cdc_ArrayHelper::current($m->hydrateResultOf($m->createQuery(array('where' => array('id =' => key(Estrutura::$titulos)))))));

            header('Location: ' . $this->link('estrutura-item', array('slug' => $default['slug'])));
            die;
        }
        else
        {
            $result = $m->hydrateResultOf($m->createQuery(array('where' => array('slug =' => $slug))));
        }

        $item = Cdc_ArrayHelper::current(Cdc_ArrayHelper::current($result));

        if(!$item)
        {
            return null; // 404
        }

        $this->itemTitle = $item['nome'];

        $mPessoa = $this->getModel('Equipe', 'item');
        $responsavel = Cdc_ArrayHelper::current(Cdc_ArrayHelper::current($mPessoa->hydrateResultOf($mPessoa->createQuery(array('where' => array('id =' => (int) $item['estrutura_responsavel_id']))))));

        ob_start();
        include $this->getTemplate('estrutura/item.phtml');
        return ob_get_clean();
    }
}
