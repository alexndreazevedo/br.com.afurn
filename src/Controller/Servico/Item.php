<?php
class Controller_Servico_Item extends Controller
{
    public $pageTitle = 'Serviços';
    public $icone = 'star';

    public function content()
    {
        return $this->getItemDefault('Servico', 'servico-item', 'servico/item.phtml');
    }
}
