<?php

use Facebook\FacebookSession;
use Facebook\FacebookRequest;

class Controller_Acontece_Galeria_Index extends Controller
{
    public $pageTitle = 'Galeria';
    public $icone = 'picture-o';
    public $index = 'galeria';

    public function content()
    {
        $albuns = array();
        $total = 0;
        $limit = 10;

        FacebookSession::setDefaultApplication($this->appId, $this->appSecret);

        $session = FacebookSession::newAppSession();

        try
        {
            $session->validate();

            $query = (new FacebookRequest($session, 'GET', '/' . $this->facebookPage . '?fields=albums.fields(id,name,cover_photo,place,location,count,type)&locale=pt_BR'))->execute()->getGraphObject()->asArray();

            foreach($query['albums']->data as $album)
            {
                if($album->type == 'normal')
                {
                    $query = (new FacebookRequest($session, 'GET', '/' . $album->cover_photo . '?fields=id,name,source,width,height&locale=pt_BR'))->execute()->getGraphObject()->asArray();

                    $albuns[] = array(
                        'id' => $album->id,
                        'name' => $album->name,
                        'cover_photo' => $query,
                    );
                }
            }

            $total = count($albuns);
        }
        catch (FacebookRequestException $ex)
        {
            echo '<div class="alert alert-error fade in">' . $ex->getMessage() . '</div>';
        }
        catch (\Exception $ex)
        {
            echo '<div class="alert alert-error fade in">' . $ex->getMessage() . '</div>';
        }

        ob_start();
        include $this->getTemplate('acontece/galeria/index.phtml');
        return ob_get_clean();
    }
}