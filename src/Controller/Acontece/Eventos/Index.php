<?php

use Facebook\FacebookSession;
use Facebook\FacebookRequest;

class Controller_Acontece_Eventos_Index extends Controller
{
    public $pageTitle = 'Eventos';
    public $icone = 'calendar-o';
    public $index = 'eventos';

    public function content()
    {
        $events = array();
        $total = 0;
        $limit = 10;

        FacebookSession::setDefaultApplication($this->appId, $this->appSecret);

        $session = FacebookSession::newAppSession();

        try
        {
            $session->validate();

            $query = (new FacebookRequest($session, 'GET', '/' . $this->facebookPage . '?fields=events{id,name,location,start_time,timezone,is_date_only}&locale=pt_BR'))->execute()->getGraphObject()->asArray();

            if(key_exists('events', $query))
            {
                foreach($query['events']->data as $event)
                {
                    $events[] = $event;
                }
            }

            $total = count($events);
        }
        catch (FacebookRequestException $ex)
        {
            echo '<div class="alert alert-error fade in">' . $ex->getMessage() . '</div>';
        }
        catch (\Exception $ex)
        {
            echo '<div class="alert alert-error fade in">' . $ex->getMessage() . '</div>';
        }

        ob_start();
        include $this->getTemplate('acontece/eventos/index.phtml');
        return ob_get_clean();
    }
}