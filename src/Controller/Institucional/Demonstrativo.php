<?php
class Controller_Institucional_Demonstrativo extends Controller
{
    public $pageTitle = 'Demonstrativo Financeiro';
    public $icone = 'bar-chart-o';
    public $index = 'demonstrativo';
    
    public function content()
    {

        $page = f(INPUT_GET, 'p');
        $limit = 200;

        $sql = new Cdc_Sql_Select($this->getPdo());
        $result = $sql->cols(array(
                'bm.id',
                'bm.valor as valor',
                'bm.demonstrativo_balancomensal_ano as ano',
                'bm.demonstrativo_balancomensal_mes as mes',
                'bm.detalhes',
                'c.demonstrativo_tipo_id as tipo',
                'c.id as categoria',
                'c.nome as categoria_nome',
                'fc.id as fluxocaixa',
                'fc.nome as fluxocaixa_nome',
            ))
            ->from(array(
                'demonstrativo_balancomensal as bm',
            ))
            ->order(array(
                'ano desc',
                'mes asc',
                'tipo asc',
                'categoria asc',
                'fluxocaixa asc',
                'bm.ordem asc',
                'fc.ordem asc',
                'c.ordem asc',
            ))
            ->join(array(
                'demonstrativo_fluxocaixa as fc' => array(
                    'inner' => array(
                        'fc.id = bm.demonstrativo_fluxocaixa_id',
                    ),
                ),
                'demonstrativo_categoria as c' => array(
                    'inner' => array(
                        'c.id = fc.demonstrativo_categoria_id',
                    ),
                ),
            ))
            ->where(array(
                'bm.publicado =' => true,
                'fc.publicado =' => true,
                'c.publicado =' => true,
            ))
            ->limit(array(
                'limit' => $limit,
                'page' => $page,
            ))
            ->stmt()
            ->fetchAll();


        $total = (int) Cdc_ArrayHelper::current(current($sql->cols(array('count(*) as total'))->order(array())->limit(array())->stmt()->fetchAll()));
        $pager = Cdc_Pager::renderSimple($_SERVER['REQUEST_URI'], $total, $page, $limit);

        $lista = array();

        if($total)
        {
            foreach($result as $value)
            {
                $lista[$value['ano']][$value['mes']][$value['tipo']][$value['categoria']][$value['fluxocaixa']][] = $value;
            }
        }

        ob_start();
        include $this->getTemplate('institucional/demonstrativo.phtml');
        return ob_get_clean();
    }
}
