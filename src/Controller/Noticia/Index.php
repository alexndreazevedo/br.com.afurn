<?php
class Controller_Noticia_Index extends Controller
{
    public $pageTitle = 'Notícias';
     public $index = 'noticias';
    public $icone = 'file-text';

    public function content()
    {
        $page = f(INPUT_GET, 'p');
        $limit = 20;

        $c = $this->getModel('NoticiaCategoria', 'item');
        $categoria = $c->slugs();

        $m = $this->getModel('Noticia', 'item');
        $result = $m->hydrateResultOf($m->createQuery(array('where' => array('publicado = ' => true), 'limit' => array('limit' => $limit, 'page' => $page), 'order' => array('criacao' => 'desc'))));

        $total = key($result);
        $lista = Cdc_ArrayHelper::current($result);
        $pager = Cdc_Pager::renderSimple($_SERVER['REQUEST_URI'], $total, $page, $limit);

        ob_start();
        include $this->getTemplate('noticia/index.phtml');
        return ob_get_clean();
    }
}
