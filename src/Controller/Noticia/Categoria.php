<?php
class Controller_Noticia_Categoria extends Controller
{
    public $pageTitle = 'Notícias';
    public $icone = 'file-text';

    public function content()
    {
        $has_categoria = false;
        $slug = f($this->urlParams, 'categoria');
        $page = f(INPUT_GET, 'p');
        $id = null;
        $limit = 20;

        $c = $this->getModel('NoticiaCategoria', 'item');
        $categoria = $c->slugs();

        foreach($categoria as $categ)
        {
            if($categ['slug'] == $slug)
            {
                $has_categoria = true;
                $id = $categ['id'];
            }
        }

        if(!$has_categoria)
        {
            return null; // 404
        }

        $this->index = $slug;

        $m = $this->getModel('Noticia', 'item');
        $sql = $m->createQuery(array('where' => array('noticia_categoria_id =' => $id, 'publicado = ' => true), 'limit' => array('limit' => $limit, 'page' => $page), 'order' => array('criacao' => 'desc')));
        $result = $m->hydrateResultOf($sql);

        $total = key($result);
        $lista = Cdc_ArrayHelper::current($result);
        $pager = Cdc_Pager::renderSimple($_SERVER['REQUEST_URI'], $total, $page, $limit);

        ob_start();
        include $this->getTemplate('noticia/index.phtml');
        return ob_get_clean();
    }
}
