<?php
class Controller_Noticia_Item extends Controller
{
    public $pageTitle = 'Notícias';
    public $icone = 'file-text';

    public function content()
    {
        $m = $this->getModel('Noticia', 'item');
        $slug = f($this->urlParams, 'slug');
        $categoria = f($this->urlParams, 'categoria');
        $this->index = 'noticia-item-' . $categoria;

        if (!$slug)
        {
            return null; // 404
        }

        $result = $m->hydrateResultOf($m->createQuery(array('where' => array('slug =' => $slug))));

        $item = Cdc_ArrayHelper::current(Cdc_ArrayHelper::current($result));

        if (!$item)
        {
            return null; // 404
        }

        $mNoticiaCategoria = $this->getModel('NoticiaCategoria', 'read');
        $c = current(Cdc_ArrayHelper::current($mNoticiaCategoria->hydrateResultOf($mNoticiaCategoria->createQuery(array('cols' => array('id', 'slug', 'nome'), 'where' => array('id =' => $item['noticia_categoria_id']))))));

        if($c['slug'] != $categoria)
        {
            return null; // 404
        }

        $item['categoria'] = $c['nome'];

        $this->itemTitle = $item['titulo'];

        $url = Config::$base_url . substr($this->link('noticia-item', array('categoria' => $categoria, 'slug' => $slug)), 1);

        $share = array(
            'title' => $this->itemTitle . ' - ' . Config::$baseTitle,
            'type' => 'article',
            'url' => $url,
            //'image' => $imagem ? $imagem : $url,
            'description' => trim(html_entity_decode(strip_tags(nl2br($item['titulo'])), ENT_COMPAT, 'UTF-8')),
        );

        $this->share = array_merge($this->share, $share);

        ob_start();
        include $this->getTemplate('noticia/item.phtml');
        return ob_get_clean();
    }
}
