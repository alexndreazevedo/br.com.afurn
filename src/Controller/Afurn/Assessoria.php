<?php
class Controller_Afurn_Assessoria extends Controller
{
    public $pageTitle = 'Assessoria de Comunicação';
    public $icone = 'microphone';
    public $index = 'assessoria';

    public function content()
    {
        $m = $this->getModel('Equipe', 'item');

        $assessores = $m->hydrateResultOf($m->createQuery(array('where' => array('cargo_tipo_id =' => Equipe::TIPO_ASSESSORIA, 'publicado = ' => true), 'order' => array('ordem' => 'asc'))));

        $total = key($assessores);

        if(!empty($assessores))
        {
            $assessores = Cdc_ArrayHelper::current($assessores);
        }

        $texto = $this->renderText(Texto::ASSESSORIA, 'id', false);

        $this->itemTitle = $this->pageTitle;

        ob_start();
        include $this->getTemplate('afurn/assessoria.phtml');
        return ob_get_clean();
    }
}
