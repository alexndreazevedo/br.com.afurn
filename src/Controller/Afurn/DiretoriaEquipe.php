<?php
class Controller_Afurn_DiretoriaEquipe extends Controller
{
    public $pageTitle = 'Diretoria e Equipe';
    public $icone = 'group';
    public $index = 'diretoria_equipe';

    public function content()
    {
        $m = $this->getModel('Equipe', 'item');

        $diretoria = $m->hydrateResultOf($m->createQuery(array('where' => array('cargo_tipo_id =' => Equipe::TIPO_DIRETORIA, 'publicado = ' => true), 'order' => array('ordem' => 'asc'))));
        $equipe = $m->hydrateResultOf($m->createQuery(array('where' => array('cargo_tipo_id =' => Equipe::TIPO_FUNCIONARIO, 'publicado = ' => true), 'order' => array('ordem' => 'asc'))));

        $total_diretoria = key($diretoria);
        $total_equipe = key($equipe);

        if(!empty($diretoria))
        {
            $diretoria = Cdc_ArrayHelper::current($diretoria);
        }

        if(!empty($equipe))
        {
            $equipe = Cdc_ArrayHelper::current($equipe);
        }

        $texto_diretoria = $this->renderText(Texto::DIRETORIA, 'id', false);
        $texto_equipe = $this->renderText(Texto::EQUIPE, 'id', false);

        $this->itemTitle = $this->pageTitle;

        ob_start();
        include $this->getTemplate('afurn/diretoria_equipe.phtml');
        return ob_get_clean();
    }
}
