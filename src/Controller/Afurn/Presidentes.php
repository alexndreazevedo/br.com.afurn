<?php
class Controller_Afurn_Presidentes extends Controller
{
    public $pageTitle = 'Presidentes';
    public $icone = 'certificate';
    public $index = 'presidentes';

    public function content()
    {
        $m = $this->getModel('Equipe', 'item');

        $presidentes = $m->hydrateResultOf($m->createQuery(array('where' => array('cargo_tipo_id =' => Equipe::TIPO_PRESIDENTE, 'publicado = ' => true), 'order' => array('ordem' => 'asc'))));
		
        $total = key($presidentes);

        if(!empty($presidentes))
        {
            $presidentes = Cdc_ArrayHelper::current($presidentes);
        }
		
        $texto = $this->renderText(Texto::PRESIDENTES, 'id', false);

        $this->itemTitle = $this->pageTitle;
        
        ob_start();
        include $this->getTemplate('afurn/presidentes.phtml');
        return ob_get_clean();
    }
}
