<?php
class Controller_Afurn_Convenios extends Controller
{
    public $pageTitle = 'Convenios';
    public $icone = 'briefcase';
    public $index = 'convenios';

    public function content()
    {
        $page = f(INPUT_GET, 'p');
        $limit = 20;

        $m = $this->getModel('Parceria', 'item');
        $result = $m->hydrateResultOf($m->createQuery(array('where' => array('parceria_tipo_id =' => Parceria::TIPO_CONVENIO, 'publicado =' => true), 'limit' => array('limit' => $limit, 'page' => $page), 'order' => array('ordem' => 'asc'))));

        $total = key($result);
        $lista = Cdc_ArrayHelper::current($result);
        $pager = Cdc_Pager::renderSimple($_SERVER['REQUEST_URI'], $total, $page, $limit);

        $texto_convenios = $this->renderText(Texto::CONVENIOS, 'id', false);

        ob_start();
        include $this->getTemplate('afurn/convenios.phtml');
        return ob_get_clean();
    }
}
