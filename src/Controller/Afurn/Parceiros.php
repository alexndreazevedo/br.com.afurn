<?php
class Controller_Afurn_Parceiros extends Controller
{
    public $pageTitle = 'Parceiros';
    public $icone = 'file-text';
    public $index = 'parceiros';

    public function content()
    {
        $page = f(INPUT_GET, 'p');
        $limit = 20;

        $m = $this->getModel('Parceria', 'item');
        $result = $m->hydrateResultOf($m->createQuery(array('where' => array('parceria_tipo_id =' => Parceria::TIPO_PARCEIRO, 'publicado =' => true), 'limit' => array('limit' => $limit, 'page' => $page), 'order' => array('ordem' => 'asc'))));

        $total = key($result);
        $lista = Cdc_ArrayHelper::current($result);
        $pager = Cdc_Pager::renderSimple($_SERVER['REQUEST_URI'], $total, $page, $limit);

        $texto_parceiros = $this->renderText(Texto::PARCEIROS, 'id', false);

        ob_start();
        include $this->getTemplate('afurn/parceiros.phtml');
        return ob_get_clean();
    }
}
