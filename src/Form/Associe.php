<?php

class Form_Associe extends Contato_Abstract
{

    public $index = 'associe';

    public function init()
    {

        $this->pageTitle = Contato::$titulos[Contato::ASSOCIE_SE];
        $this->title = Contato::$titulos[Contato::ASSOCIE_SE];
        $this->slug = Cdc_Texto::slug(Contato::$titulos[Contato::ASSOCIE_SE]);
        $this->template = Contato::$template[Contato::ASSOCIE_SE];
        $this->icone = 'heart';

        $this->struct = array(
            'nome' => array(
                'type' => Cdc_Definition::TYPE_COLUMN,
                Cdc_Definition::TYPE_WIDGET => array(
                    'attributes' => array(
                        'maxlength' => 255,
                    ),
                ),
                Cdc_Definition::OPERATION => array(
                    'contato' => array(),
                ),
                Cdc_Definition::TYPE_RULE => array(
                    array('Cdc_Rule_Required'),
                    array('Cdc_Rule_Trim'),
                ),
            ),
            'cpf' => array(
                'type' => Cdc_Definition::TYPE_COLUMN,
                Cdc_Definition::TYPE_WIDGET => array(
                    'widget' => 'text',
                    'attributes' => array(
                        'maxlength' => 11,
                        'class' => 'medium cpf',
                    ),
                ),
                Cdc_Definition::OPERATION => array(
                    'contato' => array(),
                ),
                Cdc_Definition::TYPE_RULE => array(
                    array('Cdc_Rule_Required'),
                    array('Cdc_Rule_Trim'),
                    array('Cdc_Rule_CPF'),
                    array('Cdc_Rule_Length', array(1, 11)),
                ),
            ),
            'email' => array(
                'type' => Cdc_Definition::TYPE_COLUMN,
                Cdc_Definition::TYPE_WIDGET => array(
                    'widget' => 'email',
                    'attributes' => array(
                        'maxlength' => 255,
                        'class' => 'big',
                    ),
                ),
                Cdc_Definition::OPERATION => array(
                    'contato' => array(),
                ),
                Cdc_Definition::TYPE_RULE => array(
                    array('Cdc_Rule_Required'),
                    array('Cdc_Rule_Trim'),
                ),
            ),
            'telefone_fixo' => array(
                'type' => Cdc_Definition::TYPE_COLUMN,
                Cdc_Definition::TYPE_WIDGET => array(
                    'attributes' => array(
                        'maxlength' => 15,
                        'class' => 'medium tel',
                    ),
                ),
                Cdc_Definition::OPERATION => array(
                    'contato' => array(),
                ),
                Cdc_Definition::TYPE_RULE => array(
                    array('Cdc_Rule_Required'),
                    array('Cdc_Rule_Trim'),
                    array('Cdc_Rule_Numeric'),
                ),
            ),
            'telefone_celular' => array(
                'type' => Cdc_Definition::TYPE_COLUMN,
                Cdc_Definition::TYPE_WIDGET => array(
                    'attributes' => array(
                        'maxlength' => 15,
                        'class' => 'medium tel',
                    ),
                ),
                Cdc_Definition::OPERATION => array(
                    'contato' => array(),
                ),
                Cdc_Definition::TYPE_RULE => array(
                    array('Cdc_Rule_Required'),
                    array('Cdc_Rule_Trim'),
                    array('Cdc_Rule_Numeric'),
                ),
            ),
            'endereco' => array(
                'type' => Cdc_Definition::TYPE_COLUMN,
                Cdc_Definition::TYPE_WIDGET => array(
                    'attributes' => array(
                        'maxlength' => 255,
                    ),
                ),
                Cdc_Definition::OPERATION => array(
                    'contato' => array(),
                ),
                Cdc_Definition::TYPE_RULE => array(
                    array('Cdc_Rule_Trim'),
                    array('Cdc_Rule_Length', array(0, 255)),
                ),
            ),
            'complemento' => array(
                'type' => Cdc_Definition::TYPE_COLUMN,
                Cdc_Definition::TYPE_WIDGET => array(
                    'attributes' => array(
                        'maxlength' => 255,
                    ),
                ),
                Cdc_Definition::OPERATION => array(
                    'contato' => array(),
                ),
                Cdc_Definition::TYPE_RULE => array(
                    array('Cdc_Rule_Trim'),
                    array('Cdc_Rule_Length', array(0, 255)),
                ),
            ),
            'bairro' => array(
                'type' => Cdc_Definition::TYPE_COLUMN,
                Cdc_Definition::TYPE_WIDGET => array(
                    'attributes' => array(
                        'maxlength' => 255,
                        'class' => 'big',
                    ),
                ),
                Cdc_Definition::OPERATION => array(
                    'contato' => array(),
                ),
                Cdc_Definition::TYPE_RULE => array(
                    array('Cdc_Rule_Trim'),
                    array('Cdc_Rule_Length', array(0, 255)),
                ),
            ),
            'cep' => array(
                'type' => Cdc_Definition::TYPE_COLUMN,
                Cdc_Definition::TYPE_WIDGET => array(
                    'attributes' => array(
                        'maxlength' => 10,
                        'class' => 'small cep',
                    ),
                ),
                Cdc_Definition::OPERATION => array(
                    'contato' => array(),
                ),
                Cdc_Definition::TYPE_RULE => array(
                    array('Cdc_Rule_Trim'),
                ),
            ),
            'cidade' => array(
                'type' => Cdc_Definition::TYPE_COLUMN,
                Cdc_Definition::TYPE_WIDGET => array(
                    'attributes' => array(
                        'maxlength' => 255,
                        'class' => 'big',
                    ),
                ),
                Cdc_Definition::OPERATION => array(
                    'contato' => array(),
                ),
                Cdc_Definition::TYPE_RULE => array(
                    array('Cdc_Rule_Required'),
                    array('Cdc_Rule_Trim'),
                ),
            ),
            'estado' => array(
                'type' => Cdc_Definition::TYPE_COLUMN,
                Cdc_Definition::TYPE_WIDGET => array(
                    'widget' => 'select',
                    'options' => Datastore::$estados,
                    'attributes' => array(
                        'default' => 'RN',
                    ),
                ),
                Cdc_Definition::OPERATION => array(
                    'contato' => array(),
                ),
                Cdc_Definition::TYPE_RULE => array(
                    array('Cdc_Rule_Required'),
                    array('Cdc_Rule_ArrayKeyExists'),
                ),
            ),
            'valor_venda' => array(
                'type' => Cdc_Definition::TYPE_COLUMN,
                Cdc_Definition::TYPE_WIDGET => array(
                    'attributes' => array(
                        'maxlength' => 255,
                        'class' => 'medium money',
                    ),
                ),
                Cdc_Definition::OPERATION => array(
                    'contato' => array(),
                ),
                Cdc_Definition::TYPE_RULE => array(
                    array('Cdc_Rule_Trim'),
                ),
            ),
            'valor_iptu' => array(
                'type' => Cdc_Definition::TYPE_COLUMN,
                Cdc_Definition::TYPE_WIDGET => array(
                    'attributes' => array(
                        'maxlength' => 255,
                        'class' => 'medium money',
                    ),
                ),
                Cdc_Definition::OPERATION => array(
                    'contato' => array(),
                ),
                Cdc_Definition::TYPE_RULE => array(
                    array('Cdc_Rule_Trim'),
                ),
            ),
            'valor_condominio' => array(
                'type' => Cdc_Definition::TYPE_COLUMN,
                Cdc_Definition::TYPE_WIDGET => array(
                    'attributes' => array(
                        'maxlength' => 255,
                        'class' => 'medium money',
                    ),
                ),
                Cdc_Definition::OPERATION => array(
                    'contato' => array(),
                ),
                Cdc_Definition::TYPE_RULE => array(
                    array('Cdc_Rule_Trim'),
                ),
            ),
            'dormitorios' => array(
                'type' => Cdc_Definition::TYPE_COLUMN,
                Cdc_Definition::TYPE_WIDGET => array(
                    'widget' => 'integer',
                    'attributes' => array(
                        'maxlength' => 1,
                        'data-min' => 0,
                        'data-max' => 9,
                        'class' => 'mini digit',
                        'value' => '0',
                    ),
                ),
                Cdc_Definition::OPERATION => array(
                    'contato' => array(),
                ),
                Cdc_Definition::TYPE_RULE => array(
                    array('Cdc_Rule_Trim'),
                ),
            ),
            'suites' => array(
                'type' => Cdc_Definition::TYPE_COLUMN,
                Cdc_Definition::TYPE_WIDGET => array(
                    'widget' => 'integer',
                    'attributes' => array(
                        'maxlength' => 1,
                        'data-min' => 0,
                        'data-max' => 9,
                        'class' => 'mini digit',
                        'value' => '0',
                    ),
                ),
                Cdc_Definition::OPERATION => array(
                    'contato' => array(),
                ),
                Cdc_Definition::TYPE_RULE => array(
                    array('Cdc_Rule_Trim'),
                ),
            ),
            'vagas' => array(
                'type' => Cdc_Definition::TYPE_COLUMN,
                Cdc_Definition::TYPE_WIDGET => array(
                    'widget' => 'integer',
                    'attributes' => array(
                        'maxlength' => 1,
                        'data-min' => 0,
                        'data-max' => 9,
                        'class' => 'mini digit',
                        'value' => '0',
                    ),
                ),
                Cdc_Definition::OPERATION => array(
                    'contato' => array(),
                ),
                Cdc_Definition::TYPE_RULE => array(
                    array('Cdc_Rule_Trim'),
                ),
            ),
            'area_privativa' => array(
                'type' => Cdc_Definition::TYPE_COLUMN,
                Cdc_Definition::TYPE_WIDGET => array(
                    'attributes' => array(
                        'maxlength' => 255,
                        'class' => 'medium area',
                    ),
                ),
                Cdc_Definition::OPERATION => array(
                    'contato' => array(),
                ),
                Cdc_Definition::TYPE_RULE => array(
                    array('Cdc_Rule_Trim'),
                ),
            ),
            'area_total' => array(
                'type' => Cdc_Definition::TYPE_COLUMN,
                Cdc_Definition::TYPE_WIDGET => array(
                    'attributes' => array(
                        'maxlength' => 255,
                        'class' => 'medium area',
                    ),
                ),
                Cdc_Definition::OPERATION => array(
                    'contato' => array(),
                ),
                Cdc_Definition::TYPE_RULE => array(
                    array('Cdc_Rule_Trim'),
                ),
            ),
            'imagem_imovel' => array(
                'type' => Cdc_Definition::TYPE_COLUMN,
                Cdc_Definition::TYPE_WIDGET => array(
                    'widget' => 'file',
                ),
                Cdc_Definition::OPERATION => array(
                    'contato' => array(),
                ),
            ),
        );
    }

    public function getFrom()
    {
        return array(
            $this->input['nome'] => $this->input['email'],
        );
    }

    public function getTo()
    {
        return $this->to;
    }

    public function getSubject()
    {
        return 'Associe-se - ' . Config::$baseTitle;
    }

    public function getBody()
    {
        $return = '<p>Solicitação de enviada pelo site ' . Config::$baseTitle . '</p>' .
            '<p>O seguinte requerente deseja ingressar no quadro de associados da instituição.</p>' .
            '<p>' .
            '<b>' . label('nome', Config::$labels) . '</b>: ' .
            $this->input['nome'] . '<br>' .
            '<b>' . label('cpf', Config::$labels) . '</b>: ' .
            $this->input['cpf'] . '<br>' .
            '<b>' . label('email', Config::$labels) . '</b>: ' .
            $this->input['email'] . '<br>' .
            '<b>' . label('telefone_fixo', Config::$labels) . '</b>: ' .
            $this->input['telefone_fixo'] . '<br>' .
            '<b>' . label('telefone_celular', Config::$labels) . '</b>: ' .
            $this->input['telefone_celular'] . '<br>' .
            '<b>' . label('endereco', Config::$labels) . '</b>: ' .
            $this->input['endereco'] . '<br>' .
            '<b>' . label('complemento', Config::$labels) . '</b>: ' .
            $this->input['complemento'] . '<br>' .
            '<b>' . label('bairro', Config::$labels) . '</b>: ' .
            $this->input['bairro'] . '<br>' .
            '<b>' . label('cep', Config::$labels) . '</b>: ' .
            $this->input['cep'] . '<br>' .
            '<b>' . label('cidade', Config::$labels) . '</b>: ' .
            $this->input['cidade'] . '<br>' .
            '<b>' . label('estado', Config::$labels) . '</b>: ' .
            Datastore::$estados[$this->input['estado']] . '<br>' .
            '<b>' . label('valor_venda', Config::$labels) . '</b>: ' .
            $this->input['valor_venda'] . '<br>' .
            '<b>' . label('valor_iptu', Config::$labels) . '</b>: ' .
            $this->input['valor_iptu'] . '<br>' .
            '<b>' . label('valor_condominio', Config::$labels) . '</b>: ' .
            $this->input['valor_condominio'] . '<br>' .
            '<b>' . label('dormitorios', Config::$labels) . '</b>: ' .
            $this->input['dormitorios'] . '<br>' .
            '<b>' . label('suites', Config::$labels) . '</b>: ' .
            $this->input['suites'] . '<br>' .
            '<b>' . label('vagas', Config::$labels) . '</b>: ' .
            $this->input['vagas'] . '<br>' .
            '<b>' . label('area_privativa', Config::$labels) . '</b>: ' .
            $this->input['area_privativa'] . '<br>' .
            '<b>' . label('area_total', Config::$labels) . '</b>: ' .
            $this->input['area_total'] . '</p>' .
            '<p>A imagem do imóvel segue em anexo.</p>';

        return $return;
    }

    public function getMode()
    {
        return self::TYPE_HTML;
    }


    public function send()
    {
        if (!empty($_FILES))
        {
            if (array_key_exists('imagem_imovel', $_FILES))
            {
                if ($_FILES['imagem_imovel']['error'] == UPLOAD_ERR_OK)
                {
                    $this->mail->createAttachment(file_get_contents($_FILES['imagem_imovel']['tmp_name']), Zend_Mime::TYPE_OCTETSTREAM, Zend_Mime::DISPOSITION_ATTACHMENT, Zend_Mime::ENCODING_BASE64, $_FILES['imagem_imovel']['name']);
                }
            }
        }

        return parent::send();
    }

}