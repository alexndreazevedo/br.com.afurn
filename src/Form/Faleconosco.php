<?php

class Form_Faleconosco extends Contato_Abstract
{

    public $index = 'faleconosco';

    public function init()
    {

        $this->pageTitle = Contato::$titulos[Contato::FALE_CONOSCO];
        $this->title = Contato::$titulos[Contato::FALE_CONOSCO];
        $this->slug = Cdc_Texto::slug(Contato::$titulos[Contato::FALE_CONOSCO]);
        $this->template = Contato::$template[Contato::FALE_CONOSCO];

        $this->struct = array(
            'nome' => array(
                'type' => Cdc_Definition::TYPE_COLUMN,
                Cdc_Definition::TYPE_WIDGET => array(
                    'attributes' => array(
                        'maxlength' => 255,
                    ),
                ),
                Cdc_Definition::TYPE_RULE => array(
                    array('Cdc_Rule_Required'),
                    array('Cdc_Rule_Trim'),
                ),
                Cdc_Definition::OPERATION => array(
                    'contato' => array(),
                ),
            ),
            'email' => array(
                'type' => Cdc_Definition::TYPE_COLUMN,
                Cdc_Definition::TYPE_WIDGET => array(
                    'widget' => 'email',
                    'attributes' => array(
                        'maxlength' => 255,
                        'class' => 'big',
                    ),
                ),
                Cdc_Definition::TYPE_RULE => array(
                    array('Cdc_Rule_Required'),
                    array('Cdc_Rule_Trim'),
                ),
                Cdc_Definition::OPERATION => array(
                    'contato' => array(),
                ),
            ),
            'telefone' => array(
                'type' => Cdc_Definition::TYPE_COLUMN,
                Cdc_Definition::TYPE_WIDGET => array(
                    'attributes' => array(
                        'class' => 'medium tel',
                    ),
                ),
                Cdc_Definition::TYPE_RULE => array(
                    array('Cdc_Rule_Trim'),
                ),
                Cdc_Definition::OPERATION => array(
                    'contato' => array(),
                ),
            ),
            'assunto' => array(
                'type' => Cdc_Definition::TYPE_COLUMN,
                Cdc_Definition::TYPE_WIDGET => array(
                    'attributes' => array(
                        'maxlength' => 255,
                    ),
                ),
                Cdc_Definition::TYPE_RULE => array(
                    array('Cdc_Rule_Trim'),
                ),
                Cdc_Definition::OPERATION => array(
                    'contato' => array(),
                ),
            ),
            'mensagem' => array(
                'type' => Cdc_Definition::TYPE_COLUMN,
                Cdc_Definition::TYPE_WIDGET => array(
                    'widget' => 'textarea',
                    'attributes' => array(
                        'cols' => 60,
                        'rows' => 10,
                    ),
                ),
                Cdc_Definition::TYPE_RULE => array(
                    array('Cdc_Rule_Trim'),
                    array('Cdc_Rule_Required'),
                ),
                Cdc_Definition::OPERATION => array(
                    'contato' => array(),
                ),
            ),
        );
    }

    public function getFrom()
    {
        return array(
            $this->input['nome'] => $this->input['email'],
        );
    }

    public function getTo()
    {
        return $this->to;
    }

    public function getSubject()
    {
        return $this->input['assunto'];
    }

    public function getBody()
    {
        $return = '<h3>Mensagem enviada pelo site ' . Config::$baseTitle . '</h3>' .
            '<p>' .
            '<b>' . label('nome', Config::$labels) . '</b>: ' .
            $this->input['nome'] . '<br>' .
            '<b>' . label('email', Config::$labels) . '</b>: ' .
            '<a href="mailto:' . $this->input['email'] . '">' . $this->input['email'] . '</a><br>' .
            '<b>' . label('telefone', Config::$labels) . '</b>: ' .
            $this->input['telefone'] . '<br>' .
            '<b>' . label('mensagem', Config::$labels) . '</b>: ' .
            '<br>' . nl2br($this->input['mensagem']) . '</p>';

        return $return;
    }

    public function getMode()
    {
        return self::TYPE_HTML;
    }

}
