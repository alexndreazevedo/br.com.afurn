<?php

class DemonstrativoFluxoCaixa extends Datastore
{

    public function init()
    {
        $this->struct = array(
            'demonstrativo_fluxocaixa' => array(
                'type' => Cdc_Definition::TYPE_RELATION,
                'statement_type' => Cdc_Definition::STATEMENT_SELECT,
                Cdc_Definition::OPERATION => array(
                    'read' => array(),
                    'item' => array(),
                    'create' => array(
                        'statement_type' => Cdc_Definition::STATEMENT_INSERT,
                    ),
                    'update' => array(
                        'statement_type' => Cdc_Definition::STATEMENT_UPDATE,
                    ),
                    'delete' => array(
                        'statement_type' => Cdc_Definition::STATEMENT_DELETE,
                    ),
                ),
            ),
            'id' => self::primaryColumn(),
            'nome' => self::setRequired(array_merge(array('search' => array('operator' => 'like')), self::textColumn())),
            'demonstrativo_categoria_id' => array(
                'type' => Cdc_Definition::TYPE_COLUMN,
                'search' => array(
                    'operator' => '=',
                ),
                Cdc_Definition::TYPE_WIDGET => array(
                    'widget' => 'select',
                    'callback' => array(array($this, 'fetchKeyValue'), array(array('from' => array('demonstrativo_categoria c'), 'cols' => array('id', DemonstrativoCategoria::replace('c.nome as nome')), 'order' => array('c.ordem' => 'asc'), 'where' => array('c.publicado = ' => true)))),
                ),
                Cdc_Definition::TYPE_RULE => array(
                    array('Cdc_Rule_Trim'),
                    array('Cdc_Rule_ArrayKeyExists'),
                ),
                Cdc_Definition::OPERATION => array(
                    'read' => array(
                        Cdc_Definition::FORMATTER => array(array($this, 'formatter'), array($this->getPdo(), 'demonstrativo_categoria c', DemonstrativoCategoria::replace('c.nome as nome'))),
                    ),
                    'item' => array(
                        Cdc_Definition::FORMATTER => array(array($this, 'formatter'), array($this->getPdo(), 'demonstrativo_categoria c', DemonstrativoCategoria::replace('c.nome as nome'))),
                    ),
                    'create' => array(),
                    'update' => array(),
                ),
            ),
            'detalhes' => self::textareaColumn(),
            'ordem' => self::orderColumn(),
            'publicado' => self::publishedColumn(),
        );
    }
}