<?php

class Noticia extends Datastore
{
    public function init()
    {
        $this->struct = array(
            'noticia' => array(
                'type' => Cdc_Definition::TYPE_RELATION,
                'statement_type' => Cdc_Definition::STATEMENT_SELECT,
                Cdc_Definition::OPERATION => array(
                    'read' => array(),
                    'item' => array(),
                    'create' => array(
                        'statement_type' => Cdc_Definition::STATEMENT_INSERT,
                    ),
                    'update'         => array(
                        'statement_type' => Cdc_Definition::STATEMENT_UPDATE,
                    ),
                    'delete'         => array(
                        'statement_type' => Cdc_Definition::STATEMENT_DELETE,
                    ),
                ),
                Cdc_Definition::TYPE_ATTACHMENT => array(
                    'arquivo' => self::arquivoAttachment('noticia'),
                ),
            ),
            'id' => self::primaryColumn(),
            'imagem' => self::arquivoColumn(),
            'slug' => self::slugColumn(),
            'titulo' => self::setRequired(array_merge(array('search' => array('operator' => 'like')), self::textColumn())),
            'noticia_categoria_id' => array(
                'type' => Cdc_Definition::TYPE_COLUMN,
                'search' => array(
                    'operator' => '=',
                ),
                Cdc_Definition::TYPE_WIDGET => array(
                    'widget' => 'select',
                    'callback' => array(array($this, 'fetchKeyValue'), array(array('from' => array('noticia_categoria'), 'cols' => array('id', 'nome'), 'order' => array('nome' => 'asc')))),
                    'attributes' => array(
                        'required' => 'required',
                    ),
                ),
                Cdc_Definition::TYPE_RULE => array(
                    array('Cdc_Rule_Trim'),
                    array('Cdc_Rule_Required'),
                    array('Cdc_Rule_ArrayKeyExists'),
                ),
                Cdc_Definition::OPERATION => array(
                    'read' => array(
                        Cdc_Definition::FORMATTER => array(array($this, 'formatter'), array($this->getPdo(), 'noticia_categoria')),
                    ),
                    'item' => array(
                        Cdc_Definition::FORMATTER => array(array($this, 'formatter'), array($this->getPdo(), 'noticia_categoria')),
                    ),
                    'create' => array(),
                    'update' => array(),
                ),
            ),
            'subtitulo' => self::setRequired(self::textColumn(null, null, 255, 0, array(
                'item' => array(),
                'create' => array(),
                'update' => array(),
            ))),
            'resumo' => self::textareaColumn(),
            'texto' => self::richColumn(),
            'criacao' => self::dateColumn(array('item' => array())),
            'atualizacao' => self::dateColumn(array('item' => array())),
            'publicado' => self::publishedColumn(),
        );
    }

    public function execute($sql)
    {
        if ($sql->getOperation() == 'create')
        {
            $sql->cols['criacao'] = date('Y-m-d H:i:s');
            $sql->cols['slug'] = Cdc_Texto::slug($sql->cols['titulo']);
        }
        if($sql->getOperation() == 'update')
        {
            $sql->cols['atualizacao'] = date('Y-m-d H:i:s');
            $sql->cols['slug'] = Cdc_Texto::slug($sql->cols['titulo']);
        }

        return parent::execute($sql);
    }

}