<?php

class Texto extends Base_Texto
{

    const APRESENTACAO  = 1;
    const HISTORIA      = 2;
    const ESTATUTO      = 3;
    const PRESIDENTES   = 4;
    const DIRETORIA     = 5;
    const EQUIPE        = 6;
    const ASSESSORIA    = 7;
    const PARCEIROS     = 8;
    const CONVENIOS     = 9;
    const ESTRUTURA     = 10;
    const SERVICOS      = 11;
    const ASSOCIE_SE    = 12;
    const GALERIA       = 13;
    const EVENTOS       = 14;
    const FALE_CONOSCO  = 15;

    public static $titulos = array(
        self::APRESENTACAO  => 'Apresentação',
        self::HISTORIA      => 'História',
        self::ESTATUTO      => 'Estatuto',
        self::PRESIDENTES   => 'Mensagem do Presidente',
        self::DIRETORIA     => 'Diretoria',
        self::EQUIPE        => 'Equipe',
        self::ASSESSORIA    => 'Assessoria de Comunicação',
        self::PARCEIROS     => 'Parcerias',
        self::CONVENIOS     => 'Convênios',
        self::ESTRUTURA     => 'Estrutura',
        self::SERVICOS      => 'Serviços',
        self::ASSOCIE_SE    => 'Associe-se',
        self::GALERIA       => 'Associe-se',
        self::EVENTOS       => 'Associe-se',
        self::FALE_CONOSCO  => 'Fale Conosco',
    );

    public static $corpo = array(
        self::APRESENTACAO  => 'A AFURN - Associação dos Funcionários da Universidade Federal do Rio Grande do Norte tem como principal objetivo desenvolver atividades de socialização, que proporcionem qualidade de vida e bem estar para seus associados e dependentes. Para isso conta com uma infra-estrutura que oferece atendimento  de saúde, administrativo,  educacional, esportivo, jurídico,  cultural e de lazer, com sedes próprias para cada atendimento:
 Sede administrativa – Mirassol/Natal
 Centro Clinico – Mirassol/Natal
 Centro de treinamento – Ribeira/Natal
 Sede social de Búzios – Praia de Búzios/Nísia Floresta
 Sede social de Santa Cruz – município de Santa Cruz/RN
  AFURN Fitness Academia = Mirassol/Natal
 Atualmente a AFURN presta assistência a cerca de 3000 associados, que usufruem dos serviços da entidade, além de desfrutar dos convênios com diversas empresas da cidade: supermercados, farmácias, óticas, livrarias, sapatarias, planos de saúde, clínicas, colégios e postos de combustíveis.
 Mais informação sobre como se tornar sócio ligue (84) 3231-6330, ou procure a sede administrativa da AFURN, localizada na Rua das violetas, 628-B, no conjunto Mirassol.',
        self::HISTORIA      => '<div align="center">
	<strong>Primeiros passos da AFURN</strong></div>
<div>
	&nbsp;</div>
<div>
	&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; O ano de 1961 entrou para a história após o soviético Yuri Gagarin tornar-se o primeiro homem a viajar pelo espaço. Enquanto isso, no Brasil, a eleição de Jânio Quadros e sua súbita renúncia agitaram a política e a sociedade no país. Neste mesmo ano nascia em Natal a <strong>Associação dos Funcionários da Universidade Federal do Rio Grande do Norte (AFURN)</strong>.</div>
<div>
	Fundada em 29 de maio, a <strong>AFURN</strong> é uma entidade sem fins lucrativos que tem por objetivos a prestação de assistência social, jurídica, cultural, esportiva e financeira através de programas específicos aos sócios servidores ativos, aposentados e pensionistas da UFRN.</div>
<div>
	&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Dentre os servidores que assinam a ata de fundação estavam Ailson Marinho Lopes, Antonio Gentil de Medeiros, Antonio Diógenes Fernandes, Augusto Carlos Bezerra de Melo, Ana Maria de Castro Guerra, Carlos Alberto Moreira Campos, Clesito César Fechine, Edmilson Cavalcante, Francisco de Assis Rocha Cavalcante, Getúlio Leopoldo Machado da Câmara, Gladstone Cardoso, Hélio Xavier de Vasconcelos, João Augusto Monteiro Bezerra de Melo, José Gomes de Oliveira, Maria Bandeira Guedes, Maria Evilda Machado Dantas, Maria Zélia Braz Gomes, Pedro Pinheiro de Souza, Pedro Ivo de Medeiros, Sebastião Fernandes de Oliveira Neto, Saulo Colaço Chaves, Waldir Lopes da Silva e Wanda Machado da Câmara. Presidiu os trabalhos o Sr. Francisco de Assis Rocha Cavalcante, tendo como Secretário o Sr. Hélio Xavier de Vasconcelos.</div>
<div>
	&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; No mesmo dia foi escolhida uma diretoria provisória, e no dia 22 de junho, foi realizada a primeira assembléia geral extraordinária e formada a comissão eleitoral para conduzir as eleições que escolheria a primeira diretoria executiva da Associação dos Funcionários da UFRN.</div>
<div>
	&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; As primeiras reuniões da entidade aconteciam na antiga sede da Reitoria da Universidade, que ficava na Av. Hermes da Fonseca, onde hoje está sediado o Comando do 3&ordm; Distrito Naval.</div>
<div>
	&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; A votação, que aconteceu no dia 23, nas dependências da Faculdade de Farmácia, elegeu como primeiro presidente da AFURN o servidor Francisco de Assis Rocha Cavalcante, e como vice-presidente, João Augusto Monteiro Bezerra de Melo. Pedro Pinheiro de Souza foi escolhido 1o. secretário, e José Gomes de Oliveira, 2o. secretário. Como titular da tesouraria, Antonio Diógenes Fernandes e Wanda Machado da Câmara, Vice-tesoureira, e como primeiro orador da AFURN, Hélio Xavier de Vasconcelos.</div>
<div>
	&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; O conselho fiscal, eleito também na mesma assembléia, ficou assim constituído: Luiz Gonçalves Pinheiro, Saulo Colaço Chaves, Manoel Rodrigues de Melo, Clesito César Fechine e Célia Viana Lira, Titulares; Mário Guedes da Silva, Ana Maria de Castro Guerra, Carlos Alberto Moreira Campos, Áurea Cavalcanti Albuquerque e Iolanda Dantas como suplentes. &nbsp;Zila Mamede foi indicada, por unanimidade, para dirigir o Departamento Cultural, e Waldir Lopes da Silva para o Departamento de Cooperativismo. Sebastião Fernandes de Oliveira Neto ficou com o Departamento de Esportes e Diversões, Airton de Castro com o Departamento de Turismo e Colônia de Férias, Maria de Lourdes Reis Câmara eleita para o Departamento Cultural (provisoriamente) e Romeu Aranha Marinho para o Departamento Social e Jurídico.</div>
<div>
	&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; &nbsp;O mandato da primeira diretoria da Associação dos Funcionários da UFRN seguiu até o dia 28 de maio de 1964.</div>
<div>
	.&nbsp;</div>
<p>
	&nbsp;</p>
<p>
	&nbsp;</p>
',
        self::ESTATUTO      => '<p style="text-align: center;">

	<strong>ESTATUTO DA ASSOCIAÇÃO DOS FUNCIONÁRIOS DA UNIVERSIDADE</strong><br />

	<strong><strong>FEDERAL DO RIO GRANDE DO NORTE &ndash; AFURN </strong></strong></p>

<p>

	<strong>CAPÍTULO I &ndash; DOS FINS SEDE E DURAÇÃO</strong></p>

<p>

	<strong>Art. 1&deg;</strong> - Associação dos Funcionários da Universidade Federal do Rio Grande do Norte &ndash; AFURN fundada em 29 de maio de 1961, com sede e foro nesta capital, com inscrição no cadastro Geral de Contribuintes do Ministério da Fazenda sob n&deg; 08.215.600/0001-89, registrado no 2&deg; Ofício de notas às fls. 106 do livro n&deg; A-4 sob o n&deg; de ordem 604 desta comarca. Entidade sem fins lucrativos, com tempo de duração indeterminado e seu ano social coincide com o ano civil, reconhecida como de utilidade pública municipal conforme Lei n&deg; 4.710 de 19 de dezembro de 1995, regendo-se pelo presente estatuto, tendo por finalidade precípua a prestação de assistência social, esportiva e cultural aos seus sócios servidores ativos, aposentados e pensionistas da UFRN.</p>

<p>

	<strong>Art. 2&deg; - SÃO FINALIDADES DA AFURN:</strong><br />

	a) Representar junto aos poderes legalmente constituídos, em juízo ou fora dele, as questões de interesse de seus associados;<br />

	b) Manter relações com demais entidades;<br />

	c) Prestar assistência social, jurídica, cultural, esportiva e financeira através de programas específicos que vierem a ser definidos pelas possibilidades financeira da entidade.</p>

<p>

	<strong>CAPÍTULO II &ndash; DA ESTRUTURA ORGANIZACIONAL</strong><br />

	<br />

	<strong>Art. 3&deg;</strong> - A Associação dos Funcionários da Universidade Federal do Rio Grande do Norte &ndash; AFURN, tem a seguinte estrutura:<br />

	a) Assembléia Geral;<br />

	b) Diretoria Executiva;<br />

	c) Conselho Fiscal;</p>

<p>

	<strong>DA ASSEMBLÉIA GERAL</strong></p>

<p>

	<strong>Art. 4&deg;</strong> - A Assembléia Geral é o órgão máximo de deliberação composta de todos os associados presentes à convocação e em pleno gozo de seus direitos.</p>

<p>

	<strong>Art. 5&deg; - A ASSEMBLÉIA GERAL COMPETE</strong><br />

	a) Deliberar sobre todos os assuntos de interesse da categoria, sendo suas decisões de caráter soberano;<br />

	b) Deliberar a qualquer época de acordo com proposta da Diretoria Executiva a reforma do presente estatuto ou extraordinariamente sempre que for necessário;<br />

	c) A proposta definida no item anterior será aprovada em primeira convocação por 3% dos seus associados, e em seguida e última convocação, com intervalo de 20 minutos com 2% de seus associados;<br />

	d) Aprovar ou rejeitar as contas, o orçamento e o balanço proposto pela Diretoria Executiva com parecer prévio do Conselho Fiscal;<br />

	e) Destituir membros eleitos, resguardado o direito de defesa e o quorum mínimo estabelecido no item &ldquo;C&rdquo;;<br />

	f) Dar posse à Diretoria;<br />

	g) Aprovar hipoteca, venda e doações de bens móveis, imóveis e ações, bem como, construções novas, compra e troca de bens imóveis e ações, proposta pela Diretoria Executiva com parecer do Conselho Fiscal;<br />

	h) Fixar o valor da contribuição individual dos associados;<br />

	i) Deliberar sobre recursos processuais de atos cometidos pela Diretoria Executiva;<br />

	j) Disciplinar o processo eleitoral de forma suplementar naquilo que não constar deste Estatuto.</p>

<p>

	<strong>Art. 6&deg;</strong> - A Assembléia Geral se reunirá ordinariamente uma vez por ano para deliberar sobre a prestação de contas ao fim do ano fiscal, por convocação da Diretoria Executiva ou por 2% dos seus associados.</p>

<p>

	<strong>Art. 7&deg;</strong> - A Assembléia só pode deliberar sobre assuntos constantes do edital de convocação. As decisões da assembléia só podem ser modificadas ou revogadas através de outra assembléia convocada para este fim.</p>

<p>

	<strong>Art. 8&deg;</strong> - A Assembléia deverá ser convocada, com no mínimo cinco dias úteis de antecedência salvo as ordinárias e nos casos previstos nos itens b e d do Art. 5&deg;.</p>

<p>

	<strong>Art. 9&deg;</strong> - A Assembléia deliberará sua pauta através da maioria simples dos membros presentes e o tipo de votação será decidido no início dos trabalhos.</p>

<p>

	<strong>PARÁGRAFO ÚNICO:</strong> - A Assembléia será instalada pelo Presidente, pelo Vice-Presidente, ou membro da Diretoria Executiva indicado pelo Presidente ou seu substituto.</p>

<p>

	<strong>DA DIRETORIA EXECUTIVA</strong></p>

<p>

	<strong>Art. 10&deg;</strong> - A DIRETORIA EXECUTIVA COMPÕE-SE DE<br />

	a) Presidente;<br />

	b) Vice-Presidente;<br />

	c) 1&deg; Secretário;<br />

	d) 2&deg; Secretário;<br />

	e) Diretor Financeiro;<br />

	f) Diretor Financeiro Adjunto e Patrimonial;<br />

	g) Diretor de Promoções Sociais;</p>

<p>

	h) Diretor de Promoções Esportivas<br />

	i) Diretor de Assuntos Jurídicos e Integração;<br />

	j) Diretor de Apoio ao Aposentado.</p>

<p>

	<strong>Art. 11&deg;</strong> - A Diretoria Executiva será eleita em eleição direta e secreta, conforme o disposto neste estatuto.</p>

<p>

	<strong>Art. 12&deg;</strong> <strong>- A DIRETORIA EXECUTIVA COMPETE:</strong><br />

	a) Elaborar o orçamento e a prestação de contas da Associação;<br />

	b) Deliberar sobre gestão de pessoal, orçamento e prestação de contas da Associação;<br />

	c) Administrar o patrimônio da entidade;<br />

	d) Convocar Assembléia Geral através da maioria de seus membros;<br />

	e) Decidir sobre compra e troca de bens móveis, bem como o descarte destes bens julgados inservíveis, respeitando o limite estabelecido no item &ldquo;a&rdquo; e &ldquo;g&rdquo; Art 5&deg;;<br />

	f) Analisar e decidir sobre proposta de empréstimos financeiros para a entidade, encaminhando para Assembléia Geral quando os empréstimos comprometerem acima de 50% do patrimônio líquido apresentado no último balanço anual, devidamente corrigido monetariamente;<br />

	g) Encaminhar proposta para Assembléia Geral sobre hipoteca, venda e doações de bens móveis, imóveis e ações, bem como, construções novas, compra e troca de bens imóveis e ações;<br />

	h) Deliberar sobre ingresso e exclusão de associados;<br />

	i) Autorizar despesas administrativas de acordo com orçamento aprovado;<br />

	j) Aplicar penalidades previstas neste estatuto.</p>

<p>

	<strong>PARÁGRAFO 1&deg;</strong> - CONSELHO AUXILIAR Composto por 05 (cinco) membros, sendo 02 (dois) eleitos em assembléia e 03 (três) indicados pela Diretoria Executiva.</p>

<p>

	<strong>PARÁGRAFO 2&deg;</strong> - Os 02 (dois) membros escolhidos em Assembléia Geral, serão eleitos e empossados na mesma Assembléia que elegerá Conselho Fiscal.</p>

<p>

	<strong>Art. 13&deg; - AO PRESIDENTE COMPETE</strong><br />

	a) Representar juntamente com o Diretor Jurídico, a associação ativa e passivamente em juízo ou fora dele, nas demandas judiciais de interesse dos associados;<br />

	b) Convocar e presidir as assembléias e reuniões da Diretoria Executiva, cumprindo suas decisões;<br />

	c) Aprovar despesas dentro dos limites deste estatuto;<br />

	d) Visar despesas aprovadas pela Diretoria Executiva e assinar juntamente com o Diretor Financeiro, cheques e ordem de pagamento;<br />

	e) Assinar juntamente com o Diretor Financeiro, acordos, convênios, contratos e outros instrumentos legais com vista ao atendimento dos objetivos da associação, respeitando os limites estabelecidos neste estatuto;<br />

	f) Preparar juntamente com o Diretor Financeiro a prestação de contas e balanço anual;<br />

	g) Aplicar elogios e penalidades, decididas pela Diretoria Executiva, ou pela Assembléia Geral;<br />

	h) Nomear comissão de sindicância, baixar portarias e editais de interesse da Associação.</p>

<p>

	<strong>Art. 14&deg; - AO VICE PRESIDENTE COMPETE</strong><br />

	a) Substituir o Presidente nas ausências e impedimentos legais, inclusive em caso de vacância do cargo;<br />

	b) Auxiliar o Presidente em suas obrigações administrativas.</p>

<p>

	<strong>Art. 15&deg; - AO 1&deg; SECRETÁRIO COMPETE</strong><br />

	a) Secretariar as reuniões de Diretoria Executiva e Assembléia Geral;<br />

	b) Organizar os arquivos e memória da entidade;<br />

	c) Incumbir-se do expediente da entidade.</p>

<p>

	<strong>Art. 16&deg; - AO 2&deg; SECRETÁRIO COMPETE</strong><br />

	a) Auxiliar o 1&deg; Secretário em suas atividades, substituindo-o suas faltas, impedimento e em caso de vacância;</p>

<p>

	<strong>Art. 17&deg; - AO DIRETOR FINANCEIRO COMPETE</strong><br />

	a) Administrar as finanças da associação, efetuando o controle de receitas e despesas;<br />

	b) Apresentar o balancete mensal e balanço anual;<br />

	c) Assinar juntamente com o Presidente os cheques e/ou ordens de pagamento emitidas pela associação;<br />

	d) Manter a contabilidade em ordem providenciando as medidas necessárias ao atendimento das exigências legais;<br />

	e) Celebrar juntamente com o Presidente, convênios, acordos e contratos.</p>

<p>

	<strong>Art. 18&deg; - AO DIRETOR FINANCEIRO ADJUNTO E PATRIMONIAL COMPETE</strong><br />

	a) Auxiliar o Diretor Financeiro em suas tarefas;<br />

	b) Substituir o Diretor Financeiro em impedimentos e ausências inclusive em caso de vacância;<br />

	c) Controlar os bens patrimoniais promovendo o seu cadastramento e controle físico e contábil;<br />

	d) Promover e controlar os processos de compras de bens duráveis e de consumo, observando as normas estabelecidas pela Diretoria Executiva.</p>

<p>

	<strong>Art. 19&deg; - AO DIRETOR DE PROMOÇÕES SOCIAIS COMPETE</strong><br />

	a) Dirigir as atividades sociais da associação;<br />

	b) Promover atividades recreativas e culturais;<br />

	c) Administrar a sede social.</p>

<p>

	<strong>PARÁGRAFO ÚNICO</strong> &ndash; Esta Diretoria contará com uma assessoria indicada pela Diretoria Executiva com a seguinte atribuição:<br />

	a) Auxiliar a Diretoria Social em suas atribuições.</p>

<p>

	<strong>Art. 20&deg; - AO DIRETOR DE PROMOÇÕES ESPORTIVAS COMPETE</strong><br />

	a) Dirigir o Departamento Esportivo promovendo competições e jogos esportivos;<br />

	b) Inscrever a Associação em torneios e campeonatos nas diversas modalidades esportivas;<br />

	c) Ter sob sua guarda material esportivo da associação zelando por sua conservação e controlando o uso adequado;<br />

	d) Nomear assessores para distribuição de tarefas do departamento, podendo delegar poderes no âmbito de sua competência.</p>

<p>

	<strong>Art. 21&deg; - AO DIRETOR DE ASSUNTOS JURÍDICOS E INTEGRAÇÃO COMPETE</strong><br />

	a) Emitir parecer em processos submetidos a sua apreciação;<br />

	b) Coordenar e acompanhar as ações judiciais de interesse da associação e seus associados;<br />

	c) Promover a integração da Instituição a outras Entidades.</p>

<p>

	<strong>Art. 22&deg; - AO DIRETOR DE APOIO AO APOSENTADO COMPETE</strong><br />

	a) Desenvolver programações nas áreas de lazer, artes, cultura e esporte;<br />

	b) Promover e integrar o sócio aposentado à associação;<br />

	c) Defender os interesses da categoria;<br />

	d) Divulgar entre os aposentados as programações oferecidas pela associação.</p>

<p>

	<strong>Art. 23&deg;</strong> - Os Diretores exercem seus cargos sem direito a remuneração podendo a critério da Diretoria receber, em casos específicos ajuda de custos para cumprimento de atividades da AFURN com a devida comprovação.</p>

<p>

	<strong>Art. 24&deg;</strong> - Todos os membros da Diretoria Executiva são obrigados a comparecer as Assembléias Gerais e reuniões de Diretoria.</p>

<p>

	<strong>Art. 25&deg;</strong> - A Diretoria Executiva se reunirá ordinariamente uma vez por mês em dia e hora determinado na primeira reunião ou extraordinariamente quando convocada pelo Presidente ou pela maioria de seus membros.</p>

<p>

	<strong>Art. 26&deg;</strong> - O Diretor que faltar a três reuniões consecutivas ou seis alternadas sem justificativa, será advertido e em caso de reincidência sofrerá a perda do mandato, a cargo da Diretoria Executiva.</p>

<p>

	<strong>Art. 27&deg; </strong>- O mandato da Diretoria Executiva é de 03 (três) anos e será eleita em escrutínio direto e secreto na forma do presente estatuto.</p>

<p>

	<strong>PARÁGRAFO ÚNICO</strong> - Poderá haver recondução de qualquer membro da Diretoria Executiva por mais um mandato consecutivo.</p>

<p>

	<br />

	<strong>CAPITÚLO III &ndash; DA REPRESENTAÇÃO</strong></p>

<p>

	<strong>Art. 28&deg;</strong> - Em cada unidade do interior que contar com mais de 40 sócios será instalada uma representação.</p>

<p>

	<strong>PARÁGRAFO ÚNICO</strong> &ndash; Cada representação será dirigida por um Representante Geral e um auxiliar eleitos pelos sócios da AFURN naquela unidade.</p>

<p>

	<strong>Art. 29&deg; - O REPRESENTANTE TERÁ AS SEGUINTES ATRIBUIÇÕES</strong><br />

	a) Presidir Assembléias e reuniões de sua representação;<br />

	b) Gerir bens patrimoniais e financeiros da representação, cumprindo com as determinações emanadas da Diretoria Executiva e deliberações de assembléia geral;<br />

	c) Receber e prestar contas, junto a Diretoria Executiva, dos recursos financeiros, controlando e fiscalizando as despesas autorizadas;<br />

	d) Desenvolver programas com vista as atividades de lazer, esporte e cultural.</p>

<p>

	<strong>Art.30&deg; - AO REPRESENTANTE AUXILIAR COMPETE</strong>: Colaborar com o Representante Geral em suas tarefas substituindo-o em casos de impedimentos, afastamentos e em caso de vacância.</p>

<p>

	<strong>Art. 31&deg;</strong> - Nas unidades do interior em que não tiver número suficiente para implantação de uma representação, estes passam a ser integrados à representação geograficamente mais próxima.</p>

<p>

	<strong>CAPÍTULO IV &ndash; DO QUADRO SOCIAL</strong></p>

<p>

	<strong>Art. 32&deg;</strong> - Todo servidor ativo, aposentado e pensionistas da Universidade Federal do Rio Grande do Norte tem direito a ser sócio.</p>

<p>

	<strong>Art. 33&deg;</strong> - A admissão do sócio se dará através de assinatura de uma proposta e aprovada pela Diretoria Executiva.</p>

<p>

	<strong>Art. 34&deg;</strong> - Todo sócio admitido até 60 (sessenta) dias antes da eleição tem direito a votar e só poderá ser votado após 180 (cento e oitenta) dias da eleição.</p>

<p>

	<strong>Art. 35&deg;</strong> - Os pensionistas e dependentes inscritos no Departamento de Pessoal da UFRN gozarão dos benefícios da Associação exceto os previstos no artigo anterior.</p>

<p>

	<strong>DOS DEVERES DOS SÓCIOS</strong></p>

<p>

	<strong>Art. 36&deg; - A TODO SÓCIO COMPETE:</strong><br />

	a) Zelar o patrimônio da Associação, indenizando-a quando por sua culpa, imprudência ou negligência danificar bens da Associação;<br />

	b) Cumprir e fazer cumprir o presente estatuto, os regimentos e regulamentos, e acatar as deliberações tomadas pelos organismos da Associação;<br />

	c) Pagar mensalmente as contribuições e autorizar o pagamento de despesas realizadas na associação, através de fontes pagadoras de seus vencimentos, ou Instituições Bancárias com autorização do sócio;<br />

	d) Apresentar sempre que lhe for exigida, documentação comprobatória do quadro social;<br />

	e) Comparecer às Assembléia Gerais, denunciar junto a Diretoria Executiva irregularidades cometidas por diretores da associação;<br />

	f) Prestar contas das tarefas que lhe forem atribuídas pelos órgãos da Associação;<br />

	g) O sócio mesmo que tenha solicitado desligamento do quadro social junto ao DAP/UFRN, não estará isento do cumprimento dos seus débitos contraídos anteriormente através da AFURN.</p>

<p>

	<strong>DOS DIREITOS DOS SÓCIOS</strong></p>

<p>

	<strong>Art. 37&deg; - SÃO DIREITOS DOS SÓCIOS</strong><br />

	a) Requerer convocações de assembléia geral;<br />

	b) Exercer atividades junto aos organismos da associação;<br />

	c) Participar de reuniões e eventos promovidos pela AFURN;<br />

	d) Ter acesso a toda informação de caráter administrativo e financeiro da Associação;<br />

	e) Participar como ouvinte das reuniões de Diretorias;<br />

	f) Utilizar-se de todos os serviços prestados pela Associação;<br />

	g) Fiscalizar o funcionamento da Associação e manifestar-se sobre o mesmo;<br />

	h) Auxílio funeral: 02 (dois) salários mínimos vigentes a ser pago a um dos dependentes legais, mediante documento comprobatório do óbito. O prazo para solicitação do referido auxílio é de 60 (sessenta) dias a contar do óbito, desde que esteja cumprindo o Art. 36 letra &ldquo;C&rdquo; deste estatuto.</p>

<p>

	<strong>CAPÍTULO V &ndash; DO CONSELHO FISCAL</strong></p>

<p>

	<strong>Art. 38&deg;</strong> - O Conselho Fiscal, com mandato de 03 (três) anos, será composto por 05 (cinco) membros eleitos e empossados em Assembléia Geral Extraordinária convocada para este fim até 60 (sessenta) dias após a posse da Diretoria Executiva, os 03 (três) candidatos mais votados serão os titulares e os 02 (dois) menos votados os suplentes.</p>

<p>

	<strong>PARÁGRAFO 1&deg; - COMPETE AO CONSELHO FISCAL</strong><br />

	a) Apreciar as contas e relatórios da Diretoria Executiva reunindo-se para esse fim Ordinariamente a cada três meses e extraordinariamente quando convocado pelo Presidente do Conselho ou pela maioria de seus membros efetivos;<br />

	b) Solicitar a Diretoria Executiva ou qualquer diretor esclarecimentos e informações podendo se assim considerar necessário convocar toda Diretoria Executiva;<br />

	c) Emitir parecer sobre contas e relatórios a serem apreciadas pela Assembléia Geral;<br />

	d) Eleger através dos seus membros efetivos dentre os membros titulares o seu próprio Presidente;<br />

	e) Elaborar o seu regimento interno.</p>

<p>

	<strong>PARÁGRAFO 2&deg;</strong> - É vedada a reeleição consecutiva de qualquer membro deste conselho, bem como a acumulação de cargos dentro desta Associação.</p>

<p>

	<strong>CAPÍTULO VI &ndash; DO PATRIMÔNIO</strong></p>

<p>

	<strong>Art. 39&deg; - CONSTITUEM PATRIMÔNIO DA AFURN:</strong><br />

	a) Os bens móveis e imóveis adquiridos;<br />

	b) Doações e recursos que lhe sejam destinados;<br />

	c) Receitas ordinárias e extraordinárias;</p>

<p>

	<strong>PARÁGRAFO ÚNICO</strong> &ndash; até 30% do valor arrecadado das contribuições dos sócios lotados em uma representação será revertido à mesma.</p>

<p>

	<strong>Art. 40&deg; </strong>- No caso de extinção desta Associação o destino do patrimônio será decidido na Assembléia Geral que aprovar a sua extinção.</p>

<p>

	<br />

	<strong>CAPÍTULO VII &ndash; DO PROCESSO ELEITORAL</strong></p>

<p>

	<strong>Art. 41&deg;</strong> - A Eleição para Diretoria Executiva será convocada pelo Presidente ou seu substituto legal, no mínimo 50 (cinqüenta) dias antes do término do mandato da Diretoria Executiva, devendo realizar-se no mínimo dez dias úteis antes daquele término.</p>

<p>

	<strong>Art. 42&deg;</strong> - A convocação será realizada através de edital publicado em jornal de circulação no Estado do Rio Grande do Norte e afixado nos pontos de maior afluência de servidores na UFRN, onde deve constar o prazo para inscrição da(s) chapa(s) e a data da eleição.</p>

<p>

	<strong>PARÁGRAFO ÚNICO</strong> &ndash; A inscrição de chapas se dará até 15 (quinze) dias antes da data determinada para as eleições e a ordem das chapas se dará por sorteio.</p>

<p>

	<strong>Art. 43&deg;</strong> - A inscrição de chapas, contendo os nomes e matrículas de todos os candidatos aos diversos cargos se dará junto a Comissão Eleitoral, que é composta de cinco membros titulares e dois suplentes eleitos em Assembléia Geral.</p>

<p>

	<strong>Art. 44&deg; </strong>- Terão direito a votar e ser votado todos os sócios em gozo de seus direitos, na forma deste estatuto, não sendo permitido o voto por procuração.</p>

<p>

	<strong>Art. 45&deg;</strong> - As chapas concorrentes no ato da inscrição poderão indicar um representante para fiscalizar o processo eleitoral.</p>

<p>

	<strong>Art. 46&deg;</strong> - A Comissão Eleitoral publicará, locais de votação e horário de funcionamento das mesas receptoras de votos no diversos setores da UFRN e na sede da Associação onde votarão os sócios aposentados.</p>

<p>

	<strong>Art. 47&deg;</strong> - É vedada a candidatura de membros da Comissão Eleitoral.</p>

<p>

	<strong>Art. 48&deg; - COMPETE A COMISSÃO ELEITORAL:</strong><br />

	a) Organizar e fiscalizar o pleito;<br />

	b) Proceder a inscrição de chapas;<br />

	c) Determinar locais e horários de votação bem como início da apuração;<br />

	d) Instalar as mesas receptoras de votos;<br />

	e) Verificar a relação dos sócios aptos a votarem;<br />

	f) Proceder apuração de votos e homologar a chapa vencedora;<br />

	g) Julgar em única instância recursos impetrados pelos fiscais ou pelas chapas.</p>

<p>

	<strong>Art. 49&deg;</strong> - A Comissão Eleitoral terá um Presidente escolhido entre seus membros titulares.</p>

<p>

	<strong>Art. 50&deg; </strong>- A apuração se dará em local determinado pela Comissão Eleitoral, os votos das representações do interior serão apurados no próprio local e o resultado deverá ser enviado a Comissão Eleitoral.</p>

<p>

	<strong>Art. 51&deg;</strong> - Na mesa receptora de votos, deverá constar todo material necessário a realização do pleito.</p>

<p>

	<strong>Art. 52&deg;</strong> - No ato de votação, o sócio deverá se identificar através de documento oficial que contenha foto.</p>

<p>

	<strong>Art. 53&deg; </strong>- A proclamação dos eleitos será dada pela Comissão Eleitoral dois dias após o encerramento da apuração.</p>

<p>

	<strong>Art. 54&deg;</strong> - O prazo para recurso do resultado será até 24 horas após o encerramento da apuração.</p>

<p>

	<strong>Art. 55&deg; </strong>- As decisões da Comissão Eleitoral se darão através da maioria dos membros titulares.</p>

<p>

	<strong>Art. 56&deg; - SERÁ DECLARADA VENCEDORA</strong><br />

	a) Havendo mais de duas chapas inscritas no pleito e nenhuma obtiver 50% dos votos mais um, haverá um segundo turno, no prazo de dez dias úteis, entre as duas chapas que obtiveram maior número de votos;<br />

	b) Havendo segundo turno a chapa que obtiver o maior número de votos válidos;<br />

	c) Havendo apenas uma chapa será eleita com 50% dos votos válidos mais um;<br />

	d) Não havendo chapa vencedora será convocada nova eleição dentro de trinta dias.</p>

<p>

	<strong>PARÁGRAFO ÚNICO</strong> &ndash; Havendo nova eleição e o prazo extrapolar o mandato em vigor, este fica prorrogado até cinco dias úteis após a realização do pleito que declarar uma chapa vencedora.</p>

<p>

	<br />

	<strong>CAPÍTULO VIII &ndash; DAS DISPOSIÇÕES GERAIS E TRANSITÓRIAS</strong></p>

<p>

	<strong>Art. 57&deg; </strong>- O ocorrendo vacância de qualquer cargo (exceto o de Presidente), será convidado pela Diretoria Executiva um sócio para assumir o cargo vago.</p>

<p>

	<strong>Art. 58&deg;</strong> - O sócio convidado, obrigatoriamente deverá está em gozo dos seus direitos e deveres conforme estatuto em vigor.</p>

<p>

	<strong>Art. 59&deg;</strong> - O Art. 27 entrará em vigor a contar da posse da nova Diretoria Executiva período 2001/2004.</p>

<p>

	<strong>Art. 60&deg;</strong>- Este estatuto entra em vigor a partir de sua aprovação em Assembléia Geral extraordinária e registro em cartório de sua competência, ficando revogada qualquer disposição em contrário.</p>

',
        self::PRESIDENTES   => ' A AFURN – Associação dos funcionários da UFRN existe para atender as necessidades dos associados no tocante aos serviços de assistência social, jurídica, cultural, esportiva e financeira. A associação possui programas específicos que permitem o funcionamento pleno dos serviços oferecidos, além de dispor de uma infraestrutura de qualidade e um quadro de recursos humanos qualificado.
Oferecemos uma ampla rede de convênios de serviços e produtos em estabelecimentos reconhecidos na cidade: SESI, SESC, farmácias, supermercados, postos de combustíveis, óticas, sapatarias, livrarias, dentre outros. Tudo isso pensado para o melhor atendimento ao associado AFURN.
Em destaque, além da sede administrativa, temos o Centro Clinico e o Centro de treinamento, braços da instituição que complementam a oferta de serviços. No Centro Clinico dispomos de diversas especialidades médicas e um completo atendimento odontológico. E no Centro de Treinamento o foco é a preparação dos funcionários e seus dependentes, para as demandas novas do mercado de trabalho. São oferecidos cursos de idiomas e de informática, ministrados por professores qualificados.
Nosso objetivo é estar sempre à disposição do funcionário ativo, aposentado e seus dependentes, oferecendo o que há de melhor em serviços.

Francisco Carlúcio Porfírio
Presidente da AFURN
',
        self::DIRETORIA     => '[Texto para Diretoria]',
        self::EQUIPE        => '[Texto para Equipe]',
        self::ASSESSORIA    => '[Texto para Assessoria de Comunicação]',
        self::PARCEIROS     => '[Texto para Parcerias]',
        self::CONVENIOS     => '[Texto para Convênios]',
        self::ESTRUTURA     => '[Texto para Estrutura]',
        self::SERVICOS      => '[Texto para Serviços]',
        self::ASSOCIE_SE    => '[Texto para Associe-se]',
        self::GALERIA       => '[Texto para Galeria de Fotos]',
        self::EVENTOS       => '[Texto para Eventos]',
        self::FALE_CONOSCO  => '[Texto para Fale Conosco]',
    );

    public static $paginas = array(
        Texto::APRESENTACAO,
        Texto::HISTORIA,
        Texto::ESTATUTO,
    );
}