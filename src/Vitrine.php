<?php

class Vitrine extends Base_Usuario
{
    const MESMA_JANELA = '_self';
    const NOVA_JANELA = '_blank';

    public static $janela_destino = array(
        self::MESMA_JANELA => 'Mesma janela',
        self::NOVA_JANELA => 'Nova janela',
    );

    public function init()
    {
        $this->struct = array(
            'vitrine' => array(
                'type' => Cdc_Definition::TYPE_RELATION,
                'statement_type' => Cdc_Definition::STATEMENT_SELECT,
                Cdc_Definition::OPERATION => array(
                    'read' => array(),
                    'item' => array(),
                    'create' => array(
                        'statement_type' => Cdc_Definition::STATEMENT_INSERT,
                    ),
                    'update' => array(
                        'statement_type' => Cdc_Definition::STATEMENT_UPDATE,
                    ),
                    'delete' => array(
                        'statement_type' => Cdc_Definition::STATEMENT_DELETE,
                    ),
                ),
                Cdc_Definition::TYPE_ATTACHMENT => array(
                    'arquivo' => self::arquivoAttachment('vitrine'),
                ),
            ),
            'id' => self::primaryColumn(),
            'imagem' => self::arquivoColumn(),
            'titulo' => self::setRequired(self::textColumn()),
            'texto' => self::textareaColumn(),
            'url' => array(
                'type' => Cdc_Definition::TYPE_COLUMN,
                Cdc_Definition::TYPE_WIDGET => array(),
                Cdc_Definition::OPERATION => array(
                    //'read' => array(),
                    'item' => array(),
                    'create' => array(),
                    'update' => array(),
                ),
                Cdc_Definition::TYPE_RULE => array(),
            ),
            'janela_destino' => array(
                'type' => Cdc_Definition::TYPE_COLUMN,
                Cdc_Definition::TYPE_WIDGET => array(
                    'widget' => 'select',
                    'options' => self::$janela_destino,
                    'attributes' => array(
                        'required' => 'required',
                    ),
                ),
                Cdc_Definition::OPERATION => array(
                    'item' => array(),
                    'create' => array(),
                    'update' => array(),
                ),
                Cdc_Definition::TYPE_RULE => array(
                    array('Cdc_Rule_Required'),
                    array('Cdc_Rule_Trim'),
                    array('Cdc_Rule_ArrayKeyExists'),
                ),
            ),
            'ordem' => self::orderColumn(),
            'publicado' => self::publishedColumn(),
        );
    }

}