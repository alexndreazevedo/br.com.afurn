<?php

class Servico extends Datastore
{
    public $servicos = null;

    const ASSESSORIA_JURIDICA = 1;
    const CENTRO_CLINICO = 2;
    const CENTRO_TREINAMENTO = 3;
    const ACADEMIA_DANCA = 4;
    const CONVENIOS = 5;

    public static $titulos = array(

        self::ASSESSORIA_JURIDICA => 'Assessoria Jurídica',
        self::CENTRO_CLINICO => 'Centro Clínico',
        self::CENTRO_TREINAMENTO => 'Centro de Treinamento',
        self::ACADEMIA_DANCA => 'Academia e Dança',
        self::CONVENIOS => 'Sistema de Convênios',

    );

    public static $texto = array(

        self::ASSESSORIA_JURIDICA => 'Texto para Assessoria Jurídica',
        self::CENTRO_CLINICO => 'Texto para Centro Clínico',
        self::CENTRO_TREINAMENTO => 'O Centro de Treinamento e Cultura da AFURN oferece cursos de qualificação profissional,&nbsp;além de sua Escola de Idiomas e&nbsp;Centro de Informática, direcionados aos associados e dependentes da AFURN e comunidade em geral.
	&nbsp; Interessados procurar a sede do CT AFURN, na Avenida Junqueira Ayres, 365 &ndash; Centro. Mais informações pelo telefone: 3222-9660.
<h3>CURSOS 2012.2</h3>
<table class="table table-bordered table-striped table-hover">
	<thead>
		<tr>
			<th>CURSO</th>
			<th>DURAÇÃO</th>
			<th>MATERIAL</th>
			<th colspan="2">MENSALIDADES</th>
		</tr>
	</thead>
	<tbody>
		<tr>
			<td>Inglês</td>
			<td>3 anos (Básico ao Avançado)</td>
			<td>Interchange</td>
			<td>Sócio: R$30,00</td>
			<td>Não Sócio: R$ 60,00</td>
		</tr>
		<tr>
			<td>Espanhol</td>
			<td>3 anos (Básico ao Avançado)</td>
			<td>Espanhol sem fronteiras</td>
			<td>Sócio: R$30,00</td>
			<td>Não Sócio: R$ 60,00</td>
		</tr>
		<tr>
			<td>Informática</td>
			<td>6 meses</td>
			<td>5 Apostilas (R$ 40,00)</td>
			<td>Sócio: R$30,00</td>
			<td>Não Sócio: R$ 60,00</td>
		</tr>
		<tr>
			<td>Assistente Administrativo</td>
			<td>4 meses</td>
			<td>Apostila (R$ 20,00)</td>
			<td>Sócio: R$30,00</td>
			<td>Não Sócio: R$ 60,00</td>
		</tr>
	</tbody>
</table>',
        self::ACADEMIA_DANCA => 'Texto para Academia e Dança',
        self::CONVENIOS => 'Texto para Sistema de Convênios',
    );

    public static $localidade = array(

        self::ASSESSORIA_JURIDICA => Estrutura::COMPLEXO_MIRASSOL,
        self::CENTRO_CLINICO => Estrutura::COMPLEXO_MIRASSOL,
        self::CENTRO_TREINAMENTO => Estrutura::POSTO_RIBEIRA,
        self::ACADEMIA_DANCA => Estrutura::COMPLEXO_MIRASSOL,
        self::CONVENIOS => null,
    );

    public function init()
    {
        $this->struct = array(
            'servico' => array(
                'type' => Cdc_Definition::TYPE_RELATION,
                'statement_type' => Cdc_Definition::STATEMENT_SELECT,
                Cdc_Definition::OPERATION => array(
                    'read' => array(),
                    'item' => array(),
                    'create' => array(
                        'statement_type' => Cdc_Definition::STATEMENT_INSERT,
                    ),
                    'update'         => array(
                        'statement_type' => Cdc_Definition::STATEMENT_UPDATE,
                    ),
                    'delete'         => array(
                        'statement_type' => Cdc_Definition::STATEMENT_DELETE,
                    ),
                ),
                Cdc_Definition::TYPE_ATTACHMENT => array(
                    'arquivo' => self::arquivoAttachment('servico'),
                ),
            ),
            'id' => self::primaryColumn(),
            'imagem' => self::arquivoColumn(),
            'slug' => self::slugColumn(),
            'nome' => self::setRequired(self::textColumn()),
            'servico_estrutura_id' => array(
                'type' => Cdc_Definition::TYPE_COLUMN,
                Cdc_Definition::TYPE_WIDGET => array(
                    'widget' => 'select',
                    'callback' => array(array($this, 'fetchKeyValue'), array(array('from' => array('estrutura'), 'cols' => array('id', 'nome'), 'order' => array('nome' => 'asc')))),
                ),
                Cdc_Definition::TYPE_RULE => array(
                    array('Cdc_Rule_Trim'),
                    array('Cdc_Rule_ArrayKeyExists'),
                ),
                Cdc_Definition::OPERATION => array(
                    'read' => array(
                        Cdc_Definition::FORMATTER => array(array($this, 'formatter'), array($this->getPdo(), 'estrutura')),
                    ),
                    'item' => array(
                        Cdc_Definition::FORMATTER => array(array($this, 'formatter'), array($this->getPdo(), 'estrutura')),
                    ),
                    'create' => array(),
                    'update' => array(),
                ),
            ),
            'servico_responsavel_id' => array(
                'type' => Cdc_Definition::TYPE_COLUMN,
                Cdc_Definition::TYPE_WIDGET => array(
                    'widget' => 'select',
                    'callback' => array(array($this, 'fetchKeyValue'), array(array('from' => array('equipe'), 'cols' => array('id', 'nome'), 'order' => array('ordem' => 'asc')))),
                ),
                Cdc_Definition::TYPE_RULE => array(
                    array('Cdc_Rule_Trim'),
                    array('Cdc_Rule_ArrayKeyExists'),
                ),
                Cdc_Definition::OPERATION => array(
                    'read' => array(
                        Cdc_Definition::FORMATTER => array(array($this, 'formatter'), array($this->getPdo(), 'equipe')),
                    ),
                    'item' => array(
                        Cdc_Definition::FORMATTER => array(array($this, 'formatter'), array($this->getPdo(), 'equipe')),
                    ),
                    'create' => array(),
                    'update' => array(),
                ),
            ),
            'texto' => self::richColumn(),
        );
    }

    public function getHtmlOptions($row, $rowset, $args)
    {
        if (!$this->allow($row))
        {
            return '';
        }

        $return = '';
        $link_args = array(
            'r' => $args['controller']->relation
        );
        $link_http_params = array(
            'op' => 'update',
            $args['controller']->primary => $row[$args['controller']->primary],
        );
        $return .= '<a class="btn" rel="tooltip" title="Editar" href="' . $args['controller']->link('admin', $link_args, $link_http_params) . '"><i class="fa fa-pencil"></i></a>';

        return '<div class="btn-group">' . $return . '</div>';
    }

    public function slugs()
    {
        if (null === $this->servicos)
        {
            $sql = new Cdc_Sql_Select($this->getPdo());
            $sql->cols = array('id', 'slug', 'nome');
            $sql->from = array('servico');
            $this->sedes = Cdc_ArrayHelper::keyPair($sql->stmt()->fetchAll());
        }
        return $this->sedes;
    }
    public function execute($sql)
    {
        if ($sql->getOperation() == 'create' || $sql->getOperation() == 'update')
        {
            $sql->cols['slug'] = Cdc_Texto::slug($sql->cols['nome']);
        }

        return parent::execute($sql);
    }

}