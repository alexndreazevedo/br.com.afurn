<?php

abstract class Controller extends Cdc_Controller
{

    public $title = '';
    public $subtitle = null;
    public $icone = null;
    public $sidebar = false;
    public $slugs = array();
    public $categorias = array();
    public $sedes = array();
    public $servicos = array();
    public $share = array();
    public $appId = '1538899456374452';
    public $appSecret = '097746ecfa880ef6abfd8a59eab82577';
    public $facebookPage = 'associacaodosfuncionariosdaufrn';

    public function init()
    {
        $modelTexto = $this->getModel('Texto');
        $this->slugs = $modelTexto->slugs();

        $modelNoticiaCategoria = $this->getModel('NoticiaCategoria');
        $this->categorias = $modelNoticiaCategoria->slugs();

        $modelEstrutura = $this->getModel('Estrutura');
        $this->sedes = $modelEstrutura->slugs();

        $modelServico = $this->getModel('Servico');
        $this->servicos = $modelServico->slugs();

        $this->share = array(
            'og:title' => $this->titlebar(),
            'og:type' => 'website',
            'og:url' => Config::$base_url,
            'og:image' => Config::$base_url . 'assets/site/images/share.jpg',
            'og:site_name' => Config::$baseTitle,
            'fb:admins' => $this->appId,
        );
    }

    public function renderText($item, $column = 'id', $show_container = true)
    {
        $model = $this->getModel('Texto', 'item');
        $result = Cdc_ArrayHelper::current(
            $model->hydrateResultOf(
                $model->createQuery(
                    array(
                        'where' => array(
                            $column . ' =' => $item
                        )
                    )
                )
            )
        );

        if (!$result)
        {
            return null;
        }

        $item = current($result);

        if ($show_container)
        {
            ob_start();
            include $this->getTemplate('texto/item.phtml', 'base');
            return ob_get_clean();
        }

        return $item;
    }

    public function getContact($secao)
    {
        $lista = array();

        $model = $this->getModel('Contato', 'send');
        $result = Cdc_ArrayHelper::current(
            $model->hydrateResultOf(
                $model->createQuery(
                    array(
                        'where' => array(
                            'secao =' => $secao
                        )
                    )
                )
            )
        );

        if (!$result)
        {
            return null;
        }

        foreach ($result as $key => $value)
        {
            $lista[$value['nome']] = $value['email'];
        }

        return $lista;
    }

    public function getItemDefault($class, $link_item, $template)
    {
        $m = $this->getModel($class, 'item');
        $slug = f($this->urlParams, 'slug');

        $this->index = $slug;

        if (!$slug)
        {
            $default = Cdc_ArrayHelper::current(
                Cdc_ArrayHelper::current(
                    $m->hydrateResultOf(
                        $m->createQuery(
                            array(
                                'where' => array(
                                    'id =' => key($class::$titulos)
                                )
                            )
                        )
                    )
                )
            );

            header('Location: ' . $this->link($link_item, array('slug' => $default['slug'])));
            die;
        }
        else
        {
            $result = $m->hydrateResultOf(
                $m->createQuery(
                    array(
                        'where' => array(
                            'slug =' => $slug
                        )
                    )
                )
            );
        }

        $item = Cdc_ArrayHelper::current(Cdc_ArrayHelper::current($result));

        if (!$item)
        {
            return null; // 404
        }

        $this->itemTitle = $item['nome'];

        ob_start();
        include $this->getTemplate($template);
        return ob_get_clean();
    }

    public function menu()
    {
        $slugs = $this->slugs;
        $categorias = $sedes = $servicos = array();

        foreach ($this->categorias as $key => $value)
        {
            $categorias[$value['slug']] = array(
                'title' => $value['nome'],
                'url' => $this->link('noticia-categoria', array('categoria' => $value['slug'])),
            );
        }

        $categorias['noticias'] = array(
            'title' => 'Todas as Notícias',
            'url' => $this->link('noticia'),
        );

        foreach ($this->sedes as $key => $value)
        {
            $sedes[$value['slug']] = array(
                'title' => $value['nome'],
                'url' => $this->link('estrutura-item', array('slug' => $value['slug'])),
            );
        }

        foreach ($this->servicos as $key => $value)
        {
            $servicos[$value['slug']] = array(
                'title' => $value['nome'],
                'url' => $this->link('servico-item', array('slug' => $value['slug'])),
            );
        }

        return array(
            'institucional' => array(
                'title' => 'Institucional',
                'url' => 'javascript:;',
                'submenu' => array(
                    'apresentacao' => array(
                        'title' => $slugs[Texto::APRESENTACAO]['nome'],
                        'url' => $this->link('institucional', array(
                            'slug' => $slugs[Texto::APRESENTACAO]['slug'])),
                    ),
                    'historia' => array(
                        'title' => $slugs[Texto::HISTORIA]['nome'],
                        'url' => $this->link('institucional', array(
                            'slug' => $slugs[Texto::HISTORIA]['slug'])),
                    ),
                    'estatuto' => array(
                        'title' => $slugs[Texto::ESTATUTO]['nome'],
                        'url' => $this->link('institucional', array(
                            'slug' => $slugs[Texto::ESTATUTO]['slug'])),
                    ),
                    'demonstrativo' => array(
                        'title' => 'Demonstrativo',
                        'url' => $this->link('demonstrativo'),
                    ),
                ),
            ),
            'afurn' => array(
                'title' => 'AFURN',
                'url' => $this->link('presidentes'),
                'submenu' => array(
                    'presidentes' => array(
                        'title' => 'Presidentes',
                        'url' => $this->link('presidentes'),
                    ),
                    'diretoria_equipe' => array(
                        'title' => 'Diretoria e equipe',
                        'url' => $this->link('diretoria_equipe'),
                    ),
                    'assessoria' => array(
                        'title' => 'Assess. de Comunicação',
                        'url' => $this->link('assessoria'),
                    ),
                    'parceiros' => array(
                        'title' => 'Parceiros',
                        'url' => $this->link('parceiros'),
                    ),
                    'convenios' => array(
                        'title' => 'Convênios',
                        'url' => $this->link('convenios'),
                    ),
                ),
            ),
            'noticia' => array(
                'title' => 'Notícias',
                'url' => 'javascript:;',
                'submenu' => $categorias,
            ),
            'estrutura' => array(
                'title' => 'Estrutura',
                'url' => 'javascript:;',
                'submenu' => $sedes,
            ),
            'servicos' => array(
                'title' => 'Serviços',
                'url' => 'javascript:;',
                'submenu' => $servicos,
            ),
            'acontece' => array(
                'title' => 'Acontece',
                'url' => 'javascript:;',
                'submenu' => array(
                    'galeria' => array(
                        'title' => 'Galeria de Fotos',
                        'url' => $this->link('acontece-galeria'),
                    ),
                    'eventos' => array(
                        'title' => 'Eventos',
                        'url' => $this->link('acontece-eventos'),
                    ),
                ),
            ),
            'faleconosco' => array(
                'title' => $slugs[Texto::FALE_CONOSCO]['nome'],
                'url' => $this->link('faleconosco'),
            ),
        );
    }

    public function breadcrumb()
    {
        $slugs = $this->slugs;
        $categorias = $sedes = $servicos = array();

        foreach ($this->categorias as $key => $value)
        {
            $categorias[$value['slug']] = array(
                'title' => 'Sobre ' . $value['nome'],
                'url' => $this->link('noticia-categoria', array('categoria' => $value['slug'])),
                'submenu' => array(
                    'noticia-item-' . $value['slug'] => array(
                        'title' => 'notícia',
                        'url' => 'javascript:;',
                    ),
                ),
            );
        }

        $categorias['noticias'] = array(
            'title' => 'Todas as Notícias',
            'url' => $this->link('noticia'),
        );

        foreach ($this->sedes as $key => $value)
        {
            $sedes[$value['slug']] = array(
                'title' => $value['nome'],
                'url' => $this->link('estrutura-item', array('slug' => $value['slug'])),
            );
        }

        foreach ($this->servicos as $key => $value)
        {
            $servicos[$value['slug']] = array(
                'title' => $value['nome'],
                'url' => $this->link('servico-item', array('slug' => $value['slug'])),
            );
        }

        return array(
            'home' => array(
                'title' => 'Início',
                'url' => $this->link('home'),
                'submenu' => array(
                    'institucional' => array(
                        'title' => 'Institucional',
                        'url' => $this->link('institucional', array(
                            'slug' => $slugs[Texto::APRESENTACAO]['slug'])),
                        'submenu' => array(
                            $slugs[Texto::APRESENTACAO]['slug'] => array(
                                'title' => $slugs[Texto::APRESENTACAO]['nome'],
                                'url' => $this->link('institucional', array(
                                    'slug' => $slugs[Texto::APRESENTACAO]['slug'])),
                            ),
                            $slugs[Texto::HISTORIA]['slug'] => array(
                                'title' => $slugs[Texto::HISTORIA]['nome'],
                                'url' => $this->link('institucional', array(
                                    'slug' => $slugs[Texto::HISTORIA]['slug'])),
                            ),
                            $slugs[Texto::ESTATUTO]['slug'] => array(
                                'title' => $slugs[Texto::ESTATUTO]['nome'],
                                'url' => $this->link('institucional', array(
                                    'slug' => $slugs[Texto::ESTATUTO]['slug'])),
                            ),
                            'demonstrativo' => array(
                                'title' => 'Demonstrativo Financeiro',
                                'url' => $this->link('demonstrativo'),
                            ),
                        ),
                    ),
                    'afurn' => array(
                        'title' => 'AFURN',
                        'url' => $this->link('presidentes'),
                        'submenu' => array(
                            'presidentes' => array(
                                'title' => 'Presidentes',
                                'url' => $this->link('presidentes'),
                            ),
                            'diretoria_equipe' => array(
                                'title' => 'Diretoria e equipe',
                                'url' => $this->link('diretoria_equipe'),
                            ),
                            'assessoria' => array(
                                'title' => 'Assess. de Comunicação',
                                'url' => $this->link('assessoria'),
                            ),
                            'parceiros' => array(
                                'title' => 'Parceiros',
                                'url' => $this->link('parceiros'),
                            ),
                            'convenios' => array(
                                'title' => 'Convênios',
                                'url' => $this->link('convenios'),
                            ),
                        ),
                    ),
                    'noticia' => array(
                        'title' => 'Notícias',
                        'url' => $this->link('noticia'),
                        'submenu' => $categorias,
                    ),
                    'estrutura' => array(
                        'title' => 'Estrutura',
                        'url' => $this->link('estrutura'),
                        'submenu' => $sedes,
                    ),
                    'servico' => array(
                        'title' => 'Serviços',
                        'url' => $this->link('servico'),
                        'submenu' => $servicos,
                    ),
                    'associe' => array(
                        'title' => $slugs[Texto::ASSOCIE_SE]['nome'],
                        'url' => $this->link('associe'),
                    ),
                    'acontece' => array(
                        'title' => 'Acontece',
                        'url' => $this->link('acontece'),
                        'submenu' => array(
                            'galeria' => array(
                                'title' => 'Galeria de Fotos',
                                'url' => $this->link('acontece-galeria'),
                                'submenu' => array(
                                    'album' => array(
                                        'title' => 'Álbum',
                                    ),
                                ),
                            ),
                            'eventos' => array(
                                'title' => 'Eventos',
                                'url' => $this->link('acontece-eventos'),
                            ),
                        ),
                    ),
                    'faleconosco' => array(
                        'title' => $slugs[Texto::FALE_CONOSCO]['nome'],
                        'url' => $this->link('faleconosco'),
                    ),
                ),
            ),
        );
    }

    public function atalhos($sidebar = false)
    {
        $slugs = $this->slugs;

        if ($sidebar)
        {

            return array(
                'associe' => array(
                    'icon' => '<i class="fa fa-heart"></i>',
                    'title' => $slugs[Texto::ASSOCIE_SE]['nome'],
                    'url' => $this->link('associe'),
                ),
                'convenios' => array(
                    'icon' => '<i class="fa fa-tag"></i>',
                    'title' => 'Convênios',
                    'url' => $this->link('convenios'),
                ),
                'informativo' => array(
                    'icon' => '<i class="fa fa-bullhorn"></i>',
                    'title' => 'Informativo',
                    'url' => 'javascript:;',
                ),
                'curta' => array(
                    'icon' => '<i class="fa fa-thumbs-up"></i>',
                    'title' => 'Curta no Face',
                    'url' => 'http://facebook.com/' . $this->facebookPage,
                    'target' => '_blank',
                ),
            );
        }
        else
        {
            return array(
                'associe' => array(
                    'icon' => '<i class="fa fa-heart"></i>',
                    'title' => $slugs[Texto::ASSOCIE_SE]['nome'],
                    'url' => $this->link('associe'),
                ),
                'convenios' => array(
                    'icon' => '<i class="fa fa-tag"></i>',
                    'title' => 'Convênios',
                    'url' => $this->link('convenios'),
                ),
                'centroclinico' => array(
                    'icon' => '<i class="fa fa-plus-circle"></i>',
                    'title' => 'Centro Clínico',
                    'url' => $this->link('servico-item', array('slug' => $this->servicos[Servico::CENTRO_CLINICO]['slug'])),
                ),
                'servicos' => array(
                    'icon' => '<i class="fa fa-star"></i>',
                    'title' => 'Serviços',
                    'url' => $this->link('servico'),
                ),
                'sedes' => array(
                    'icon' => '<i class="fa fa-thumb-tack"></i>',
                    'title' => 'Sedes',
                    'url' => $this->link('estrutura'),
                ),
                'informativo' => array(
                    'icon' => '<i class="fa fa-bullhorn"></i>',
                    'title' => 'Informativo',
                    'url' => 'javascript:;',
                ),
                'curta' => array(
                    'icon' => '<i class="fa fa-thumbs-up"></i>',
                    'title' => 'Curta no Face',
                    'url' => 'http://facebook.com/' . $this->facebookPage,
                    'target' => '_blank',
                ),
                'faleconosco' => array(
                    'icon' => '<i class="fa fa-comments"></i>',
                    'title' => $slugs[Texto::FALE_CONOSCO]['nome'],
                    'url' => $this->link('faleconosco'),
                ),
            );
        }
    }

    public function menu_rodape($index)
    {
        $return = '';
        $slugs = $this->slugs;
        $sedes = $servicos = array();

        foreach ($this->sedes as $key => $value)
        {
            $sedes[$value['slug']] = array(
                'title' => $value['nome'],
                'url' => $this->link('estrutura-item', array(
                    'slug' => $value['slug'])),
            );
        }

        foreach ($this->servicos as $key => $value)
        {
            $servicos[$value['slug']] = array(
                'title' => $value['nome'],
                'url' => $this->link('servico-item', array(
                    'slug' => $value['slug'])),
            );
        }

        $menu = array(
            array(
                'index' => array(
                    'title' => 'Início',
                    'url' => $this->link('home'),
                ),
                'institucional' => array(
                    'title' => 'Institucional',
                    'url' => $this->link('institucional', array(
                        'slug' => $slugs[Texto::APRESENTACAO]['slug'])),
                    'submenu' => array(
                        'apresentacao' => array(
                            'title' => $slugs[Texto::APRESENTACAO]['nome'],
                            'url' => $this->link('institucional', array(
                                'slug' => $slugs[Texto::APRESENTACAO]['slug'])),
                        ),
                        'historia' => array(
                            'title' => $slugs[Texto::HISTORIA]['nome'],
                            'url' => $this->link('institucional', array(
                                'slug' => $slugs[Texto::HISTORIA]['slug'])),
                        ),
                        'estatuto' => array(
                            'title' => $slugs[Texto::ESTATUTO]['nome'],
                            'url' => $this->link('institucional', array(
                                'slug' => $slugs[Texto::ESTATUTO]['slug'])),
                        ),
                        'demonstrativo' => array(
                            'title' => 'Demonstrativo',
                            'url' => $this->link('demonstrativo'),
                        ),
                    ),
                ),
            ),
            array(
                'afurn' => array(
                    'title' => 'AFURN',
                    'url' => $this->link('presidentes'),
                    'submenu' => array(
                        'presidentes' => array(
                            'title' => 'Presidentes',
                            'url' => $this->link('presidentes'),
                        ),
                        'diretoria_equipe' => array(
                            'title' => 'Diretoria e equipe',
                            'url' => $this->link('diretoria_equipe'),
                        ),
                        'assessoria' => array(
                            'title' => 'Assess. de Comunicação',
                            'url' => $this->link('assessoria'),
                        ),
                        'parceiros' => array(
                            'title' => 'Parceiros',
                            'url' => $this->link('parceiros'),
                        ),
                        'convenios' => array(
                            'title' => 'Convênios',
                            'url' => $this->link('convenios'),
                        ),
                    ),
                ),
            ),
            array(
                'noticias' => array(
                    'title' => 'Notícias',
                    'url' => $this->link('noticia'),
                ),
                'estrutura' => array(
                    'title' => 'Estrutura Física',
                    'url' => $this->link('estrutura'),
                    'submenu' => $sedes,
                ),
            ),
            array(
                'servico' => array(
                    'title' => 'Serviços',
                    'url' => $this->link('servico'),
                    'submenu' => $servicos,
                ),
            ),
            array(
                'associe' => array(
                    'title' => $slugs[Texto::ASSOCIE_SE]['nome'],
                    'url' => $this->link('associe'),
                ),
                'acontece' => array(
                    'title' => 'Acontece',
                    'url' => $this->link('acontece'),
                    'submenu' => array(
                        'galeria' => array(
                            'title' => 'Galeria de Fotos',
                            'url' => $this->link('acontece-galeria'),
                        ),
                        'eventos' => array(
                            'title' => 'Eventos',
                            'url' => $this->link('acontece-eventos'),
                        ),
                    ),
                ),
                'faleconosco' => array(
                    'title' => $slugs[Texto::FALE_CONOSCO]['nome'],
                    'url' => $this->link('faleconosco'),
                ),
            ),
        );

        $return .= '<ul class="menu">';

        foreach ($menu as $bloco)
        {
            $return .= '<li class="bloco">' . Cdc_Navigation::menu($bloco, $index, '<ul>') . '</li>';
        }

        $return .= '</ul>';

        return $return;
    }

    public function menu_admin()
    {
        $is_admin = f($_SESSION, 'administrador');
        $usuario_allowed = Config::resourceAllowed('Admin_Usuario', $is_admin);
        $grupo_allowed = Config::resourceAllowed('Admin_Grupo', $is_admin);

        $menu_sistema = array(
            'conteudo' => array(
                'title' => 'Conteúdo do Site',
                'url' => '#',
                'submenu' => array(
                    'Vitrine' => array(
                        'url' => $this->link('admin', array(
                            'r' => 'Vitrine')),
                        'title' => 'Vitrine',
                    ),
                    'Texto' => array(
                        'url' => $this->link('admin', array(
                            'r' => 'Base_Texto')),
                        'title' => 'Textos',
                    ),
                ),
            ),
            'afurn' => array(
                'title' => 'AFURN',
                'url' => '#',
                'submenu' => array(
                    'Estrutura' => array(
                        'url' => $this->link('admin', array(
                            'r' => 'Estrutura')),
                        'title' => 'Estrutura',
                    ),
                    'Servico' => array(
                        'url' => $this->link('admin', array(
                            'r' => 'Servico')),
                        'title' => 'Serviços',
                    ),
                    'Equipe' => array(
                        'url' => $this->link('admin', array(
                            'r' => 'Equipe')),
                        'title' => 'Equipe',
                    ),
                    'Parceria' => array(
                        'url' => $this->link('admin', array(
                            'r' => 'Parceria')),
                        'title' => 'Parceiros e Convênios',
                    ),
                ),
            ),
            'demonstrativo' => array(
                'title' => 'Demonstrativo',
                'url' => '#',
                'submenu' => array(
                    'DemonstrativoBalancoMensal' => array(
                        'url' => $this->link('admin', array(
                            'r' => 'DemonstrativoBalancoMensal')),
                        'title' => 'Balanço Mensal',
                    ),
                    'DemonstrativoFluxoCaixa' => array(
                        'url' => $this->link('admin', array(
                            'r' => 'DemonstrativoFluxoCaixa')),
                        'title' => 'Fluxo de Caixa',
                    ),
                    'DemonstrativoCategoria' => array(
                        'url' => $this->link('admin', array(
                            'r' => 'DemonstrativoCategoria')),
                        'title' => 'Categoria',
                    ),
                ),
            ),
            'noticias' => array(
                'title' => 'Notícias',
                'allow' => $usuario_allowed || $grupo_allowed,
                'url' => '#',
                'class' => 'teste',
                'submenu' => array(
                    'Noticia' => array(
                        'url' => $this->link('admin', array(
                            'r' => 'Noticia')),
                        'title' => 'Notícias',
                        'allow' => $usuario_allowed,
                    ),
                    'NoticiaCategoria' => array(
                        'url' => $this->link('admin', array(
                            'r' => 'NoticiaCategoria')),
                        'title' => 'Categorias',
                        'allow' => $usuario_allowed,
                    ),
                ),
            ),
            'preferencias' => array(
                'title' => 'Preferências',
                'allow' => $usuario_allowed || $grupo_allowed,
                'url' => '#',
                'alt' => 'Preferências',
                'submenu' => array(
                    'Contato_Email' => array(
                        'url' => $this->link('admin', array(
                            'r' => 'Contato_Email')),
                        'title' => 'Destinatários',
                        'allow' => $usuario_allowed,
                    ),
                    'Base_Usuario' => array(
                        'url' => $this->link('admin', array(
                            'r' => 'Base_Usuario')),
                        'title' => 'Usuários',
                        'allow' => $usuario_allowed,
                    ),
                    'Base_Grupo' => array(
                        'url' => $this->link('admin', array(
                            'r' => 'Base_Grupo')),
                        'title' => 'Grupos',
                        'allow' => $grupo_allowed,
                    ),
                    'Base_ArquivoTipo' => array(
                        'url' => $this->link('admin', array(
                            'r' => 'Base_ArquivoTipo')),
                        'title' => 'Tipos de Arquivo',
                        'allow' => $grupo_allowed,
                    ),
                ),
            ),
            'logout' => array(
                'url' => $this->link('admin_logout'),
                'title' => '<i class="fa fa-power-off"></i> Sair',
                'alt' => 'Sair',
            ),
        );

        return $menu_sistema;
    }

    public function sidebar()
    {
        ob_start();
        include $this->getTemplate('sidebar.phtml');
        return ob_get_clean();
    }

}
