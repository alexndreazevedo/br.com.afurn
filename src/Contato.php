<?php

class Contato extends Contato_Email
{

    const FALE_CONOSCO = 1;
    const ASSOCIE_SE = 2;

    public static $titulos = array(
        self::FALE_CONOSCO => 'Fale Conosco',
        self::ASSOCIE_SE => 'Associe-se',
    );

    public static $template = array(
        self::FALE_CONOSCO => 'form/faleconosco.phtml',
        self::ASSOCIE_SE => 'form/associe.phtml',
    );

    public static $titulos_id = array(
        'fale-conosco' => self::FALE_CONOSCO,
        'associe-se' => self::ASSOCIE_SE,
    );

}