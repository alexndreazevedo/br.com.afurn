$(function() {

    /*
	$('.colorbox').colorbox({
        maxWidth: '90%',
        maxHeight: '90%',
        rel: 'colorbox',
		current: false,
		previous: 'Anterior',
		next: 'Próxima',
		close: 'Fechar'
    });
	*/

    $('nav#main-menu .dropdown').hover(function(){
        var e = $(this).find('ul li').length;
        $(this).find('ul').css('height', 47 * e);
    }, function(){
        $(this).find('ul').css('height', 0);
    });

    $('nav#main-menu .dropdown').click(function(){
        var e = $(this).find('ul li').length;
        $(this).find('ul').css('height', 47 * e);
    }, function(){
        $(this).find('ul').css('height', 0);
    });

    $('nav#main-menu .dropdown').click(function(){
        var e = $(this).find('ul li').length;

        $(this).hasClass('active') ? $(this).find('ul').css('height', 0) : ($('.dropdown').removeClass('active'), $('.dropdown ul').css('height', 0), $(this).find('ul').css('height', 47 * e));
        $(this).toggleClass('active');
    });

    $('nav#menu').mmenu({
        slidingSubmenus: false
    });

    ajustarTela(false);

    $(window).bind('resize', function() {
        ajustarTela(true);
    });

    $('#slider').nivoSlider({
        effect: 'sliceDown',
        slices: 15,
        boxCols: 10,
        boxRows: 5,
        animSpeed: 100,
        pauseTime: 8000,
        startSlide: 0,
        directionNav: true,
        controlNav: true,
        controlNavThumbs: false,
        pauseOnHover: true,
        manualAdvance: false,
        prevText: 'Anterior',
        nextText: 'Próximo',
        randomStart: false
    });
	
	$('.tooltip').tooltip();
	
    setDate('date');

    setDateTime('datetime');

    setMask('tel', '(99) 9999-9999');
    setMask('cep', '99.999-999');
    setMask('cpf', '999.999.999-99');
    setMask('number', '9999999999');
    setMask('digit', '9');

	setMoney('money');
	setArea('area');
});

function setDateTime(classe) {

    classe = 'input.' + classe;

    if ($(classe).length)
    {
        $(classe).datetimepicker({
            duration: '',
            showTime: true,
            constrainInput: false,
            changeMonth: true,
            changeYear: true,
            minDate: -100
        });
    }
}

function setDate(classe) {

    classe = 'input.' + classe;

    if ($(classe).length)
    {
        $(classe).datepicker({
            duration: '',
            showTime: false,
            constrainInput: false,
            dateFormat: 'dd/mm/yy',
            yearRange: '-100Y:+0Y',
            changeMonth: true,
            changeYear: true
        }).mask('99/99/9999');
    }
}

function setMask(classe, mask) {

    classe = 'input.' + classe;

    if ($(classe).length)
    {
		$(classe).mask(mask);
	}
}

function setMoney(classe) {

    classe = 'input.' + classe;

    if ($(classe).length)
    {
		$(classe).maskMoney({symbol:'R$ ', showSymbol:true, thousands:'.', decimal:',', symbolStay: true});
	}
}

function setArea(classe) {

    classe = 'input.' + classe;

    if ($(classe).length)
    {
		$(classe).maskMoney({symbol:' m²', showSymbol:true, thousands:'.', decimal:',', symbolStay: true, symbolRight: true, precision: 3});
	}
}

function setSelect(id, text, secao) {

    id = 'select#' + id;

    var classe = '';

    if(secao == 'form')
    {
        classe = secao + ' ' + id;
        secao = '';
    }
    else
    {
        classe = '#' + secao + ' ' + id;
    }

    if ($(classe).length)
	{
        $(classe).sSelect({
            idName: secao,
            ddMaxHeight: '250px',
            defaultText: text
        });
    }
}

function ajustarTela(loaded)
{
    var largura = $(window).width();
    ajusteMenu(largura, '.mm-page');
    ajusteVitrine(largura, '#showcase .slider-image');
    ajusteComentarios(largura, '#comentarios .fb-comments', loaded);
}

function ajusteMenu(largura, obj)
{
    if(largura > 480)
    {
        $(obj).width('100%');
    }
}

function ajusteVitrine(largura, obj)
{
    var imagem  = $(obj);

    imagem.each(function() {
        if(largura <= 480)
        {
            $(this).attr('src', $(this).attr('data-src-mobile'));
        }
        else if(largura > 480 && largura <= 980)
        {
            $(this).attr('src', $(this).attr('data-src-tablet'));
        }
        else if(largura > 980 && largura < 1200)
        {
            $(this).attr('src', $(this).attr('data-src-normal'));
        }
        else if(largura >= 1200)
        {
            $(this).attr('src', $(this).attr('data-src-large'));
        }
    });
}

function ajusteComentarios(largura, obj, loaded)
{
    var container = '850',
        objeto  = $(obj),
        iframe = $(obj + ' iframe'),
        request = [];

    if(largura <= 480)
    {
        container = objeto.parent().width();
    }
    else if(largura > 480 && largura <= 980)
    {
        container = objeto.parent().width();
    }
    else if(largura > 980 && largura < 1200)
    {
        container = parseInt(objeto.attr('data-width-normal'));
    }
    else if(largura >= 1200)
    {
        container = parseInt(objeto.attr('data-width-large'));
    }

    objeto.attr('data-width', container + 'px');

    if(iframe.length && loaded)
    {
        var url = iframe.attr('src');
        var host = url.substr(0, url.indexOf('?'));
        var pairs = url.substr(url.indexOf('?') + 1).split('&');

        for (var i = 0; i < pairs.length; i++)
        {
            var pair = pairs[i].split('=');
            request[decodeURIComponent(pair[0])] = decodeURIComponent(pair[1]);
        }

        request['width'] = container;

        var pairs = [];
        for (var key in request)
        {
            if (request.hasOwnProperty(key))
            {
                pairs.push(encodeURIComponent(key) + '=' + encodeURIComponent(request[key]));
            }
        }

        var params = pairs.join('&');
        iframe.attr('src', host + '?' + params);
    }
}