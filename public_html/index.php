<?php

include '../boot.php';


$router = new Cdc_Router;

$router->setBasePath(Config::$root);

Config::configureRouter($router);

$route = $router->matchCurrentRequest();
$content = '';

if ($route)
{

    $target = $route->getTarget();

    if (array_key_exists('login_controller', $target))
    {
        $loginController = $target['login_controller'];
    }
    else
    {
        $loginController = Config::$default_login_controller;
    }

    $auth = new Cdc_Pdo_Authentication(Cdc_Pdo_Pool::getConnection(Config::$default_db), Config::$hasher);
    Config::startSessionFor($loginController, $auth);

    Config::$dispatcher = new Cdc_Dispatcher($router, $route);

    $args = array(
        'db'   => 'default',
        'auth' => $auth,
    );

    $controller = Config::$dispatcher->dispatch($args);
    $auth       = $args['auth'];
    if (!Config::routeAllowed($route, Cdc_Pdo_Pool::getConnection(Config::$default_db), f($_SESSION, $auth->getId()), f($_SESSION, 'administrador')))
    {
        if (!Cdc_Pdo_Authentication::isAuthenticated())
        {
            $route->setTarget(array('class'     => $loginController));
            Config::$dispatcher->setMatchedRoute($route);
            $controller = Config::$dispatcher->dispatch($args);
        }
        else
        {
            $args['auth']->reset();
            flash('Você foi desconectado por não ter acesso à página solicitada.', LOG_ERR);
            header('Location: ' . $_SERVER['REQUEST_URI']);
            die;
        }
    }

    $content = $controller->content();
}


if (!$route || !$content)
{
    header('HTTP/1.1 404 Not Found');
    die('404');
}

include $controller->getLayout();


